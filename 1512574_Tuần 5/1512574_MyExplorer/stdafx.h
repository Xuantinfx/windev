// stdafx.h : include file for standard system include files,
// or project specific include files that are used frequently, but
// are changed infrequently
//

#pragma once

#include "targetver.h"

#define WIN32_LEAN_AND_MEAN             // Exclude rarely-used stuff from Windows headers
// Windows Header Files:
#include <windows.h>

// C RunTime Header Files
#include <stdlib.h>
#include <malloc.h>
#include <memory.h>
#include <tchar.h>

#include <CommCtrl.h>
#include <Winuser.h>
#include <Shobjidl.h>
#include <shellapi.h>
#include <Shlwapi.h>
#include <Shlobj.h>
#include <wchar.h>
#pragma comment(lib, "User32.lib")
#pragma comment (lib,"Shlwapi.lib")
#pragma comment (lib,"Shell32.lib")
#pragma comment(lib,"comctl32.lib")

// TODO: reference additional headers your program requires here
