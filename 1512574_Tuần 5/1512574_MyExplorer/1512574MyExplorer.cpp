﻿// 1512574MyExplorer.cpp : Defines the entry point for the application.
//

#include "stdafx.h"
#include "1512574MyExplorer.h"
#define MAX_LOADSTRING 100

// Global Variables:
HINSTANCE hInst;                                // current instance
WCHAR szTitle[MAX_LOADSTRING];                  // The title bar text
WCHAR szWindowClass[MAX_LOADSTRING];            // the main window class name

/********************* Khai báo biến **************************/
HWND g_hWndTreeView;
HWND g_hWndListView;
HWND g_hWndStatus;
int g_WidthTreeView;
HWND hWnd;
bool InLineChangWidth = false;
int x_posClick;
bool accessMove = false;
HIMAGELIST g_hml16;
HIMAGELIST g_hml32;
#define SPLIPTER_WIDTH 5 
#define MAX_TREEVIEW_WIDTH 400
#define MIN_TREEVIEW_WIDTH 100
#define HEIGHT_STATUS_BAR 20

//Vị trí trong image list sau khi add
#define IDI_FOLDER 4
#define IDI_EXE 1
#define IDI_FILE 2
#define IDI_MYCOMPUTER 0

#define IDI_FLOPPY 3
#define IDI_USB 6
#define IDI_HDD 5
#define IDI_CD  8
/*******************  Hết Khai báo biến ***********************/

/********************** Định nghĩa hàm ***********************/
void ChangeWidthTreeView();
void Track(HWND hwnd);
void GetThisPC();
void GetDriver(WCHAR s[], WCHAR result[]);
LPCWSTR GetPath(HWND hWndListView, int iItem);
void LoadTreeView(LPCWSTR path, HTREEITEM _hParent, bool loadForPC);
void LoadListView(LPCWSTR path, bool loadForPC);
int ClickItem(HWND hWndListView, LPCWSTR path);
void KhoiTaoListViewFolder();
void KhoiTaoListViewDrive();
LPWSTR ConvertSize(__int64 nSize);
LPWSTR GetTypeFile(LPWSTR name);
LPWSTR GetDateModified(const FILETIME &ftLastWrite);
void TaoReSourchIcon();
/**********************  Hết định nghĩa hàm *************************/

// Forward declarations of functions included in this code module:
ATOM                MyRegisterClass(HINSTANCE hInstance);
BOOL                InitInstance(HINSTANCE, int);
LRESULT CALLBACK    WndProc(HWND, UINT, WPARAM, LPARAM);
INT_PTR CALLBACK    About(HWND, UINT, WPARAM, LPARAM);

int APIENTRY wWinMain(_In_ HINSTANCE hInstance,
                     _In_opt_ HINSTANCE hPrevInstance,
                     _In_ LPWSTR    lpCmdLine,
                     _In_ int       nCmdShow)
{
    UNREFERENCED_PARAMETER(hPrevInstance);
    UNREFERENCED_PARAMETER(lpCmdLine);

    // TODO: Place code here.

    // Initialize global strings
    LoadStringW(hInstance, IDS_APP_TITLE, szTitle, MAX_LOADSTRING);
    LoadStringW(hInstance, IDC_1512574MYEXPLORER, szWindowClass, MAX_LOADSTRING);
    MyRegisterClass(hInstance);

    // Perform application initialization:
    if (!InitInstance (hInstance, nCmdShow))
    {
        return FALSE;
    }

    HACCEL hAccelTable = LoadAccelerators(hInstance, MAKEINTRESOURCE(IDC_1512574MYEXPLORER));

    MSG msg;

    // Main message loop:
    while (GetMessage(&msg, nullptr, 0, 0))
    {
        if (!TranslateAccelerator(msg.hwnd, hAccelTable, &msg))
        {
            TranslateMessage(&msg);
            DispatchMessage(&msg);
        }
    }

    return (int) msg.wParam;
}



//
//  FUNCTION: MyRegisterClass()
//
//  PURPOSE: Registers the window class.
//
ATOM MyRegisterClass(HINSTANCE hInstance)
{
    WNDCLASSEXW wcex;

    wcex.cbSize = sizeof(WNDCLASSEX);

    wcex.style          = CS_HREDRAW | CS_VREDRAW;
    wcex.lpfnWndProc    = WndProc;
    wcex.cbClsExtra     = 0;
    wcex.cbWndExtra     = 0;
    wcex.hInstance      = hInstance;
    wcex.hIcon          = LoadIcon(hInstance, MAKEINTRESOURCE(IDI_1512574MYEXPLORER));
    wcex.hCursor        = LoadCursor(nullptr, IDC_ARROW);
    wcex.hbrBackground  = (HBRUSH)(COLOR_BTNFACE+1);
    wcex.lpszMenuName   = MAKEINTRESOURCEW(IDC_1512574MYEXPLORER);
    wcex.lpszClassName  = szWindowClass;
    wcex.hIconSm        = LoadIcon(wcex.hInstance, MAKEINTRESOURCE(IDI_SMALL));

    return RegisterClassExW(&wcex);
}

//
//   FUNCTION: InitInstance(HINSTANCE, int)
//
//   PURPOSE: Saves instance handle and creates main window
//
//   COMMENTS:
//
//        In this function, we save the instance handle in a global variable and
//        create and display the main program window.
//
BOOL InitInstance(HINSTANCE hInstance, int nCmdShow)
{
   hInst = hInstance; // Store instance handle in our global variable

   hWnd = CreateWindowW(szWindowClass, szTitle, WS_OVERLAPPEDWINDOW,
      CW_USEDEFAULT, 0, CW_USEDEFAULT, 0, nullptr, nullptr, hInstance, nullptr);

   if (!hWnd)
   {
      return FALSE;
   }

   ShowWindow(hWnd, nCmdShow);
   UpdateWindow(hWnd);

   return TRUE;
}

//
//  FUNCTION: WndProc(HWND, UINT, WPARAM, LPARAM)
//
//  PURPOSE:  Processes messages for the main window.
//
//  WM_COMMAND  - process the application menu
//  WM_PAINT    - Paint the main window
//  WM_DESTROY  - post a quit message and return
//
//


LRESULT CALLBACK WndProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam)
{
    switch (message)
    {
	case WM_CREATE:
	{
		//đọc file ini
		WCHAR buff[10];
		WCHAR current[MAX_PATH];
		GetCurrentDirectory(MAX_PATH, current);
		wsprintf(current, L"%s\\config.ini", current);

		int left, right, top, bottom;
		GetPrivateProfileString(L"My_Explorer", L"top", L"115", buff, MAX_PATH, current);
		top = _wtoi(buff);

		GetPrivateProfileString(L"My_Explorer", L"bottom", L"620", buff, MAX_PATH, current);
		bottom = _wtoi(buff);

		GetPrivateProfileString(L"My_Explorer", L"left", L"200", buff, MAX_PATH, current);
		left = _wtoi(buff);

		GetPrivateProfileString(L"My_Explorer", L"right", L"1000", buff, MAX_PATH, current);
		right = _wtoi(buff);

		GetPrivateProfileString(L"My_Explorer", L"width_Tree_View", L"200", buff, MAX_PATH, current);
		g_WidthTreeView = _wtoi(buff);

		//resize lại theo kích thước cũ
		MoveWindow(hWnd, left, top, right - left, bottom - top, true);

		RECT rect;
		GetClientRect(hWnd, &rect);

		g_hWndTreeView = CreateWindowEx(NULL, WC_TREEVIEW, _T("Tree View"),
			WS_CHILD | WS_VISIBLE | WS_BORDER | WS_VSCROLL | WS_HSCROLL | PGS_AUTOSCROLL | TVS_HASBUTTONS | TVS_LINESATROOT | WS_TABSTOP, 0, 0, 100, 100, hWnd, (HMENU)IDC_TREEVIEW, hInst, NULL);
		g_hWndListView = CreateWindowEx(NULL, WC_LISTVIEW, _T("List View"), WS_CHILD | WS_VISIBLE | WS_VSCROLL | WS_HSCROLL | WS_TABSTOP | LVS_ICON | LVS_EDITLABELS | LVS_SHOWSELALWAYS | LVS_REPORT, 100, 0, 100, 100, hWnd, (HMENU)IDC_LISTVIEW, hInst, NULL);

		g_hWndStatus = CreateWindow(STATUSCLASSNAME, NULL, WS_CHILD | WS_VISIBLE | SBARS_SIZEGRIP, 0, 200, 100, 100, hWnd, NULL, hInst, NULL);

		//load các ổ đĩa cho listview
		KhoiTaoListViewDrive();
		LoadListView(L"", true);

		//load This PC cho treeview
		GetThisPC();


		//lấy icon hệ thống
		TaoReSourchIcon();
	}
		break;

	case WM_WINDOWPOSCHANGED:
	{
		ChangeWidthTreeView();

		RECT rect;
		GetWindowRect(hWnd, &rect);

		//ghi file ini
		WCHAR buff[MAX_PATH];
		WCHAR current[MAX_PATH];
		GetCurrentDirectory(MAX_PATH, current);
		wsprintf(current, L"%s\\config.ini", current);

		wsprintf(buff, L"%d", rect.top);
		WritePrivateProfileString(L"My_Explorer", L"top", buff, current);

		wsprintf(buff, L"%d", rect.bottom);
		WritePrivateProfileString(L"My_Explorer", L"bottom", buff, current);

		wsprintf(buff, L"%d", rect.left);
		WritePrivateProfileString(L"My_Explorer", L"left", buff, current);

		wsprintf(buff, L"%d", rect.right);
		WritePrivateProfileString(L"My_Explorer", L"right", buff, current);

		wsprintf(buff, L"%d", g_WidthTreeView);
		WritePrivateProfileString(L"My_Explorer", L"width_Tree_View", buff, current);
		break;
	}
	case WM_NOTIFY:
	{
		//lParam chứa đối tượng và mã sự kiện
		UINT code = ((LPNMHDR)lParam)->code;
		switch (code)
		{
		case TVN_ITEMEXPANDING:
		{
			HTREEITEM treeChild = ((LPNMTREEVIEW)(LPNMHDR)lParam)->itemNew.hItem;

			HTREEITEM hCurSelChild = TreeView_GetChild(g_hWndTreeView, treeChild);

			//nếu đã load thì k load nữa
			if (hCurSelChild != NULL)
				break;

			WCHAR * buffer = new WCHAR[1024];
			TVITEM tv;
			tv.mask = TVIF_PARAM;
			tv.hItem = treeChild;
			tv.lParam = (LPARAM)buffer;
			TreeView_GetItem(g_hWndTreeView, &tv);
			//expainding This PC
			if (StrCmp((LPWSTR)tv.lParam, L"") == 0)
			{
				break;
			}
			else
			{
				LoadTreeView((LPWSTR)tv.lParam, treeChild, false);
			}
			break;
		}
		case TVN_SELCHANGED:
		{

			SetWindowText(g_hWndStatus, L"");

			ListView_DeleteAllItems(g_hWndListView); //Xóa sạch List View

			LPCWSTR m_path_ListView;
			//lấy đường dẫn từ file được chọn
			m_path_ListView = (LPCWSTR)((LPNMTREEVIEW)(LPNMHDR)lParam)->itemNew.lParam;

			//load cho PC
			if (StrCmp(m_path_ListView, L"") == 0)
			{
				KhoiTaoListViewDrive();
				LoadListView(m_path_ListView, true);
			}
			else //load cho thu muc
			{
				KhoiTaoListViewFolder();
				LoadListView(m_path_ListView, false);
			}
			break;
		}
		case NM_CLICK:
			if ((((LPNMHDR)lParam)->hwndFrom) == g_hWndListView)
			{
				if (ListView_GetSelectionMark(g_hWndListView) != -1)
				{
					WCHAR *buffer = new WCHAR[1024];
					buffer[0] = L'\0';
					LPCWSTR m_path = GetPath(g_hWndListView, ListView_GetSelectionMark(g_hWndListView));
					WIN32_FIND_DATA fd;
					GetFileAttributesEx(m_path, GetFileExInfoStandard, &fd);
					//là thư mục
					if (fd.dwFileAttributes & FILE_ATTRIBUTE_DIRECTORY)
					{
						SetWindowText(g_hWndStatus, L"");
						break;
					}
					else
					{
						wsprintf(buffer, L"Size: %s", ConvertSize(fd.nFileSizeLow));
						SetWindowText(g_hWndStatus, buffer);
					}
				}
			}
			break;
		case NM_DBLCLK:
		{
			if ((((LPNMHDR)lParam)->hwndFrom) == g_hWndListView)
			{
				if (ListView_GetSelectionMark(g_hWndListView) != -1)
				{
					LPCWSTR m_path = GetPath(g_hWndListView, ListView_GetSelectionMark(g_hWndListView));
					ClickItem(g_hWndListView, m_path);
				}
			}
		}
		break;
		}
		break;
	}
	case WM_MOUSEMOVE:
	{
		//Nếu con chuột vừa mới di chuyển vào vùng splitter thì tiến hành track và đổi trạng thái 
		//chuột sang mũi tên
		if (InLineChangWidth == FALSE)
		{
			Track(hWnd);
			InLineChangWidth = TRUE;
		}
		//Đổi con chuột sang dạng mũi tên
		SetCursor(LoadCursor(nullptr, IDC_SIZEWE));

		//Nếu chuột đang click trái và ở trong khu vực kéo và đang di chuyển
		if (accessMove)
		{
			int x = (int)LOWORD(lParam);
			
			int change = x - x_posClick;
			x_posClick = x;
			g_WidthTreeView += change;
			if (g_WidthTreeView < MIN_TREEVIEW_WIDTH)
				g_WidthTreeView = MIN_TREEVIEW_WIDTH;
			if (g_WidthTreeView > MAX_TREEVIEW_WIDTH)
				g_WidthTreeView = MAX_TREEVIEW_WIDTH;
			ChangeWidthTreeView();
		}
	}
	break;
	case WM_MOUSELEAVE:
		InLineChangWidth = FALSE;
		break;
	case WM_LBUTTONDOWN:
	{
		SetCapture(hWnd);
		if (InLineChangWidth == TRUE)
		{
			accessMove = TRUE;
			x_posClick = (int)LOWORD(lParam);
		}
	}
		break;
	case WM_LBUTTONUP:
	{
		accessMove = FALSE;
		ReleaseCapture();
		break;
	}
    case WM_COMMAND:
        {
            int wmId = LOWORD(wParam);
            // Parse the menu selections:
            switch (wmId)
            {
            case IDM_ABOUT:
                DialogBox(hInst, MAKEINTRESOURCE(IDD_ABOUTBOX), hWnd, About);
                break;
            case IDM_EXIT:
                DestroyWindow(hWnd);
                break;
            default:
                return DefWindowProc(hWnd, message, wParam, lParam);
            }
        }
        break;
    case WM_PAINT:
        {
            PAINTSTRUCT ps;
            HDC hdc = BeginPaint(hWnd, &ps);
            // TODO: Add any drawing code that uses hdc here...
            EndPaint(hWnd, &ps);
        }
        break;
    case WM_DESTROY:
        PostQuitMessage(0);
        break;
    default:
        return DefWindowProc(hWnd, message, wParam, lParam);
    }
    return 0;
}

// Message handler for about box.
INT_PTR CALLBACK About(HWND hDlg, UINT message, WPARAM wParam, LPARAM lParam)
{
    UNREFERENCED_PARAMETER(lParam);
    switch (message)
    {
    case WM_INITDIALOG:
        return (INT_PTR)TRUE;

    case WM_COMMAND:
        if (LOWORD(wParam) == IDOK || LOWORD(wParam) == IDCANCEL)
        {
            EndDialog(hDlg, LOWORD(wParam));
            return (INT_PTR)TRUE;
        }
        break;
    }
    return (INT_PTR)FALSE;
}


void ChangeWidthTreeView()
{
	RECT rect;
	GetClientRect(hWnd, &rect);

	MoveWindow(g_hWndTreeView, 0, 0, g_WidthTreeView, rect.bottom - rect.top - HEIGHT_STATUS_BAR, TRUE);
	MoveWindow(g_hWndListView, g_WidthTreeView + SPLIPTER_WIDTH, 0, rect.right - rect.left - SPLIPTER_WIDTH - g_WidthTreeView, rect.bottom - rect.top - HEIGHT_STATUS_BAR,TRUE);
	MoveWindow(g_hWndStatus, 0, rect.bottom - HEIGHT_STATUS_BAR, rect.bottom - rect.top, HEIGHT_STATUS_BAR, true);
}

void Track(HWND hwnd)
{
	TRACKMOUSEEVENT track;
	track.cbSize = sizeof(TRACKMOUSEEVENT);
	track.dwFlags = TME_HOVER | TME_LEAVE;
	track.dwHoverTime = 1;
	track.hwndTrack = hwnd;
	TrackMouseEvent(&track);
}

BOOL PreLoad(LPCWSTR path, HTREEITEM _hParent) {
	LPITEMIDLIST penumIDL_from_path = ILCreateFromPath(path);
	LPSHELLFOLDER psfParent = NULL;
	SHBindToObject(NULL, penumIDL_from_path, NULL, IID_IShellFolder, (void**)&psfParent);
	LPENUMIDLIST penumIDL = NULL;

	psfParent->EnumObjects(NULL, SHCONTF_FOLDERS, &penumIDL);

	HRESULT hr = NULL;
	LPITEMIDLIST pidl = NULL;
	do {
		hr = penumIDL->Next(1, &pidl, NULL);
		if (hr == S_OK)
		{
			WCHAR buffer[1024];
			STRRET strret;
			psfParent->GetDisplayNameOf(pidl, SHGDN_NORMAL, &strret);
			StrRetToBuf(&strret, pidl, buffer, 1024);

			//lấy kích thước file hoặc thư mục, nếu như có kích thước thì là file
			//nếu là file thì bỏ qua
			WCHAR strsize[100];
			WIN32_FIND_DATA dt;
			SHGetDataFromIDList(psfParent, pidl, SHGDFIL_FINDDATA, &dt, sizeof(dt));
			_ltow_s(dt.nFileSizeLow, strsize, 10);
			if (wcscmp(strsize, L"0") != 0)
				continue;

			if (StrCmp(buffer, L"") != 0)
			{
				return TRUE;
			}

		}
	} while (hr == S_OK);

	psfParent->Release();

	return FALSE;
}

void LoadListView(LPCWSTR path,bool loadForPC)
{
	LPITEMIDLIST penumIDL_from_path = ILCreateFromPath(path);
	LPSHELLFOLDER psfParent = NULL;
	SHBindToObject(NULL, penumIDL_from_path, NULL, IID_IShellFolder, (void**)&psfParent);
	LPENUMIDLIST penumIDL = NULL;

	psfParent->EnumObjects(NULL, SHCONTF_STORAGE, &penumIDL);

	HRESULT hr = NULL;
	LPITEMIDLIST pidl = NULL;
	int count = 0;
	do {
		hr = penumIDL->Next(1, &pidl, NULL);
		if (hr == S_OK)
		{
			WCHAR buffer[MAX_PATH];
			STRRET strret;
			psfParent->GetDisplayNameOf(pidl, SHGDN_NORMAL, &strret);
			StrRetToBuf(&strret, pidl, buffer, MAX_PATH);

			if (StrCmp(buffer, L"") != 0)
			{
				LVITEM lv;
				lv.mask = LVIF_TEXT | LVIF_IMAGE | LVIF_PARAM;
				lv.iItem = count;
				lv.iImage = IDI_FOLDER;
				lv.iSubItem = 0;		//Côt tên thư mục
				lv.pszText = buffer;
				WCHAR *path_child = new WCHAR[MAX_PATH];
				if (loadForPC)
				{
					lv.iImage = IDI_HDD;
					GetDriver(buffer, path_child);
				}
				else
				{
					if ((WCHAR)path[lstrlenW(path) -1] == L'\\')
						wsprintf(path_child, L"%s%s", path, buffer);
					else
						wsprintf(path_child, L"%s\\%s", path, buffer);
				}
				lv.lParam = (LPARAM)path_child;
				ListView_InsertItem(g_hWndListView, &lv);

				if(loadForPC)
				{ 
					ListView_SetItemText(g_hWndListView, count, 1, L"Disk");//Cột loại
					__int64 size = 0;
					SHGetDiskFreeSpaceEx(path_child, NULL, (PULARGE_INTEGER)&size, NULL);
					ListView_SetItemText(g_hWndListView,count,2, ConvertSize(size)); //Lấy dung lượng ổ đĩa

					SHGetDiskFreeSpaceEx(path_child, NULL, NULL, (PULARGE_INTEGER)&size);
					ListView_SetItemText(g_hWndListView,count,3, ConvertSize(size)); //Dung lượng trống
				}
				else
				{ 
					WIN32_FIND_DATA fd;
					GetFileAttributesEx(path_child, GetFileExInfoStandard, &fd);
					if (fd.dwFileAttributes & FILE_ATTRIBUTE_DIRECTORY)
					{
						ListView_SetItemText(g_hWndListView, count, 2, L"Thư mục");//Cột loại
					}
					else
					{
						if(StrCmpW(GetTypeFile(buffer),L"EXE") == 0)
							lv.iImage = IDI_EXE;
						else
						lv.iImage = IDI_FILE;
						ListView_SetItemText(g_hWndListView, count, 2, GetTypeFile(buffer));//Cột loại
						ListView_SetItemText(g_hWndListView, count, 1,ConvertSize(fd.nFileSizeLow)); // Cột size
					}
					
					//Cột Ngày chỉnh sửa
					ListView_SetItemText(g_hWndListView, count, 3, GetDateModified(fd.ftLastWriteTime));
				}
				ListView_SetItem(g_hWndListView, &lv);
				count++;
			}

		}
	} while (hr == S_OK);

	psfParent->Release();
}

void LoadTreeView(LPCWSTR path, HTREEITEM _hParent, bool loadForPC) {
	LPITEMIDLIST penumIDL_from_path = ILCreateFromPath(path);
	LPSHELLFOLDER psfParent = NULL;
	SHBindToObject(NULL, penumIDL_from_path,NULL, IID_IShellFolder, (void**)&psfParent);
	LPENUMIDLIST penumIDL = NULL;

	psfParent->EnumObjects(NULL, SHCONTF_FOLDERS, &penumIDL);

	HRESULT hr = NULL;
	LPITEMIDLIST pidl = NULL;
	do {
		hr = penumIDL->Next(1, &pidl, NULL);
		if (hr == S_OK)
		{
			WCHAR buffer[MAX_PATH];
			STRRET strret;
			psfParent->GetDisplayNameOf(pidl, SHGDN_NORMAL, &strret);
			StrRetToBuf(&strret, pidl, buffer, MAX_PATH);
			
			//lấy kích thước file hoặc thư mục, nếu như có kích thước thì là file
			//nếu là file thì bỏ qua
			WCHAR strsize[100];
			WIN32_FIND_DATA dt;
			SHGetDataFromIDList(psfParent, pidl, SHGDFIL_FINDDATA, &dt, sizeof(dt));
			_ltow_s(dt.nFileSizeLow, strsize, 10);
			if (wcscmp(strsize, L"0") != 0)
				continue;

			if (StrCmp(buffer, L"") != 0)
			{
				TV_INSERTSTRUCT tv;
				tv.hParent = _hParent;
				tv.hInsertAfter = TVI_LAST;
				tv.item.mask = TVIF_TEXT | TVIF_IMAGE | TVIF_PARAM | TVIF_CHILDREN | TVIF_SELECTEDIMAGE;
				tv.item.iImage = IDI_FOLDER;
				tv.item.iSelectedImage = IDI_FOLDER;
				tv.item.cChildren = 0;
				//	tv.item.iImage = IDI_MYCOMPUTER;
				//	tv.item.iSelectedImage = IDI_MYCOMPUTER;
				tv.item.pszText = buffer;
				WCHAR *path_child = new WCHAR[MAX_PATH];
				if (loadForPC)
				{
					tv.item.iImage = IDI_HDD;
					tv.item.iSelectedImage = IDI_HDD;

					//để sửa lại đường dẫn ví dụ như "Local Disk (C:)" thành "C:\\"
					GetDriver(buffer, path_child);
				}
				else
				{
					if((WCHAR)path[lstrlenW(path) -1] == L'\\')
						wsprintf(path_child, L"%s%s", path, buffer);
					else
						wsprintf(path_child, L"%s\\%s", path, buffer);
				}
				tv.item.lParam = (LPARAM)path_child;

				if(PreLoad(LPCWSTR(path_child), NULL))
					tv.item.cChildren = 1;
				TreeView_InsertItem(g_hWndTreeView, &tv);
			}

		}
	} while (hr == S_OK);

	psfParent->Release();
}


void GetThisPC(){

	LPSHELLFOLDER psfDesktop = NULL;
	//lấy desktop
	SHGetDesktopFolder(&psfDesktop);

	LPENUMIDLIST penumIDL = NULL; //lấy enumerator
	psfDesktop->EnumObjects(NULL, SHCONTF_FOLDERS, &penumIDL);

	LPITEMIDLIST pidl = NULL; //PIDL dùng để duyệt
	HRESULT hr = NULL;

	do {
		hr = penumIDL->Next(1, &pidl, NULL);
		if (hr == S_OK)
		{
			WCHAR buffer[1024];
			STRRET strret;
			psfDesktop->GetDisplayNameOf(pidl, SHGDN_NORMAL, &strret);
			StrRetToBuf(&strret, pidl, buffer, 1024);
			if (StrCmp(buffer, L"This PC") == 0)
			{
				//Tạo nút gốc là This PC
				HTREEITEM PC;
				TV_INSERTSTRUCT tv;
				tv.hParent = NULL;
				tv.hInsertAfter = TVI_LAST;
				tv.item.mask = TVIF_TEXT | TVIF_IMAGE | TVIF_PARAM |TVIF_CHILDREN;
				tv.item.iImage = IDI_MYCOMPUTER;
				tv.item.iSelectedImage = IDI_MYCOMPUTER;
				tv.item.cChildren = 1;
				tv.item.pszText = L"This PC";
				tv.item.lParam = (LPARAM)L"";
				WCHAR tam[100];
				PC = TreeView_InsertItem(g_hWndTreeView, &tv);
				LoadTreeView(L"",PC,true);
				break;
			}
		}
	} while (hr == S_OK);

	psfDesktop->Release();
}

//Click vào item ở listview
int ClickItem(HWND hWndListView, LPCWSTR path)
{
	if (!path)
		return 0;
	WIN32_FIND_DATA fd;
	GetFileAttributesEx(path, GetFileExInfoStandard, &fd);
	if (fd.dwFileAttributes & FILE_ATTRIBUTE_DIRECTORY)
	{
		ListView_DeleteAllItems(g_hWndListView);
		KhoiTaoListViewFolder();
		LoadListView(path,false);
		return 0;
	}
	ShellExecute(NULL, L"open", path, NULL, NULL, SW_SHOWNORMAL);
	return 1;
}


//lấy đường dẫn ổ đĩa từ tên ổ đĩa
void GetDriver(WCHAR s[], WCHAR result[]) {
	for (int i = 0; i < lstrlenW(s); i++) {
		if (s[i] == L':')
		{
			result[0] = s[i -1];
		}
	}
	result[1] = L':';
	result[2] = L'\0';
	StrCat(result, L"\\");
}

//Lấy đường dẫn từ thứ tự trong listview
LPCWSTR GetPath(HWND hWndListView, int iItem)
{
	WCHAR * buffer = new WCHAR[1024];
	LVITEM lv;
	lv.mask = LVIF_PARAM;
	lv.iItem = iItem;
	lv.iSubItem = 0;
	lv.lParam = (LPARAM)buffer;
	ListView_GetItem(hWndListView, &lv);
	return (LPCWSTR)lv.lParam;
}

void KhoiTaoListViewDrive()
{
	for (int i = 0; i < 4; i++)
		ListView_DeleteColumn(g_hWndListView, 0);
	LVCOLUMN lvCol;
	lvCol.mask = LVCF_FMT | LVCF_TEXT | LVCF_WIDTH;
	lvCol.fmt = LVCFMT_LEFT;

	lvCol.cx = 200;
	lvCol.pszText = _T("Tên");
	ListView_InsertColumn(g_hWndListView, 0, &lvCol);

	lvCol.cx = 80;
	lvCol.pszText = _T("Kích thước");
	ListView_InsertColumn(g_hWndListView, 2, &lvCol);

	lvCol.cx = 100;
	lvCol.pszText = _T("Loại");
	ListView_InsertColumn(g_hWndListView, 1, &lvCol);


	lvCol.cx = 300;
	lvCol.pszText = _T("Dung lượng trống");
	ListView_InsertColumn(g_hWndListView, 3, &lvCol);
}

void KhoiTaoListViewFolder()
{
	for (int i = 0; i < 4; i++)
		ListView_DeleteColumn(g_hWndListView, 0);
	LVCOLUMN lvCol;

	lvCol.mask = LVCF_FMT | LVCF_TEXT | LVCF_WIDTH;
	lvCol.fmt = LVCFMT_LEFT;

	lvCol.cx = 200;
	lvCol.pszText = _T("Tên");
	ListView_InsertColumn(g_hWndListView, 0, &lvCol);

	lvCol.cx = 70;
	lvCol.fmt = LVCFMT_RIGHT;

	lvCol.pszText = _T("Kích thước");
	ListView_InsertColumn(g_hWndListView, 1, &lvCol);

	lvCol.cx = 80;
	lvCol.fmt = LVCFMT_LEFT;
	lvCol.pszText = _T("Loại");
	ListView_InsertColumn(g_hWndListView, 2, &lvCol);


	lvCol.cx = 300;
	lvCol.pszText = _T("Thời gian chỉnh sửa");
	ListView_InsertColumn(g_hWndListView, 3, &lvCol);
}


//Đổi thời gian sang chuỗi
LPWSTR GetDateModified(const FILETIME &ftLastWrite)
{
	SYSTEMTIME t;
	FileTimeToSystemTime(&ftLastWrite, &t);
	WCHAR *buffer = new WCHAR[50];
	swprintf_s(buffer, 50, L"%d/%02d/%02d %02d:%02d",
		t.wDay, t.wMonth, t.wYear, t.wHour,
		t.wMinute);
	return buffer;
}

//Lấy đuôi của tập tin
LPWSTR GetTypeFile(LPWSTR name)
{
	int i;
	int len = wcslen(name);
	WCHAR* res = new WCHAR[30];
	for (i = len - 1; i >= 0 && name[i] != '.'; i--);
	if (i == -1)
		return L"File";
	wsprintf(res, L"%s", &name[i + 1]);
	for (int i = 0; i < lstrlenW(res);i++)
	{
		res[i] = towupper(res[i]);
	}
	return res;
}

#define KB 1
#define MB 2
#define GB 3
#define TB 4
#define RADIX 10

//Hàm chuyển đổi kích thước lấy từ code của thầy Quang
LPWSTR ConvertSize(__int64 nSize)
{
	int nType = 0; //Bytes

	while (nSize >= 1048576) //
	{
		nSize /= 1024;
		++nType;
	}

	__int64 nRight;

	if (nSize >= 1024)
	{
		//Lấy một chữ số sau thập phân của nSize chứa trong nRight
		nRight = nSize % 1024;

		while (nRight > 99)
			nRight /= 10;

		nSize /= 1024;
		++nType;
	}
	else
		nRight = 0;

	TCHAR *buffer = new TCHAR[11];
	_itow_s(nSize, buffer, 11, RADIX);

	if (nRight != 0 && nType > KB)
	{
		StrCat(buffer, _T("."));
		TCHAR *right = new TCHAR[3];
		_itow_s(nRight, right, 3, RADIX);
		StrCat(buffer, right);
	}

	switch (nType)
	{
	case 0://Bytes
		StrCat(buffer, _T(" BYTES"));
		break;
	case KB:
		StrCat(buffer, _T(" KB"));
		break;
	case MB:
		StrCat(buffer, _T(" MB"));
		break;
	case GB:
		StrCat(buffer, _T(" GB"));
		break;
	case TB:
		StrCat(buffer, _T(" TB"));
		break;
	}

	return buffer;
}

//Load các icon lên danh sách image
void TaoReSourchIcon()
{
	g_hml32 = ImageList_Create(32, 32, ILC_MASK | ILC_COLOR32, 1, 1);
	g_hml16 = ImageList_Create(16, 16, ILC_MASK | ILC_COLOR16, 1, 1);

	HICON s;
	s = ExtractIcon(hInst, L"%SystemRoot%\\System32\\shell32.dll", 15);//PC
	ImageList_AddIcon(g_hml32, s);
	ImageList_AddIcon(g_hml16, s);

	s = ExtractIcon(hInst, L"%SystemRoot%\\System32\\shell32.dll", 2);//exe
	ImageList_AddIcon(g_hml32, s);
	ImageList_AddIcon(g_hml16, s);


	s = ExtractIcon(hInst, L"%SystemRoot%\\System32\\shell32.dll", 0);//file
	ImageList_AddIcon(g_hml32, s);
	ImageList_AddIcon(g_hml16, s);

	s = ExtractIcon(hInst, L"%SystemRoot%\\System32\\shell32.dll", 6);//floppy
	ImageList_AddIcon(g_hml32, s);
	ImageList_AddIcon(g_hml16, s);

	s = ExtractIcon(hInst, L"%SystemRoot%\\System32\\shell32.dll", 3);//folder
	ImageList_AddIcon(g_hml32, s);
	ImageList_AddIcon(g_hml16, s);

	s = ExtractIcon(hInst, L"%SystemRoot%\\System32\\shell32.dll", 79);//hdd
	ImageList_AddIcon(g_hml32, s);
	ImageList_AddIcon(g_hml16, s);

	s = ExtractIcon(hInst, L"%SystemRoot%\\System32\\shell32.dll", 26);//usb
	ImageList_AddIcon(g_hml32, s);
	ImageList_AddIcon(g_hml16, s);

	s = ExtractIcon(hInst, L"%SystemRoot%\\System32\\shell32.dll", 11);//cd
	ImageList_AddIcon(g_hml32, s);
	ImageList_AddIcon(g_hml16, s);

	SendMessage(g_hWndTreeView, TVM_SETIMAGELIST, 0, (LPARAM)g_hml16);
	ListView_SetImageList(g_hWndListView, g_hml32, 1);

	DeleteObject(s);
}