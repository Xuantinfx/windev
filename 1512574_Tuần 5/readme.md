# 1512574 - Đào Xuân Tin - xuantinfx@gmail.com

# Bài tập tuần 5 - My_Explorer_Update

# Những gì đã làm được

1. Sử dụng shell để lấy các đối tượng thư mục và tập tin của các ổ đĩa
2. Cải tiến cho phép khi thay đổi kích thước treeview thì listview thay đổi theo
3. Bổ  sung statusbar khi click vào một tập tin trong listview thì hiển thị kích thước tập tin tương ứng, thể hiện dạng động x.x KB/MB/GB/TB/PB
4. Lưu lại kích thước cửa sổ màn hình chính và nạp lại khi chương trình chạy lên.
5. Lấy kích thước file và chạy file khi double click vào file ở listview

# 3 luồn sự kiện phụ

1. Chỉ cho thay đổi kích thước của treeview khi con trỏ chuột ở trên Spitter
2. Bắt sự kiện thay đổi kích thước màn hình để đưa kích thước vào lưu lại để lần sau khôi phục
3. Thay đổi hình dạng con trỏ chuột thành mũi tên ngang khi con trỏ chuột ở trên thanh spitter

# Link Bitbucket: https://Xuantinfx@bitbucket.org/Xuantinfx/windev.git
