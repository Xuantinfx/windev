# MSSV: 1512574 - Đào Xuân Tin - xuantinfx@gmail.com

# Tuần 6: Paint đơn giản

# Chức năng làm được
 1. Vẽ được 5 loại hình: đường thẳng, hình chữ nhật, hình vuông, hình ellipse, hình tròn.
 2. Có 3 chế độ vẽ được chọn từ menu hoặc popup bằng chuột phải, chọn vẽ đường thẳng, hình chữ nhật hoặc ellipse, đang vẽ hình nào thì sẽ có dấu tích trên loại hình cần vẽ.
 3. Nếu đang vẽ hình chữ nhật thì giữ **shift** sẽ vẽ hình vuông hoặc đang vẽ hình ellipse thì sẽ vẽ hình tròn.
 4. Đã bọc tất cả các lớp đối tượng vào từng **class** tương ứng, và class **cha** là class **CShape**.
 5. Có sử dụng mẫu Factory để tạo ra đối tượng mới và mẫu Prototype để vẽ preview.
 6. Lưu xuống file những gì đang vẽ và đọc từ file lên để vẽ tiếp.

# Chức năng chưa làm được
 1. Chưa lưu được dạng hình ảnh như bitmap hay png.
 2. Chưa load được hình ảnh dạng bitmap hay png.

# Luồng sự kiện phụ
 1. Có nút Edit > Undo để thu hồi nét vừa vẽ.
 2. Nhấn chuột phải xuất hiện popup thông với toolbar trên menu, chỉ cần đổi một ở một trong 2 thì nơi còn lại sẽ tự thay đổi theo.
 3. Sử dụng BitBlt và một số hàm khác để vẽ được mượt mà hơn, không bị giật.

# Link demo youtube: https://www.youtube.com/watch?v=Wg4l-g0IoI4&feature=youtu.be
# Link Bitbucket: https://Xuantinfx@bitbucket.org/Xuantinfx/windev.git