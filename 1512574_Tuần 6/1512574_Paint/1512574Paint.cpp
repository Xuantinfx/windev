﻿// 1512574Paint.cpp : Defines the entry point for the application.
//

#include "stdafx.h"
#include "1512574Paint.h"

#define MAX_LOADSTRING 100

// Global Variables:
HINSTANCE hInst;                                // current instance
WCHAR szTitle[MAX_LOADSTRING];                  // The title bar text
WCHAR szWindowClass[MAX_LOADSTRING];            // the main window class name

/******************* Khu vực khai báo biến ******************/
BOOL g_isDrawing = FALSE;
int g_xStart = 0, g_yStart = 0;
int g_xEnd, g_yEnd;
TypeShape g_ShapeChoise = line;
vector<CShape*> g_ListObject;
CShapeFactory g_ShapeFatory;
CShapePrototype g_ShapePrototype;
WCHAR fileName[1000] = {0};
HMENU hMenu = NULL;
HMENU hPopupMenu;

/******************* Khu vực khai báo hàm *******************/
bool SaveOject(WCHAR fileName[]);
bool LoadOject(WCHAR fileName[]);
BOOL DialogOpenFile(HWND hwnd, WCHAR fileName[]);
BOOL DiaLogSaveFile(HWND hwnd, WCHAR fileName[]);
void HandleModePaint(TypeShape type);

// Forward declarations of functions included in this code module:
ATOM                MyRegisterClass(HINSTANCE hInstance);
BOOL                InitInstance(HINSTANCE, int);
LRESULT CALLBACK    WndProc(HWND, UINT, WPARAM, LPARAM);
INT_PTR CALLBACK    About(HWND, UINT, WPARAM, LPARAM);

int APIENTRY wWinMain(_In_ HINSTANCE hInstance,
                     _In_opt_ HINSTANCE hPrevInstance,
                     _In_ LPWSTR    lpCmdLine,
                     _In_ int       nCmdShow)
{
    UNREFERENCED_PARAMETER(hPrevInstance);
    UNREFERENCED_PARAMETER(lpCmdLine);

    // TODO: Place code here.

    // Initialize global strings
    LoadStringW(hInstance, IDS_APP_TITLE, szTitle, MAX_LOADSTRING);
    LoadStringW(hInstance, IDC_1512574PAINT, szWindowClass, MAX_LOADSTRING);
    MyRegisterClass(hInstance);

    // Perform application initialization:
    if (!InitInstance (hInstance, nCmdShow))
    {
        return FALSE;
    }

    HACCEL hAccelTable = LoadAccelerators(hInstance, MAKEINTRESOURCE(IDC_1512574PAINT));

    MSG msg;

    // Main message loop:
    while (GetMessage(&msg, nullptr, 0, 0))
    {
        if (!TranslateAccelerator(msg.hwnd, hAccelTable, &msg))
        {
            TranslateMessage(&msg);
            DispatchMessage(&msg);
        }
    }

    return (int) msg.wParam;
}


//
//  FUNCTION: MyRegisterClass()
//
//  PURPOSE: Registers the window class.
//
ATOM MyRegisterClass(HINSTANCE hInstance)
{
    WNDCLASSEXW wcex;

    wcex.cbSize = sizeof(WNDCLASSEX);

    wcex.style          = CS_HREDRAW | CS_VREDRAW;
    wcex.lpfnWndProc    = WndProc;
    wcex.cbClsExtra     = 0;
    wcex.cbWndExtra     = 0;
    wcex.hInstance      = hInstance;
    wcex.hIcon          = LoadIcon(hInstance, MAKEINTRESOURCE(IDI_1512574PAINT));
    wcex.hCursor        = LoadCursor(nullptr, IDC_ARROW);
    wcex.hbrBackground  = (HBRUSH)(COLOR_WINDOW+1);
    wcex.lpszMenuName   = MAKEINTRESOURCEW(IDC_1512574PAINT);
    wcex.lpszClassName  = szWindowClass;
    wcex.hIconSm        = LoadIcon(wcex.hInstance, MAKEINTRESOURCE(IDI_SMALL));

    return RegisterClassExW(&wcex);
}

//
//   FUNCTION: InitInstance(HINSTANCE, int)
//
//   PURPOSE: Saves instance handle and creates main window
//
//   COMMENTS:
//
//        In this function, we save the instance handle in a global variable and
//        create and display the main program window.
//
BOOL InitInstance(HINSTANCE hInstance, int nCmdShow)
{
   hInst = hInstance; // Store instance handle in our global variable

   HWND hWnd = CreateWindowW(szWindowClass, szTitle, WS_OVERLAPPEDWINDOW,
      CW_USEDEFAULT, 0, CW_USEDEFAULT, 0, nullptr, nullptr, hInstance, nullptr);

   if (!hWnd)
   {
      return FALSE;
   }

   ShowWindow(hWnd, nCmdShow);
   UpdateWindow(hWnd);

   return TRUE;
}

//
//  FUNCTION: WndProc(HWND, UINT, WPARAM, LPARAM)
//
//  PURPOSE:  Processes messages for the main window.
//
//  WM_COMMAND  - process the application menu
//  WM_PAINT    - Paint the main window
//  WM_DESTROY  - post a quit message and return
//
//
LRESULT CALLBACK WndProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam)
{
    switch (message)
    {
	case WM_CREATE:
	{
		SetWindowText(hWnd, L"New Paint Symple (unsave)");

		hMenu = GetMenu(hWnd);

		//chỉnh chức năng mặc định lúc vào là vẽ đường thẳng
		HandleModePaint(line);

		//Thêm các thuộc tính cho Popup
		hPopupMenu = CreatePopupMenu();
		InsertMenu(hPopupMenu, 0, MF_CHECKED, ID_PAINT_LINE, L"Line");
		InsertMenu(hPopupMenu, 1, MF_UNCHECKED, ID_PAINT_Ellipse, L"Ellipse");
		InsertMenu(hPopupMenu, 2, MF_UNCHECKED, ID_PAINT_RECTANGLE, L"Rectangle");
	}
		break;
	case WM_MOUSEMOVE:
		if (g_isDrawing)
		{
			g_xEnd = GET_X_LPARAM(lParam);
			g_yEnd = GET_Y_LPARAM(lParam);
			g_ShapePrototype.Prototype[g_ShapeChoise]->SetPosition(CPoint2D(g_xStart, g_yStart), CPoint2D(g_xEnd, g_yEnd));
			//SendMessage(hWnd, WM_PAINT, 0, 0);
			RECT rect;
			GetClientRect(hWnd,&rect);
			InvalidateRect(hWnd, &rect, false);
			//SendMessage(hWnd, WM_PAINT, 0, 0);
		}
		break;
	case WM_LBUTTONDOWN:
		g_isDrawing = TRUE;
		g_xStart = GET_X_LPARAM(lParam);
		g_yStart = GET_Y_LPARAM(lParam);

		break;
	case WM_LBUTTONUP:
		if (g_isDrawing)
		{
			g_xEnd = GET_X_LPARAM(lParam);
			g_yEnd = GET_Y_LPARAM(lParam);
			g_isDrawing = FALSE;
			//lưu hình vào
			g_ListObject.push_back(g_ShapeFatory.GetShape(g_ShapeChoise, g_xStart, g_yStart, g_xEnd, g_yEnd));
			RECT rect;
			GetClientRect(hWnd, &rect);
			InvalidateRect(hWnd, &rect, TRUE);
			//SendMessage(hWnd, WM_PAINT, 0, 0);
		}
		break;
	case WM_KEYDOWN:
	{
		//Nếu nút shift được nhấn
		if (wParam == VK_SHIFT)
		{
			if (g_ShapeChoise == rectangle)
				g_ShapeChoise = square;
			if (g_ShapeChoise == ellipse)
				g_ShapeChoise = circle;
		}
	}
		break;
	case WM_KEYUP:
	{
		//nếu nút shift được thả ra
		if (wParam == VK_SHIFT)
		{
			if (g_ShapeChoise == square)
				g_ShapeChoise = rectangle;
			if (g_ShapeChoise == circle)
				g_ShapeChoise = ellipse;
		}
		break;
	}
	case WM_RBUTTONUP:
	{
		POINT point;

		//lấy tọa độ thả chuột phải trên màn hình chứa
		point.x = GET_X_LPARAM(lParam);
		point.y = GET_Y_LPARAM(lParam);

		//chuyển tọa độ màn hình chứa sang màn hình chính
		ClientToScreen(hWnd, &point);

		//Hiển thị popup
		TrackPopupMenu(hPopupMenu, TPM_BOTTOMALIGN, point.x, point.y, 0, hWnd, NULL);
	}
		break;
    case WM_COMMAND:
        {
            int wmId = LOWORD(wParam);
            // Parse the menu selections:
            switch (wmId)
            {
            case IDM_ABOUT:
                DialogBox(hInst, MAKEINTRESOURCE(IDD_ABOUTBOX), hWnd, About);
                break;
            case IDM_EXIT:
				SendMessage(hWnd, WM_CLOSE, NULL, NULL);
                break;
			case ID_FILE_NEW:
			{
				SetWindowText(hWnd, L"New Paint Symple (unsave)");
				//xóa tất cả phần tử trước khi tạo lại trang
				while (!g_ListObject.empty())
				{
					delete g_ListObject[g_ListObject.size() - 1];
					g_ListObject.pop_back();
				}
				//cập nhật lại tên file thành rỗng
				wsprintf(fileName, L"");

				//Vẽ lại windown
				RECT rect;
				GetClientRect(hWnd, &rect);
				InvalidateRect(hWnd, &rect, true);
				//SendMessage(hWnd, WM_PAINT, 0, 0);
			}
				break;
			case ID_FILE_LOAD:
			{
				if (DialogOpenFile(NULL, fileName))
				{
					LoadOject(fileName);
					SetWindowText(hWnd, fileName);
					RECT rect;
					GetClientRect(hWnd, &rect);
					InvalidateRect(hWnd, &rect, true);
					//SendMessage(hWnd, WM_PAINT, 0, 0);
				}
				else
				{
					MessageBeep(MB_ICONEXCLAMATION);
					MessageBox(hWnd, L"Không thể lưu file, thử đường dẫn khác!", L"Thông báo", MB_OK);
				}
			}
			break;
			case ID_FILE_SAVE:
			{
				if (lstrcmpW(fileName, L"") == 0)
				{
					if (DiaLogSaveFile(hWnd, fileName))
					{
						SaveOject(fileName);
						SetWindowText(hWnd, fileName);
					}
				}
				else
				{
					SaveOject(fileName);
				}
				break;
			}
			case ID_FILE_SAVEASNEWPAINT:
				if (DiaLogSaveFile(hWnd, fileName))
				{
					SaveOject(fileName);
					SetWindowText(hWnd, fileName);
				}
				break;
			case ID_EDIT_UNDO:
			{
				if (!g_ListObject.empty())
				{
					delete g_ListObject[g_ListObject.size() - 1];
					g_ListObject.pop_back();
					RECT rect;
					GetClientRect(hWnd, &rect);
					InvalidateRect(hWnd, &rect, true);
					//SendMessage(hWnd, WM_PAINT, 0, 0);
				}
				break;
			}
			case ID_PAINT_LINE:
				HandleModePaint(line);
				break;
			case ID_PAINT_Ellipse:
				HandleModePaint(ellipse);
				break;
			case ID_PAINT_RECTANGLE:
				HandleModePaint(rectangle);
				break;
            default:
                return DefWindowProc(hWnd, message, wParam, lParam);
            }
        }
        break;
    case WM_PAINT:
        {
            PAINTSTRUCT ps;
            HDC hdc = BeginPaint(hWnd, &ps);
			RECT rect;
			GetClientRect(hWnd, &rect);
			HBITMAP hBitmap = CreateCompatibleBitmap(hdc, rect.right, rect.bottom);
			//tạo bộ nhớ để vẽ vào trước khi show
			HDC memDC = CreateCompatibleDC(hdc);

			SelectObject(memDC, hBitmap);
			HPEN hPen = CreatePen(PS_SOLID, 2, RGB(255,0,0));
			SelectObject(memDC, GetStockObject(NULL_BRUSH));
			SelectObject(memDC, hPen);

			//tô trắng toàn màn hình
			HBRUSH hBrush = CreateSolidBrush(RGB(255, 255, 255));
			FillRect(memDC, &rect, hBrush);
			DeleteObject(hBrush);
			DeleteBrush(hBrush);

			// Vẽ lại những gì trong bộ nhớ
			for (int i = 0; i < g_ListObject.size(); i++)
			{
				g_ListObject[i]->Draw(memDC);
			}

			// Vẽ preview nếu đang vẽ
			if(g_isDrawing)
				g_ShapePrototype.Prototype[g_ShapeChoise]->Draw(memDC);

			//chuyển từ bộ nhớ memDC sang màn hình
			BitBlt(hdc, 0, 0,rect.right,rect.bottom, memDC, 0, 0, SRCCOPY);

			DeleteObject(hBitmap);
			DeleteObject(hPen);
			DeleteObject(memDC);

			DeleteBitmap(hBitmap);
			DeleteDC(memDC);
            EndPaint(hWnd, &ps);
        }
        break;
    case WM_DESTROY:
        PostQuitMessage(0);
        break;
	case WM_CLOSE:
	{
		int choise = MessageBox(hWnd, L"Bạn có muốn lưu trước khi thoát?", L"Thông báo!", MB_YESNOCANCEL);
		switch (choise)
		{
		case IDNO:
			//xóa tất cả phần tử trước khi thoát
			for (int i = 0; i < g_ListObject.size(); i++)
			{
				delete g_ListObject[i];
			}

			//thoát
			PostQuitMessage(0);
			break;
		case IDYES:
			//lưu lại
			if (DiaLogSaveFile(hWnd, fileName))
			{
				SaveOject(fileName);
			}
			else
			{
				break;
			}

			//xóa tất cả phần tử trước khi thoát
			for (int i = 0; i < g_ListObject.size(); i++)
			{
				delete g_ListObject[i];
			}

			//thoát
			PostQuitMessage(0);
			break;
		case IDCANCEL:
			break;
		default:
			break;
		}
		//DestroyWindow(hWnd);
		break;
	}
    default:
        return DefWindowProc(hWnd, message, wParam, lParam);
    }
    return 0;
}

// Message handler for about box.
INT_PTR CALLBACK About(HWND hDlg, UINT message, WPARAM wParam, LPARAM lParam)
{
    UNREFERENCED_PARAMETER(lParam);
    switch (message)
    {
    case WM_INITDIALOG:
        return (INT_PTR)TRUE;

    case WM_COMMAND:
        if (LOWORD(wParam) == IDOK || LOWORD(wParam) == IDCANCEL)
        {
            EndDialog(hDlg, LOWORD(wParam));
            return (INT_PTR)TRUE;
        }
        break;
    }
    return (INT_PTR)FALSE;
}


bool SaveOject(WCHAR fileName[])
{
	fstream f(fileName, ios_base::out);
	if (!f.is_open())
		return false;

	for (int i = 0; i < g_ListObject.size(); i++)
	{
		g_ListObject[i]->SaveToFile(f);
	}

	f << -1;
	f.close();
	return true;
}

bool LoadOject(WCHAR fileName[])
{

	//xóa tất cả phần tử hiện tại
	while (!g_ListObject.empty())
	{
		delete g_ListObject[g_ListObject.size() - 1];
		g_ListObject.pop_back();
	}

	fstream f(fileName, ios_base::in);
	if (!f.is_open())
		return false;

	int temp;
	CShape *shapeTemp;
	while (!f.eof())
	{
		f >> temp;
		if (temp == -1)
			break;
		switch (temp)
		{
		case square:
			shapeTemp = new CSquare();
			shapeTemp->LoadFromFile(f);
			break;
		case rectangle:
			shapeTemp = new CRectangle();
			shapeTemp->LoadFromFile(f);
			break;
		case ellipse:
			shapeTemp = new CEllipse();
			shapeTemp->LoadFromFile(f);
			break;
		case circle:
			shapeTemp = new CCircle();
			shapeTemp->LoadFromFile(f);
			break;
		case line:
			shapeTemp = new Cline();
			shapeTemp->LoadFromFile(f);
			break;
		default:
			break;
		}
		g_ListObject.push_back(shapeTemp);
	}
	f.close();
	return true;
}

BOOL DialogOpenFile(HWND hwnd, WCHAR fileName[])
{
	OPENFILENAMEW ofn = { 0 };
	WCHAR szFile[260];
	ofn.lStructSize = sizeof(ofn);
	ofn.hwndOwner = hwnd;
	ofn.lpstrFile = szFile;
	ofn.lpstrFile[0] = '\0';
	ofn.nMaxFile = sizeof(szFile);
	ofn.lpstrFilter = L"Paint Symple (*.paint)\0*.paint\0All File (*.*)\0*.*\0";
	ofn.nFilterIndex = 1;
	ofn.lpstrFileTitle = NULL;
	ofn.nMaxFileTitle = 0;
	ofn.lpstrInitialDir = NULL;
	ofn.Flags = OFN_PATHMUSTEXIST | OFN_FILEMUSTEXIST;
	if (!GetOpenFileNameW(&ofn))
		return FALSE;
	wsprintf(fileName, L"%s", ofn.lpstrFile);
	//wcsncpy(chuoiten,ofn.lpstrFile,lstrlenW(ofn.lpstrFile));
	return TRUE;
}

BOOL DiaLogSaveFile(HWND hwnd, WCHAR fileName[])
{
	OPENFILENAMEW ofn = { 0 };
	WCHAR szFile[260] = TEXT("Paint Symple.paint");
	ofn.lStructSize = sizeof(ofn);
	ofn.hwndOwner = hwnd;
	ofn.lpstrFile = szFile;
	ofn.nMaxFile = sizeof(szFile);
	ofn.lpstrFilter = TEXT("Paint Symple (*.paint)\0*.paint\0All File (*.*)\0*.*\0");
	ofn.nFilterIndex = 1;
	ofn.lpstrFileTitle = NULL;
	ofn.nMaxFileTitle = 0;
	ofn.lpstrInitialDir = NULL;
	ofn.Flags = OFN_PATHMUSTEXIST | OFN_FILEMUSTEXIST;
	if (GetSaveFileName(&ofn) == FALSE)
		return FALSE;
	wsprintf(fileName, L"%s", ofn.lpstrFile);
	return TRUE;
}

void HandleModePaint(TypeShape type)
{
	switch (type)
	{
	case rectangle:
		CheckMenuItem(hMenu, ID_PAINT_Ellipse, MF_UNCHECKED);
		CheckMenuItem(hMenu, ID_PAINT_LINE, MF_UNCHECKED);

		CheckMenuItem(hMenu, ID_PAINT_RECTANGLE, MF_CHECKED);


		//xóa hết những gì trong popup
		DeleteMenu(hPopupMenu, 0, MF_BYPOSITION);
		DeleteMenu(hPopupMenu, 0, MF_BYPOSITION);
		DeleteMenu(hPopupMenu, 0, MF_BYPOSITION);

		//thêm lại item vào popup
		InsertMenu(hPopupMenu, 0, MF_UNCHECKED, ID_PAINT_LINE, L"Line");
		InsertMenu(hPopupMenu, 1, MF_UNCHECKED, ID_PAINT_Ellipse, L"Ellipse");
		InsertMenu(hPopupMenu, 2, MF_CHECKED, ID_PAINT_RECTANGLE, L"Rectangle");

		g_ShapeChoise = rectangle;
		break;
	case ellipse:
		CheckMenuItem(hMenu, ID_PAINT_Ellipse, MF_CHECKED);
		CheckMenuItem(hMenu, ID_PAINT_LINE, MF_UNCHECKED);

		CheckMenuItem(hMenu, ID_PAINT_RECTANGLE, MF_UNCHECKED);

		//xóa hết những gì trong popup
		DeleteMenu(hPopupMenu, 0, MF_BYPOSITION);
		DeleteMenu(hPopupMenu, 0, MF_BYPOSITION);
		DeleteMenu(hPopupMenu, 0, MF_BYPOSITION);

		//thêm lại item vào popup
		InsertMenu(hPopupMenu, 0, MF_UNCHECKED, ID_PAINT_LINE, L"Line");
		InsertMenu(hPopupMenu, 1, MF_CHECKED, ID_PAINT_Ellipse, L"Ellipse");
		InsertMenu(hPopupMenu, 2, MF_UNCHECKED, ID_PAINT_RECTANGLE, L"Rectangle");

		g_ShapeChoise = ellipse;
		break;
	case line:
		CheckMenuItem(hMenu, ID_PAINT_Ellipse, MF_UNCHECKED);
		CheckMenuItem(hMenu, ID_PAINT_LINE, MF_CHECKED);

		CheckMenuItem(hMenu, ID_PAINT_RECTANGLE, MF_UNCHECKED);

		//xóa hết những gì trong popup
		DeleteMenu(hPopupMenu, 0, MF_BYPOSITION);
		DeleteMenu(hPopupMenu, 0, MF_BYPOSITION);
		DeleteMenu(hPopupMenu, 0, MF_BYPOSITION);

		//thêm lại item vào popup
		InsertMenu(hPopupMenu, 0, MF_CHECKED, ID_PAINT_LINE, L"Line");
		InsertMenu(hPopupMenu, 1, MF_UNCHECKED, ID_PAINT_Ellipse, L"Ellipse");
		InsertMenu(hPopupMenu, 2, MF_UNCHECKED, ID_PAINT_RECTANGLE, L"Rectangle");

		g_ShapeChoise = line;
		break;
	default:
		break;
	}
}
