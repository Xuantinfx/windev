#pragma once
#include "CShape.h"
class CSquare :
	public CShape
{
public:
	void Draw(HDC);
	TypeShape GetType();
	void SaveToFile(fstream &f);
	CSquare();
	CSquare(CPoint2D _point1,CPoint2D _point2);
	CSquare(int _x1, int _y1, int _x2,int _y2);
	~CSquare();
};

