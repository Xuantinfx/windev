#include "stdafx.h"
#include "CSquare.h"


void CSquare::Draw(HDC _hdc)
{
	int width = abs(position2.x - position1.x);
	if (width < abs(position2.y - position1.y))
	{
		width = abs(position2.y - position1.y);
	}

	if(position2.x > position1.x && position2.y > position1.y)
		Rectangle(_hdc, position1.x, position1.y, position1.x + width, position1.y + width);
	else
	if(position2.x > position1.x && position2.y < position1.y)
		Rectangle(_hdc, position1.x, position1.y, position1.x + width, position1.y - width);
	else
	if(position2.x < position1.x && position2.y < position1.y)
		Rectangle(_hdc, position1.x, position1.y, position1.x - width, position1.y - width);
	else
		Rectangle(_hdc, position1.x, position1.y, position1.x - width, position1.y + width);
}

TypeShape CSquare::GetType()
{
	return square;
}

void CSquare::SaveToFile(fstream & f)
{
	f << square << " ";
	CShape::SaveToFile(f);
}

CSquare::CSquare()
{
}

CSquare::CSquare(CPoint2D _point1, CPoint2D _point2)
{
	position1.x = _point1.x;
	position1.y = _point1.y;
	position2.x = _point2.x;
	position2.y = _point2.y;
}

CSquare::CSquare(int _x1, int _y1, int _x2,int _y2)
{
	position1.x = _x1;
	position1.y = _y1;
	position2.x = _x2;
	position2.y = _y2;
}


CSquare::~CSquare()
{
}
