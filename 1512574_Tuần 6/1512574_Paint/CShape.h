﻿#pragma once
#include "CPoint2D.h"
#include <fstream>

enum TypeShape
{
	square,
	rectangle,
	ellipse,
	circle,
	line
};

class CShape
{
public:
	//position1 luôn luôn nằm trái hơn so với positon 2
	CPoint2D position1,position2;

	CShape();
	CShape(CPoint2D,CPoint2D);
	CShape(int _x1, int _y1,int _x2,int _y2);
	void SetPosition(CPoint2D _position1, CPoint2D _position2);
	~CShape();

	virtual void Draw(HDC) = 0;
	virtual TypeShape GetType() = 0;
	virtual void SaveToFile(fstream &f);
	virtual void LoadFromFile(fstream &f);
};

