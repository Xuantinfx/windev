#include "stdafx.h"
#include "CShapeFactory.h"


CShape * CShapeFactory::GetShape(TypeShape _type, int _xStart, int _yStart, int _xEnd, int _yEnd)
{
	CPoint2D position1(_xStart, _yStart), position2(_xEnd, _yEnd);
	switch (_type)
	{
	case square:
	{
		int canh = abs(_xStart - _xEnd);
		if (canh < abs(_yStart - _xEnd));
		{
			canh = abs(_yStart - _xEnd);
		}
		return new CSquare(position1, position2);
	}
	case rectangle:
	{
		return new CRectangle(position1, position2);
	}
	case line:
	{
		return new Cline(position1, position2);
	}
	case ellipse:
	{
		return new CEllipse(position1, position2);
	}
	case circle:
	{
		return new CCircle(position1, position2);
	}
	default: return nullptr;
	}
	return nullptr;
}

CShape * CShapeFactory::GetShape(TypeShape _type, CPoint2D position1, CPoint2D position2)
{
	switch (_type)
	{
	case square:
	{
		int canh = abs(position1.x - position2.x);
		if (canh < abs(position1.y - position2.y));
		{
			canh = abs(position1.y - position2.y);
		}
		return new CSquare(position1, position2);
	}
	case rectangle:
	{
		return new CRectangle(position1, position2);
	}
	case line:
	{
		return new Cline(position1, position2);
	}
	case ellipse:
	{
		return new CEllipse(position1, position2);
	}
	case circle:
	{
		return new CCircle(position1, position2);
	}
	default: return nullptr;
	}
	return nullptr;
}

CShapeFactory::CShapeFactory()
{
}


CShapeFactory::~CShapeFactory()
{
}
