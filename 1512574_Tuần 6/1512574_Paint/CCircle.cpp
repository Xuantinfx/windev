#include "stdafx.h"
#include "CCircle.h"


CCircle::CCircle()
{
}

CCircle::CCircle(CPoint2D _point1, CPoint2D _point2)
{
	position1.x = _point1.x;
	position1.y = _point1.y;
	position2.x = _point2.x;
	position2.y = _point2.y;
}

CCircle::CCircle(int _x1, int _y1, int _x2, int _y2)
{
	position1.x = _x1;
	position1.y = _y1;
	position2.x = _x2;
	position2.y = _y2;
}

void CCircle::Draw(HDC _hdc)
{
	float duongKinh = sqrt((position1.x - position2.x)*(position1.x - position2.x) + (position1.y - position2.y)*(position1.y - position2.y));
	float banKinh = duongKinh / 2;

	if (position2.x > position1.x && position2.y > position1.y)
		Ellipse(_hdc, position1.x, position1.y, position1.x + banKinh, position1.y + banKinh);
	else
	if (position2.x > position1.x && position2.y < position1.y)
		Ellipse(_hdc, position1.x, position1.y, position1.x + banKinh, position1.y - banKinh);
	else
	if (position2.x < position1.x && position2.y < position1.y)
		Ellipse(_hdc, position1.x, position1.y, position1.x - banKinh, position1.y - banKinh);
	else
	if(position2.x < position1.x && position2.y > position1.y)
		Ellipse(_hdc, position1.x, position1.y, position1.x - banKinh, position1.y + banKinh);
}

TypeShape CCircle::GetType()
{
	return circle;
}

void CCircle::SaveToFile(fstream & f)
{
	f << circle << " ";
	CShape::SaveToFile(f);
}


CCircle::~CCircle()
{
}
