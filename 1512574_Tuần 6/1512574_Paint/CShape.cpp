﻿#include "stdafx.h"
#include "CShape.h"


CShape::CShape()
{
	position1.x = position1.y = position2.x = position2.y = 0;
}

CShape::CShape(CPoint2D _point1, CPoint2D _point2)
{
	position1.x = _point1.x;
	position1.y = _point1.y;
	position2.x = _point2.x;
	position2.y = _point2.y;
}

CShape::CShape(int _x1, int _y1,int _x2,int _y2)
{
	position1.x = _x1;
	position1.y = _y1;
	position2.x = _x2;
	position2.y = _y2;
}

void CShape::SetPosition(CPoint2D _position1, CPoint2D _position2)
{
	position1.x = _position1.x;
	position1.y = _position1.y;
	position2.x = _position2.x;
	position2.y = _position2.y;
}


CShape::~CShape()
{
}

void CShape::SaveToFile(fstream & f)
{
	//lưu các điểm
	f << position1.x << " " << position1.y << " " << position2.x << " " << position2.y << " ";
}

void CShape::LoadFromFile(fstream & f)
{
	f >> position1.x;
	f >> position1.y;
	f >> position2.x;
	f >> position2.y;
}
