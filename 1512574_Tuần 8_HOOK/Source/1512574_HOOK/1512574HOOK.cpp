﻿// 1512574HOOK.cpp : Defines the entry point for the application.
//

#include "stdafx.h"
#include "1512574HOOK.h"

#define MAX_LOADSTRING 100

// Global Variables:
HINSTANCE hInst;                                // current instance
WCHAR szTitle[MAX_LOADSTRING];                  // The title bar text
WCHAR szWindowClass[MAX_LOADSTRING];            // the main window class name

// Forward declarations of functions included in this code module:
ATOM                MyRegisterClass(HINSTANCE hInstance);
BOOL                InitInstance(HINSTANCE, int);
LRESULT CALLBACK    WndProc(HWND, UINT, WPARAM, LPARAM);
INT_PTR CALLBACK    About(HWND, UINT, WPARAM, LPARAM);
bool installHook();
bool unInstallHook();
BOOL hooked = FALSE;
HWND hwndSTT = NULL;

int APIENTRY wWinMain(_In_ HINSTANCE hInstance,
                     _In_opt_ HINSTANCE hPrevInstance,
                     _In_ LPWSTR    lpCmdLine,
                     _In_ int       nCmdShow)
{
    UNREFERENCED_PARAMETER(hPrevInstance);
    UNREFERENCED_PARAMETER(lpCmdLine);

    // TODO: Place code here.

    // Initialize global strings
    LoadStringW(hInstance, IDS_APP_TITLE, szTitle, MAX_LOADSTRING);
    LoadStringW(hInstance, IDC_1512574HOOK, szWindowClass, MAX_LOADSTRING);
    MyRegisterClass(hInstance);

    // Perform application initialization:
    if (!InitInstance (hInstance, nCmdShow))
    {
        return FALSE;
    }

    HACCEL hAccelTable = LoadAccelerators(hInstance, MAKEINTRESOURCE(IDC_1512574HOOK));

    MSG msg;

    // Main message loop:
    while (GetMessage(&msg, nullptr, 0, 0))
    {
        if (!TranslateAccelerator(msg.hwnd, hAccelTable, &msg))
        {
            TranslateMessage(&msg);
            DispatchMessage(&msg);
        }
    }

    return (int) msg.wParam;
}



//
//  FUNCTION: MyRegisterClass()
//
//  PURPOSE: Registers the window class.
//
ATOM MyRegisterClass(HINSTANCE hInstance)
{
    WNDCLASSEXW wcex;

    wcex.cbSize = sizeof(WNDCLASSEX);

    wcex.style          = CS_HREDRAW | CS_VREDRAW;
    wcex.lpfnWndProc    = WndProc;
    wcex.cbClsExtra     = 0;
    wcex.cbWndExtra     = 0;
    wcex.hInstance      = hInstance;
    wcex.hIcon          = LoadIcon(hInstance, MAKEINTRESOURCE(IDI_1512574HOOK));
    wcex.hCursor        = LoadCursor(nullptr, IDC_ARROW);
    wcex.hbrBackground  = (HBRUSH)(COLOR_BTNFACE+1);
    wcex.lpszMenuName   = MAKEINTRESOURCEW(IDC_1512574HOOK);
    wcex.lpszClassName  = szWindowClass;
    wcex.hIconSm        = LoadIcon(wcex.hInstance, MAKEINTRESOURCE(IDI_SMALL));

    return RegisterClassExW(&wcex);
}

//
//   FUNCTION: InitInstance(HINSTANCE, int)
//
//   PURPOSE: Saves instance handle and creates main window
//
//   COMMENTS:
//
//        In this function, we save the instance handle in a global variable and
//        create and display the main program window.
//
BOOL InitInstance(HINSTANCE hInstance, int nCmdShow)
{
   hInst = hInstance; // Store instance handle in our global variable

   HWND hWnd = CreateWindowW(szWindowClass, szTitle, WS_OVERLAPPEDWINDOW,
      CW_USEDEFAULT, 0, CW_USEDEFAULT, 0, nullptr, nullptr, hInstance, nullptr);

   if (!hWnd)
   {
      return FALSE;
   }

   ShowWindow(hWnd, nCmdShow);
   UpdateWindow(hWnd);

   return TRUE;
}

//
//  FUNCTION: WndProc(HWND, UINT, WPARAM, LPARAM)
//
//  PURPOSE:  Processes messages for the main window.
//
//  WM_COMMAND  - process the application menu
//  WM_PAINT    - Paint the main window
//  WM_DESTROY  - post a quit message and return
//
//
LRESULT CALLBACK WndProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam)
{
    switch (message)
    {
	case WM_CREATE:
	{
		LOGFONT lf;
		GetObject(GetStockObject(DEFAULT_GUI_FONT), sizeof(LOGFONT), &lf);
		lf.lfHeight = 15;
		lf.lfWidth = 7;
		HFONT hFontBTN = CreateFont(lf.lfHeight, lf.lfWidth,
			lf.lfEscapement, lf.lfOrientation, lf.lfWeight,
			lf.lfItalic, lf.lfUnderline, lf.lfStrikeOut, lf.lfCharSet,
			lf.lfOutPrecision, lf.lfClipPrecision, lf.lfQuality,
			lf.lfPitchAndFamily, lf.lfFaceName);
		lf.lfHeight = 20;
		lf.lfWidth = 9;
		HFONT hFontSTT = CreateFont(lf.lfHeight, lf.lfWidth,
			lf.lfEscapement, lf.lfOrientation, lf.lfWeight,
			lf.lfItalic, lf.lfUnderline, lf.lfStrikeOut, lf.lfCharSet,
			lf.lfOutPrecision, lf.lfClipPrecision, lf.lfQuality,
			lf.lfPitchAndFamily, lf.lfFaceName);
		HWND hwndtemp;
		hwndtemp = CreateWindow(L"BUTTON", L"Click me!", WS_CHILD | WS_VISIBLE | BS_PUSHBUTTON, 10, 10, 100, 20, hWnd, (HMENU)IDC_BTN, hInst, NULL);
		SendMessage(hwndtemp, WM_SETFONT, (LPARAM)hFontBTN, NULL);

		hwndtemp = CreateWindow(L"STATIC", L"Nhấn F1 để Bật/Tắt chuột trái", WS_CHILD | WS_VISIBLE, 140, 80, 400, 20, hWnd, NULL, hInst, NULL);
		SendMessage(hwndtemp, WM_SETFONT, (LPARAM)hFontSTT, NULL);

		hwndSTT = CreateWindow(L"STATIC", L"Chuột trái đang được bật", WS_VISIBLE | WS_CHILD, 140, 120, 400, 20, hWnd, NULL, hInst, NULL);
		SendMessage(hwndSTT, WM_SETFONT, (LPARAM)hFontSTT, NULL);
		break;
	}
	case WM_KEYDOWN:
	{
		if ((wParam&VK_F1 )&& (wParam == VK_F1) )
		{
			if (hooked) {
				hooked = !hooked;
				SetWindowText(hwndSTT, L"Chuột trái đang bật");
				unInstallHook();
			}
			else
			{
				hooked = !hooked;
				SetWindowText(hwndSTT, L"Chuột trái đang tắt");
				installHook();
			}
		}
		break;
	}
    case WM_COMMAND:
        {
            int wmId = LOWORD(wParam);
            // Parse the menu selections:
            switch (wmId)
            {
			case IDC_BTN:
			{
				MessageBox(hWnd, L"Bạn vừa nhấn chuột trái vào Button Click me!", L"Thông báo", MB_OK);
				break;
			}
            case IDM_ABOUT:
                DialogBox(hInst, MAKEINTRESOURCE(IDD_ABOUTBOX), hWnd, About);
                break;
            case IDM_EXIT:
                DestroyWindow(hWnd);
                break;
            default:
                return DefWindowProc(hWnd, message, wParam, lParam);
            }
        }
        break;
    case WM_PAINT:
        {
            PAINTSTRUCT ps;
            HDC hdc = BeginPaint(hWnd, &ps);
            // TODO: Add any drawing code that uses hdc here...
            EndPaint(hWnd, &ps);
        }
        break;
	case WM_CLOSE:
		unInstallHook();
		PostQuitMessage(0);
		break;
    case WM_DESTROY:
		unInstallHook();
        PostQuitMessage(0);
        break;
    default:
        return DefWindowProc(hWnd, message, wParam, lParam);
    }
    return 0;
}

// Message handler for about box.
INT_PTR CALLBACK About(HWND hDlg, UINT message, WPARAM wParam, LPARAM lParam)
{
    UNREFERENCED_PARAMETER(lParam);
    switch (message)
    {
    case WM_INITDIALOG:
        return (INT_PTR)TRUE;

    case WM_COMMAND:
        if (LOWORD(wParam) == IDOK || LOWORD(wParam) == IDCANCEL)
        {
            EndDialog(hDlg, LOWORD(wParam));
            return (INT_PTR)TRUE;
        }
        break;
    }
    return (INT_PTR)FALSE;
}


bool installHook() {
	typedef BOOL(*PROC)();

	HINSTANCE hInstLib = NULL;
	PROC procAddr = NULL;
	hInstLib = LoadLibrary(L"DLL.dll");
	if (hInstLib != NULL) {
		procAddr = (PROC)GetProcAddress(hInstLib, "InitMouseHook");
		if (procAddr != NULL) {
			return procAddr();
		}
	}
	return false;
}

bool unInstallHook() {
	typedef BOOL(*PROC)();

	HINSTANCE hInstLib = NULL;
	PROC procAddr = NULL;
	hInstLib = LoadLibrary(L"DLL.dll");
	if (hInstLib != NULL) {
		procAddr = (PROC)GetProcAddress(hInstLib, "UninstallMouseHook");
		if (procAddr != NULL) {
			return procAddr();
		}
	}
	return false;
}