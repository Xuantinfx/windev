﻿// DLL.cpp : Defines the exported functions for the DLL application.
//

#include "stdafx.h"
#include <Windows.h>

#define EXPORT __declspec(dllexport)

HHOOK		hHook		= NULL;
HINSTANCE	hInstLib;

LRESULT CALLBACK MouseHookProc(int nCode, WPARAM wParam, LPARAM lParam)
{
	if (nCode == HC_ACTION)
	{
		switch (wParam)
		{
		case WM_LBUTTONDOWN: //bắt sự kiện bấm chuột ở ngoài cửa sổ
		{
			return -1;
		}
		case WM_NCLBUTTONDOWN: //bắt sự kiện bấm chuột ở trong cửa sổ
		{
			return -1;
		}
		case WM_LBUTTONUP: //bắt sự kiện bấm chuột ở ngoài cửa sổ
		{
			return -1;
		}
		case WM_NCLBUTTONUP: //bắt sự kiện bấm chuột ở trong cửa sổ
		{
			return -1;
		}
		default:
			break;
		}
	}
	return CallNextHookEx(0, nCode, wParam, lParam);
}

EXPORT INT InitMouseHook()
{
	hHook = SetWindowsHookEx(WH_MOUSE, MouseHookProc, hInstLib, 0);
	if (hHook != NULL)
	{
		MessageBox(NULL, L"Chuột Trái đã được tắt!", L"Thông báo", MB_OK);
		return 1;
	}
	return 0;
}

EXPORT INT UninstallMouseHook()
{
	if (hHook != NULL)
	{
		UnhookWindowsHookEx(hHook);
		MessageBox(NULL, L"Chuột trái đã được bật!", L"Thông báo", MB_OK);
		return 1;
	}
	return 0;
}
