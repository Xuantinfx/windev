# MSSV: 1512574 - Đào Xuân Tin - xuantinfx@gmail.com

# Tuần 8: HOOK - Vô hiệu hóa chuột trái

# Chức năng làm được
 1. Nhấn F1 thì sẽ vô hiệu hóa chuột trái và nhấn F1 1 lần nữa thì sẽ kích hoạt lại chuột trái bình thường
 2. Bỏ các hàm Hook như installHook và unInstallHook và DLL

# Chức năng chưa làm được
 Không có

# Luồng sự kiện phụ
 1. Tạo một button để test chuột trái, lúc chưa vô hiệu hóa mà click vào Button Click me! thì sẽ có thông báo "Bạn vừa click trái vào button Click Me!", và khi vô hiệu hóa thì không thể click.
 2. Có Stt thông báo nhấn F1 để Tắt/Bật chức năng chuột trái.
 3. Có Stt thông báo tình trạng HOOk như nào, đang tắt chuột trái hay là bật chuột trái.

# Link demo youtube: https://www.youtube.com/watch?v=q7GGnNZXZQ0&feature=youtu.be
# Link Bitbucket: https://Xuantinfx@bitbucket.org/Xuantinfx/windev.git