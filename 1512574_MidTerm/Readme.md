# Đào Xuân Tin - 1512574 - xuantinfx@gmail.com

# Bài tập tuần giữa kì - Quản lý chi tiêu

# Những gì đã làm được

1. Thêm chi tiêu
    + Loại chi tiêu sẽ hiện các chi tiêu mặc định,
      người dùng có thể nhập thêm nội dung chi tiêu

    + Có giá tiền và thêm ngày chi

    + Nếu người dùng nhập thiếu hoặc sai thì sẽ thông
      báo và nhập lại

    + Hỗ trợ nhập Tiếng Việt

2. Hiển thị các chi tiêu đã thêm ở ListView bằng Tiếng Việt
nếu chi tiêu được nhập bằng Tiếng Việt

3. Hiển thị chi tiêu trong 1 tháng, mặc định là từ ngày 1,
người dùng có thể đặt lại trong phần Setting->Start Date

4. Khi chương trình tắt thì tự động lưu lại và tự load lên
lại khi ứng dụng được bật.

5. Vẽ biểu đồ hình tròn với chú thích có hiển thị phần trăm
của Loại chi tiêu.

6. Một số chức năng phụ như chức năng Back Up dữ liệu và
Restore, Reset dữ liệu, xóa 1 chi tiêu, sort theo Loại
chi tiêu, Nội dung chi, số tiền và ngày chi.

# Link youtube Demo: https://www.youtube.com/watch?v=Iy_KMMG63Xo&feature=youtu.be

# Hướng dẫn sử dụng: 
1. Thêm chi tiêu và chọn ngày chi, đặt ngày bắt đầu tính tiền

# Các ghi chú khác: 
1. Tổng chi tiêu trong tháng là tổng chi tiêu kể từ ngày bắt đầu đến bây giờ.

# Link repository: https://Xuantinfx@bitbucket.org/Xuantinfx/windev.git