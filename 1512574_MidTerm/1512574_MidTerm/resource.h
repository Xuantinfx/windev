//{{NO_DEPENDENCIES}}
// Microsoft Visual C++ generated include file.
// Used by 1512574MidTerm.rc
//
#define IDC_MYICON                      2
#define IDD_1512574MIDTERM_DIALOG       102
#define IDS_APP_TITLE                   103
#define IDD_ABOUTBOX                    103
#define IDM_ABOUT                       104
#define IDM_EXIT                        105
#define IDI_1512574MIDTERM              107
#define IDI_SMALL                       108
#define IDC_1512574MIDTERM              109
#define IDC_BTN_INSERT                  110
#define IDC_CBB_LOAICHITIEU             111
#define IDC_BTN_DELETE                  112
#define IDC_BTN_EDIT                    113
#define IDR_MAINFRAME                   128
#define IDD_DIALOG1                     129
#define IDI_ICON1                       132
#define IDI_ICON2                       133
#define IDI_ICON3                       134
#define IDI_ICON4                       135
#define IDI_ICON5                       136
#define IDI_ICON6                       137
#define IDD_STARTDAY                    138
#define ID_OK                           1001
#define ID_CANCLE                       1002
#define ID_CANCEL                       1002
#define ID_SETING                       32771
#define ID_SETING_RESET                 32774
#define ID_FILE_IMPORT                  32775
#define ID_SETING_COLOR                 32776
#define ID_FILE_BACKUP                  32777
#define ID_SETING_                      32778
#define ID_SETING_STARTDAY              32779
#define IDC_STATIC                      -1

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NO_MFC                     1
#define _APS_NEXT_RESOURCE_VALUE        139
#define _APS_NEXT_COMMAND_VALUE         32780
#define _APS_NEXT_CONTROL_VALUE         1003
#define _APS_NEXT_SYMED_VALUE           114
#endif
#endif
