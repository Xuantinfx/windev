﻿#pragma once

#include "resource.h"

extern INT g_MaChitieu;
class MYDATE
{
public:
	INT dd, mm, yy;
	MYDATE();
	~MYDATE();
	MYDATE(WCHAR *strDate) {
		WCHAR temp[10];
		int i = 0;
		//lấy tháng
		int j = 0;
		while (!(strDate[i] ==L'\0' || strDate[i] == L'\n' || strDate[i]==L'/' || strDate[i] == L'-' || strDate[i] == L' ' || strDate[i] == L'\\') )
		{
			temp[j++] = strDate[i++];
		}
		i++;
		temp[j++] = L'\0';
		mm = _wtoi(temp);
		//lấy ngày
		j = 0;
		while (!(strDate[i] == L'\0' || strDate[i] == L'\n' || strDate[i] == L'/' || strDate[i] == L'-' || strDate[i] == L' ' || strDate[i] == L'\\'))
		{
			temp[j++] = strDate[i++];
		}
		i++;
		temp[j++] = L'\0';
		dd = _wtoi(temp);

		//lấy năm
		j = 0;
		while (!(strDate[i] == L'\0' || strDate[i] == L'\n' || strDate[i] == L'/' || strDate[i] == L'-' || strDate[i] == L' ' || strDate[i] == L'\\'))
		{
			temp[j++] = strDate[i++];
		}
		temp[j++] = L'\0';
		yy = _wtoi(temp);
	}
	VOID SetDate(WCHAR *strDate) {
		WCHAR temp[10];
		int i = 0;
		//lấy tháng
		int j = 0;
		while (!(strDate[i] == L'\0' || strDate[i] == L'\n' || strDate[i] == L'/' || strDate[i] == L'-' || strDate[i] == L' ' || strDate[i] == L'\\'))
		{
			temp[j++] = strDate[i++];
		}
		i++;
		temp[j++] = L'\0';
		mm = _wtoi(temp);
		//lấy ngày
		j = 0;
		while (!(strDate[i] == L'\0' || strDate[i] == L'\n' || strDate[i] == L'/' || strDate[i] == L'-' || strDate[i] == L' ' || strDate[i] == L'\\'))
		{
			temp[j++] = strDate[i++];
		}
		i++;
		temp[j++] = L'\0';
		dd = _wtoi(temp);

		//lấy năm
		j = 0;
		while (!(strDate[i] == L'\0' || strDate[i] == L'\n' || strDate[i] == L'/' || strDate[i] == L'-' || strDate[i] == L' ' || strDate[i] == L'\\'))
		{
			temp[j++] = strDate[i++];
		}
		temp[j++] = L'\0';
		yy = _wtoi(temp);
	}
};

MYDATE::MYDATE()
{
	dd = 1;
	mm = 1;
	yy = 1990;
}

MYDATE::~MYDATE()
{
}

class CHITIEU
{
public:
	WCHAR LoaiChiTieu[50];
	WCHAR NoiDungChi[100];
	WCHAR SoTien[50];
	WCHAR Date[50];
	INT MACHITIEU;
	CHITIEU();
	CHITIEU(WCHAR*_Loai,WCHAR*_NoiDUng,WCHAR *_SoTien,WCHAR* _Date);
	~CHITIEU();
	VOID Save(std::fstream &fout) {
		fout.write((char*)LoaiChiTieu, 50);
		fout.write((char*)NoiDungChi, 100);
		fout.write((char*)SoTien, 50);
		fout.write((char*)Date, 50);
	}
	BOOL Read(std::fstream &fin) {
		fin.read((char*)LoaiChiTieu, 50);
		if (!lstrcmpW(LoaiChiTieu, L""))
		{
			return FALSE;
		}
		fin.read((char*)NoiDungChi, 100);
		fin.read((char*)SoTien, 50);
		fin.read((char*)Date, 50);
		return TRUE;
	}
	VOID SetLoaiChiTieu(WCHAR * _Loai) {
		for (int i = 0; i <= lstrlenW(_Loai); i++)
		{
			LoaiChiTieu[i] = _Loai[i];
		}
	}
	VOID SetNoiDungChi(WCHAR* _NoiDung) {
		for (int i = 0; i <= lstrlenW(_NoiDung); i++)
		{
			NoiDungChi[i] = _NoiDung[i];
		}
	}
	VOID SetSoTien(WCHAR* _SoTien) {
		for (int i = 0; i <= lstrlenW(_SoTien); i++)
		{
			SoTien[i] = _SoTien[i];
		}
	}
	VOID SetDate(WCHAR *_Date) {
		for (int i = 0; i <= lstrlenW(_Date); i++)
		{
			Date[i] = _Date[i];
		}
	}
	INT GetSoTien() {
		INT result = _wtoi(SoTien);
		return result;
	}
};

CHITIEU::CHITIEU(WCHAR*_Loai, WCHAR*_NoiDung, WCHAR *_SoTien, WCHAR* _Date)
{
	MACHITIEU = g_MaChitieu++;
	for (int i = 0; i <= lstrlenW(_Loai); i++)
	{
		LoaiChiTieu[i] = _Loai[i];
	}
	for (int i = 0; i <= lstrlenW(_NoiDung); i++)
	{
		NoiDungChi[i] = _NoiDung[i];
	}
	for (int i = 0; i <= lstrlenW(_SoTien); i++)
	{
		SoTien[i] = _SoTien[i];
	}
	for (int i = 0; i <= lstrlenW(_Date); i++)
	{
		Date[i] = _Date[i];
	}
}

CHITIEU::CHITIEU()
{
	LoaiChiTieu[0]	= 0;
	NoiDungChi[0]	= 0;
	SoTien[0]		= 0;
	Date[0]			= 0;
	MACHITIEU = g_MaChitieu++;
}
CHITIEU::~CHITIEU()
{
}