﻿// 1512574MidTerm.cpp : Defines the entry point for the application.
//

#include "stdafx.h"
#include "1512574MidTerm.h"

#define MAX_LOADSTRING 100
#define WIDTH 1000
#define HEIGHT 600

// Global Variables:
HINSTANCE hInst;                                // current instance
WCHAR szTitle[MAX_LOADSTRING];                  // The title bar text
WCHAR szWindowClass[MAX_LOADSTRING];            // the main window class name

/*Khu vực khai báo biến Global*/
HWND g_ListView			= NULL;
HWND g_LssTongChiTieu	= NULL;
HWND g_LoaiChiTieu		= NULL;
HWND g_NoiDungChi		= NULL;
HWND g_SoTien			= NULL;
HWND g_NgayChi			= NULL;
HWND g_LAU				= NULL;
HWND g_LDC				= NULL;
HWND g_LNC				= NULL;
HWND g_LXC				= NULL;
HWND g_LNYP				= NULL;
HWND g_LDV				= NULL;
HWND g_Bdelete			= NULL;
INT g_MaChitieu			= 0;
HWND g_Noti				= NULL;
HWND g_getStartDate		= NULL;
HWND g_DateThongBao		= NULL;
INT g_StartDate			= 1;
INT g_CountLV			= 0;
INT g_C_AU[3]			= {	132,94,194 };
INT g_C_DC[3]			= { 214,93,177 };
INT g_C_NC[3]			= { 44,115,210 };
INT g_C_XC[3]			= { 0,143,122 };
INT g_C_NYP[3]			= { 176,168,185 };
INT g_C_DV[3]			= { 79,251,223 };
BOOL is_TangLCT			= 0;
BOOL is_TangNDC			= 0;
BOOL is_TangST			= 0;
BOOL is_TangD			= 0;
HIMAGELIST g_hml32;
std::vector<CHITIEU*> g_DS_ChiTieu;
std::vector<WCHAR*> g_DS_SUB_AU;
std::vector<WCHAR*> g_DS_SUB_DC;
std::vector<WCHAR*> g_DS_SUB_NC;
std::vector<WCHAR*> g_DS_SUB_XC;
std::vector<WCHAR*> g_DS_SUB_NYP;
std::vector<WCHAR*> g_DS_SUB_DV;

BOOL OnCreate(HWND hwnd, LPCREATESTRUCT lpCreateStruct);
VOID OnCommand(HWND hwnd, int id, HWND hwndCtl, UINT codeNotify);
VOID OnPaint(HWND hwnd);
VOID OnDestroy(HWND hwnd);
VOID OnClose(HWND hwnd);
BOOL is_Number(WCHAR *str);
BOOL saveData(WCHAR *fileName);
BOOL readData(WCHAR *fileName);
VOID Insert_LVItem(CHITIEU *chitieu);
FLOAT CaculatePercent(WCHAR *LoaiChiTieu);
WCHAR *FloatToW(float number, int moRong);
BOOL findData(std::vector<WCHAR*> vt, WCHAR*data);
BOOL loadDataSubThemChiTieu(WCHAR* fileName);
BOOL saveDataSubThemChiTieu(WCHAR* fileName);
VOID KhoiTaoCBBNoiDungChi();
VOID ReLoadCBBNoiDungChiTieu();
VOID ReLoadListView();
void TaoReSourchIcon();
BOOL DialogOpenFile(HWND hwnd, WCHAR fileName[]);
BOOL DiaLogSaveFile(HWND hwnd, WCHAR fileName[]);
VOID NbStrToStr(WCHAR *numberStrTo, WCHAR *numberStrSrc);
VOID UpdateTotalForMount();

// Forward declarations of functions included in this code module:
ATOM                MyRegisterClass(HINSTANCE hInstance);
BOOL                InitInstance(HINSTANCE, int);
LRESULT CALLBACK    WndProc(HWND, UINT, WPARAM, LPARAM);
INT_PTR CALLBACK    About(HWND, UINT, WPARAM, LPARAM);

int APIENTRY wWinMain(_In_ HINSTANCE hInstance,
                     _In_opt_ HINSTANCE hPrevInstance,
                     _In_ LPWSTR    lpCmdLine,
                     _In_ int       nCmdShow)
{
    UNREFERENCED_PARAMETER(hPrevInstance);
    UNREFERENCED_PARAMETER(lpCmdLine);

    // TODO: Place code here.
	GdiplusStartupInput gdiplusStartupInput;
	ULONG_PTR           gdiplusToken;
	GdiplusStartup(&gdiplusToken, &gdiplusStartupInput, NULL);

    // Initialize global strings
    LoadStringW(hInstance, IDS_APP_TITLE, szTitle, MAX_LOADSTRING);
    LoadStringW(hInstance, IDC_1512574MIDTERM, szWindowClass, MAX_LOADSTRING);
    MyRegisterClass(hInstance);

    // Perform application initialization:
    if (!InitInstance (hInstance, nCmdShow))
    {
        return FALSE;
    }

    HACCEL hAccelTable = LoadAccelerators(hInstance, MAKEINTRESOURCE(IDC_1512574MIDTERM));

    MSG msg;

    // Main message loop:
    while (GetMessage(&msg, nullptr, 0, 0))
    {
		if (!IsDialogMessage(GetForegroundWindow(), &msg)) {
			if (!TranslateAccelerator(msg.hwnd, hAccelTable, &msg))
			{
				TranslateMessage(&msg);
				DispatchMessage(&msg);
			}
		}
    }
	GdiplusShutdown(gdiplusToken);

    return (int) msg.wParam;
}



//
//  FUNCTION: MyRegisterClass()
//
//  PURPOSE: Registers the window class.
//
ATOM MyRegisterClass(HINSTANCE hInstance)
{
    WNDCLASSEXW wcex;

    wcex.cbSize = sizeof(WNDCLASSEX);

    wcex.style          = CS_HREDRAW | CS_VREDRAW;
    wcex.lpfnWndProc    = WndProc;
    wcex.cbClsExtra     = 0;
    wcex.cbWndExtra     = 0;
    wcex.hInstance      = hInstance;
    wcex.hIcon          = LoadIcon(hInstance, MAKEINTRESOURCE(IDI_1512574MIDTERM));
    wcex.hCursor        = LoadCursor(nullptr, IDC_ARROW);
    wcex.hbrBackground  = (HBRUSH)(COLOR_BTNFACE+1);
    wcex.lpszMenuName   = MAKEINTRESOURCEW(IDC_1512574MIDTERM);
    wcex.lpszClassName  = szWindowClass;
    wcex.hIconSm        = LoadIcon(wcex.hInstance, MAKEINTRESOURCE(IDI_SMALL));

    return RegisterClassExW(&wcex);
}

//
//   FUNCTION: InitInstance(HINSTANCE, int)
//
//   PURPOSE: Saves instance handle and creates main window
//
//   COMMENTS:
//
//        In this function, we save the instance handle in a global variable and
//        create and display the main program window.
//
BOOL InitInstance(HINSTANCE hInstance, int nCmdShow)
{
   hInst = hInstance; // Store instance handle in our global variable

   HWND hWnd = CreateWindowW(szWindowClass, szTitle, WS_OVERLAPPED | \
	   WS_CAPTION |
	   WS_SYSMENU |
	   WS_MINIMIZEBOX ,
      CW_USEDEFAULT, 0, WIDTH, HEIGHT + 70, nullptr, nullptr, hInstance, nullptr);

   if (!hWnd)
   {
      return FALSE;
   }

   ShowWindow(hWnd, nCmdShow);
   UpdateWindow(hWnd);

   return TRUE;
}

//
//  FUNCTION: WndProc(HWND, UINT, WPARAM, LPARAM)
//
//  PURPOSE:  Processes messages for the main window.
//
//  WM_COMMAND  - process the application menu
//  WM_PAINT    - Paint the main window
//  WM_DESTROY  - post a quit message and return
//
//
LRESULT CALLBACK WndProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam)
{
    switch (message)
    {
		HANDLE_MSG(hWnd, WM_CREATE, OnCreate);
		HANDLE_MSG(hWnd, WM_COMMAND, OnCommand);
		HANDLE_MSG(hWnd, WM_PAINT, OnPaint);
		HANDLE_MSG(hWnd, WM_DESTROY, OnDestroy);
		HANDLE_MSG(hWnd, WM_CLOSE, OnClose);
	case WM_KEYDOWN:
		if (wParam == VK_RETURN)
		{
			SendMessage(hWnd, WM_COMMAND, IDC_BTN_INSERT, NULL);
		}
		break;
	case WM_NOTIFY:
	{
		UINT code = ((LPNMHDR)lParam)->code;
		switch (code)
		{
			case NM_CLICK:
			{
				if ((((LPNMHDR)lParam)->hwndFrom) == g_ListView)
				{
					if (ListView_GetSelectionMark(g_ListView) != -1)
					{
						EnableWindow(g_Bdelete, TRUE);
					}
				}
				break;
			}
			case LVN_COLUMNCLICK:
			{
				LPNMLISTVIEW pnmv = (LPNMLISTVIEW)lParam;
				switch (pnmv->iSubItem)
				{
				case 0:
					for (int i = 0; i < g_DS_ChiTieu.size() -1; i++)
					{
						for (int j = i; j < g_DS_ChiTieu.size(); j++)
						{
							if (is_TangLCT) {
								if (lstrcmpW(g_DS_ChiTieu[i]->LoaiChiTieu, g_DS_ChiTieu[j]->LoaiChiTieu) > 0) {
									CHITIEU *temp = g_DS_ChiTieu[i];
									g_DS_ChiTieu[i] = g_DS_ChiTieu[j];
									g_DS_ChiTieu[j] = temp;
								}
							}
							else
							{
								if (lstrcmpW(g_DS_ChiTieu[i]->LoaiChiTieu, g_DS_ChiTieu[j]->LoaiChiTieu) < 0) {
									CHITIEU *temp = g_DS_ChiTieu[i];
									g_DS_ChiTieu[i] = g_DS_ChiTieu[j];
									g_DS_ChiTieu[j] = temp;
								}
							}
						}
					}
					is_TangLCT = !is_TangLCT;
					break;
				case 1:
					for (int i = 0; i < g_DS_ChiTieu.size() - 1; i++)
					{
						for (int j = i; j < g_DS_ChiTieu.size(); j++)
						{
							if (is_TangNDC) {
								if (lstrcmpW(g_DS_ChiTieu[i]->NoiDungChi, g_DS_ChiTieu[j]->NoiDungChi) > 0) {
									CHITIEU *temp = g_DS_ChiTieu[i];
									g_DS_ChiTieu[i] = g_DS_ChiTieu[j];
									g_DS_ChiTieu[j] = temp;
								}
							}
							else
							{
								if (lstrcmpW(g_DS_ChiTieu[i]->NoiDungChi, g_DS_ChiTieu[j]->NoiDungChi) < 0) {
									CHITIEU *temp = g_DS_ChiTieu[i];
									g_DS_ChiTieu[i] = g_DS_ChiTieu[j];
									g_DS_ChiTieu[j] = temp;
								}
							}
						}
					}
					is_TangNDC = !is_TangNDC;
					break;
				case 2:
					for (int i = 0; i < g_DS_ChiTieu.size() - 1; i++)
					{
						for (int j = i; j < g_DS_ChiTieu.size(); j++)
						{
							if (is_TangST) {
								if (g_DS_ChiTieu[i]->GetSoTien() - g_DS_ChiTieu[j]->GetSoTien() > 0) {
									CHITIEU *temp = g_DS_ChiTieu[i];
									g_DS_ChiTieu[i] = g_DS_ChiTieu[j];
									g_DS_ChiTieu[j] = temp;
								}
							}
							else {
								if (g_DS_ChiTieu[i]->GetSoTien() - g_DS_ChiTieu[j]->GetSoTien() < 0) {
									CHITIEU *temp = g_DS_ChiTieu[i];
									g_DS_ChiTieu[i] = g_DS_ChiTieu[j];
									g_DS_ChiTieu[j] = temp;
								}
							}
						}
					}
					is_TangST = !is_TangST;
					break;
				case 3:
					for (int i = 0; i < g_DS_ChiTieu.size() - 1; i++)
					{
						for (int j = i; j < g_DS_ChiTieu.size(); j++)
						{
							if (is_TangD) {
								if (lstrcmpW(g_DS_ChiTieu[i]->Date, g_DS_ChiTieu[j]->Date) > 0) {
									CHITIEU *temp = g_DS_ChiTieu[i];
									g_DS_ChiTieu[i] = g_DS_ChiTieu[j];
									g_DS_ChiTieu[j] = temp;
								}
							}
							else
							{
								if (lstrcmpW(g_DS_ChiTieu[i]->Date, g_DS_ChiTieu[j]->Date) < 0) {
									CHITIEU *temp = g_DS_ChiTieu[i];
									g_DS_ChiTieu[i] = g_DS_ChiTieu[j];
									g_DS_ChiTieu[j] = temp;
								}
							}
						}
					}
					is_TangD = !is_TangD;
					break;
				default:
					break;
				}
				ReLoadListView();
				break;
			}
			break;
		}
		break;
	}
	case WM_CTLCOLORSTATIC:
	{
		//đổi chữ đỏ cho thông báo,
		if ((HWND)lParam == g_Noti)
		{
			HDC noti = (HDC)wParam;
			SetTextColor(noti, RGB(255, 0, 0));
		}
		if ((HWND)lParam == g_LssTongChiTieu) {
			HDC noti = (HDC)wParam;
			SetTextColor(noti, RGB(255, 0, 0));
		}
		SetBkMode((HDC)wParam, TRANSPARENT);
		return (LRESULT)(HBRUSH)(COLOR_BTNFACE + 1);
	}
	case WM_CTLCOLORBTN:
	{
		//đổi màu nút delete
		if ((HWND)lParam == g_Bdelete) {
			HDC noti = (HDC)wParam;
			SetTextColor(noti, RGB(255, 0, 0));
			SetBkMode((HDC)wParam, TRANSPARENT);
			return (LRESULT)(HBRUSH)(COLOR_BTNHIGHLIGHT + 1);
		}
		break;
	}
    default:
        return DefWindowProc(hWnd, message, wParam, lParam);
    }
    return 0;
}

// Message handler for about box.
INT_PTR CALLBACK About(HWND hDlg, UINT message, WPARAM wParam, LPARAM lParam)
{
    UNREFERENCED_PARAMETER(lParam);
    switch (message)
    {
    case WM_INITDIALOG:
	{
		// Lấy font hệ thống
		LOGFONT lf;
		GetObject(GetStockObject(DEFAULT_GUI_FONT), sizeof(LOGFONT), &lf);
		lf.lfHeight = 15;
		lf.lfWidth = 7;
		HFONT hFont = CreateFont(lf.lfHeight, lf.lfWidth,
			lf.lfEscapement, lf.lfOrientation, lf.lfWeight,
			lf.lfItalic, lf.lfUnderline, lf.lfStrikeOut, lf.lfCharSet,
			lf.lfOutPrecision, lf.lfClipPrecision, lf.lfQuality,
			lf.lfPitchAndFamily, lf.lfFaceName);
		SetWindowText(hDlg, L"Thông tin ứng dụng");
		HWND temp = CreateWindow(L"STATIC", L"Phần mềm quản lý chi tiêu", WS_VISIBLE | WS_CHILD, 130, 90, 200, 20, hDlg, NULL, hInst, NULL);
		SendMessage(temp, WM_SETFONT, (WPARAM)hFont, NULL);
		return (INT_PTR)TRUE;
	}
	case WM_CREATE:
		break;
    case WM_COMMAND:
        if (LOWORD(wParam) == IDOK || LOWORD(wParam) == IDCANCEL)
        {
            EndDialog(hDlg, LOWORD(wParam));
            return (INT_PTR)TRUE;
        }
        break;
    }
    return (INT_PTR)FALSE;
}

//Message handler for StartDay box
INT_PTR CALLBACK StartDay(HWND hDlg, UINT message, WPARAM wParam, LPARAM lParam)
{
	UNREFERENCED_PARAMETER(lParam);
	switch (message)
	{
	case WM_INITDIALOG:
	{
		// Lấy font hệ thống
		LOGFONT lf;
		GetObject(GetStockObject(DEFAULT_GUI_FONT), sizeof(LOGFONT), &lf);
		lf.lfHeight = 15;
		lf.lfWidth = 7;
		HFONT hFont = CreateFont(lf.lfHeight, lf.lfWidth,
			lf.lfEscapement, lf.lfOrientation, lf.lfWeight,
			lf.lfItalic, lf.lfUnderline, lf.lfStrikeOut, lf.lfCharSet,
			lf.lfOutPrecision, lf.lfClipPrecision, lf.lfQuality,
			lf.lfPitchAndFamily, lf.lfFaceName);
		SetWindowText(hDlg, L"Chọn ngày bắt đầu");
		HWND temp = CreateWindow(L"STATIC", L"Nhập vào ngày bắt đầu tháng của bạn,(Ngày nhận \"Lương\")", WS_CHILD | WS_VISIBLE|SS_CENTER, 20, 20, 350, 50, hDlg, NULL, hInst, NULL);
		SendMessage(temp, WM_SETFONT, (WPARAM)hFont, NULL);
		g_getStartDate = CreateWindow(L"combobox", L"", WS_CHILD | WS_VISIBLE | CBS_DROPDOWNLIST | WS_VSCROLL | WS_TABSTOP, 100, 80, 200, 150, hDlg, NULL, hInst, NULL);
		SendMessage(g_getStartDate, WM_SETFONT, (WPARAM)hFont, NULL);
		WCHAR buff[5];
		for (int i = 1; i < 32; i++) {
			wsprintf(buff, L"%d", i);
			ComboBox_AddString(g_getStartDate, buff);
		}
		SendMessage(g_getStartDate, CB_SETCURSEL, (WPARAM)g_StartDate-1, NULL);

		return (INT_PTR)TRUE;
	}
	case WM_COMMAND:
		switch (LOWORD(wParam))
		{
		case ID_OK:
		{
			WCHAR temp[5];
			GetWindowText(g_getStartDate, temp, 5);
			g_StartDate = _wtoi(temp);

			//cập nhật lại tổng tiền
			UpdateTotalForMount();

			EndDialog(hDlg, LOWORD(wParam));
			return (INT_PTR)TRUE;
		}
			break;
		case ID_CANCEL:
		{
			EndDialog(hDlg, LOWORD(wParam));
			return (INT_PTR)TRUE;
		}
			break;
		default:
			break;
		}
		break;
	}
	return (INT_PTR)FALSE;
}

BOOL OnCreate(HWND hwnd, LPCREATESTRUCT lpCreateStruct) {
	// Lấy font hệ thống
	LOGFONT lf;
	GetObject(GetStockObject(DEFAULT_GUI_FONT), sizeof(LOGFONT), &lf);
	lf.lfHeight = 15;
	lf.lfWidth = 7;
	HFONT hFont = CreateFont(lf.lfHeight, lf.lfWidth,
		lf.lfEscapement, lf.lfOrientation, lf.lfWeight,
		lf.lfItalic, lf.lfUnderline, lf.lfStrikeOut, lf.lfCharSet,
		lf.lfOutPrecision, lf.lfClipPrecision, lf.lfQuality,
		lf.lfPitchAndFamily, lf.lfFaceName);
	lf.lfWeight = 700;
	lf.lfHeight = 15;
	lf.lfWidth = 7;
	HFONT hFontNameChart = CreateFont(lf.lfHeight, lf.lfWidth,
		lf.lfEscapement, lf.lfOrientation, lf.lfWeight,
		lf.lfItalic, lf.lfUnderline, lf.lfStrikeOut, lf.lfCharSet,
		lf.lfOutPrecision, lf.lfClipPrecision, lf.lfQuality,
		lf.lfPitchAndFamily, lf.lfFaceName);
	lf.lfWeight = 700;
	lf.lfHeight = 30;
	lf.lfWidth = 8;
	HFONT hFontTotal = CreateFont(lf.lfHeight, lf.lfWidth,
		lf.lfEscapement, lf.lfOrientation, lf.lfWeight,
		lf.lfItalic, lf.lfUnderline, lf.lfStrikeOut, lf.lfCharSet,
		lf.lfOutPrecision, lf.lfClipPrecision, lf.lfQuality,
		lf.lfPitchAndFamily, lf.lfFaceName);

	//Khởi tạo giao diện
	HWND hwndTemp = NULL;
	//Tạo vùng nhập dữ liệu
	hwndTemp = CreateWindow(L"BUTTON", L"Thêm chi tiêu", WS_CHILD | WS_VISIBLE |BS_GROUPBOX|WS_GROUP|BS_CENTER, 10, 10, WIDTH/2 - 20, HEIGHT/3, hwnd, NULL, hInst, NULL);
	SendMessage(hwndTemp, WM_SETFONT, WPARAM(hFont), NULL);
	//Tạo Loại chi tiêu
	hwndTemp = CreateWindow(L"STATIC", L"Loại chi tiêu:", WS_CHILD | WS_VISIBLE, 20, 42, 150, 20, hwnd, NULL, hInst, NULL);
	SendMessage(hwndTemp, WM_SETFONT, WPARAM(hFont), NULL);
	g_LoaiChiTieu = CreateWindow(L"combobox", L"", WS_CHILD | WS_VISIBLE | CBS_DROPDOWNLIST| WS_VSCROLL|WS_TABSTOP, 120, 40, 200, 150, hwnd, (HMENU)IDC_CBB_LOAICHITIEU, hInst, NULL);
	SendMessage(g_LoaiChiTieu, WM_SETFONT, WPARAM(hFont), NULL);
	ComboBox_AddString(g_LoaiChiTieu, L"Ăn uống");
	ComboBox_AddString(g_LoaiChiTieu, L"Di chuyển");
	ComboBox_AddString(g_LoaiChiTieu, L"Nhà cửa");
	ComboBox_AddString(g_LoaiChiTieu, L"Xe cộ");
	ComboBox_AddString(g_LoaiChiTieu, L"Nhu yếu phẩm");
	ComboBox_AddString(g_LoaiChiTieu, L"Dịch vụ");
	SendMessage(g_LoaiChiTieu, CB_SETCURSEL, (WPARAM)0, NULL);

	//Tạo Nội dung chi
	hwndTemp = CreateWindow(L"STATIC", L"Nội dung chi:", WS_CHILD | WS_VISIBLE, 20, 82, 150, 20, hwnd, NULL, hInst, NULL);
	SendMessage(hwndTemp, WM_SETFONT, WPARAM(hFont), NULL);
	g_NoiDungChi = CreateWindow(L"combobox", L"", WS_CHILD | WS_VISIBLE | CBS_DROPDOWN | WS_VSCROLL|WS_TABSTOP, 120, 80, 200, 150, hwnd, NULL, hInst, NULL);
	SendMessage(g_NoiDungChi, WM_SETFONT, WPARAM(hFont), NULL);
	//Khởi tạo nội dung chi
	KhoiTaoCBBNoiDungChi();
	//load sub data
	loadDataSubThemChiTieu(L"datasub.dat");
	ReLoadCBBNoiDungChiTieu();

	//Tạo số tiền chi
	hwndTemp = CreateWindow(L"STATIC", L"Số tiền chi:", WS_CHILD | WS_VISIBLE, 20, 122, 150, 20, hwnd, NULL, hInst, NULL);
	SendMessage(hwndTemp, WM_SETFONT, WPARAM(hFont), NULL);
	g_SoTien = CreateWindow(L"EDIT", L"", WS_CHILD | WS_VISIBLE |WS_BORDER|WS_TABSTOP|ES_RIGHT, 120, 120, 200, 20, hwnd, NULL, hInst, NULL);
	SendMessage(g_SoTien, WM_SETFONT, WPARAM(hFont), NULL);
	hwndTemp = CreateWindow(L"STATIC", L"đ", WS_CHILD | WS_VISIBLE, 330, 122, 150, 20, hwnd, NULL, hInst, NULL);
	SendMessage(hwndTemp, WM_SETFONT, WPARAM(hFont), NULL);

	//Tạo ngày chi
	hwndTemp = CreateWindow(L"STATIC", L"Ngày chi:", WS_CHILD | WS_VISIBLE|WS_TABSTOP, 20, 162, 150, 20, hwnd, NULL, hInst, NULL);
	SendMessage(hwndTemp, WM_SETFONT, WPARAM(hFont), NULL);
	g_NgayChi = CreateWindowEx(0,DATETIMEPICK_CLASS,TEXT("DateTime"),WS_BORDER | WS_CHILD | WS_VISIBLE,120, 160, 200, 20,hwnd, NULL, hInst, NULL);
	SendMessage(g_NgayChi, WM_SETFONT, WPARAM(hFont), NULL);

	//Tạo Button nhập liệu
	hwndTemp = CreateWindow(L"BUTTON", L"Thêm", WS_CHILD | WS_VISIBLE | WS_BORDER|WS_TABSTOP, 370, 40, 100, 100, hwnd,HMENU(IDC_BTN_INSERT), hInst, NULL);
	SendMessage(hwndTemp, WM_SETFONT, WPARAM(hFont), NULL);
	
	//Tạo thông báo không hợp lệ
	g_Noti = CreateWindow(L"STATIC", L"", WS_CHILD | WS_VISIBLE, 370, 150, 100, 60, hwnd, NULL, hInst, NULL);
	SendMessage(g_Noti, WM_SETFONT, WPARAM(hFont), NULL);

	//Tạo vùng biểu đồ và chú thích
	hwndTemp = CreateWindow(L"BUTTON", L"Biểu đồ chi tiêu", WS_CHILD | WS_VISIBLE | BS_GROUPBOX | WS_GROUP | BS_CENTER, 10, HEIGHT / 3 + 20, WIDTH / 2 - 20, 2*(HEIGHT / 3) - 30, hwnd, NULL, hInst, NULL);
	SendMessage(hwndTemp, WM_SETFONT, WPARAM(hFont), NULL);

	//Tạo chú thích
	g_LAU = CreateWindow(L"STATIC", L"Ăn uống", WS_CHILD | WS_VISIBLE, 390, 265, 90, 40, hwnd, NULL, hInst, NULL);
	SendMessage(g_LAU, WM_SETFONT, WPARAM(hFont), NULL);
	g_LDC = CreateWindow(L"STATIC", L"Di chuyển", WS_CHILD | WS_VISIBLE, 390, 315, 90, 40, hwnd, NULL, hInst, NULL);
	SendMessage(g_LDC, WM_SETFONT, WPARAM(hFont), NULL);
	g_LNC = CreateWindow(L"STATIC", L"Nhà cửe", WS_CHILD | WS_VISIBLE, 390, 365, 90, 40, hwnd, NULL, hInst, NULL);
	SendMessage(g_LNC, WM_SETFONT, WPARAM(hFont), NULL);
	g_LXC = CreateWindow(L"STATIC", L"Xe cộ", WS_CHILD | WS_VISIBLE, 390, 415, 90, 40, hwnd, NULL, hInst, NULL);
	SendMessage(g_LXC, WM_SETFONT, WPARAM(hFont), NULL);
	g_LNYP = CreateWindow(L"STATIC", L"Nhu yếu phẩm", WS_CHILD | WS_VISIBLE, 390, 465, 90, 40, hwnd, NULL, hInst, NULL);
	SendMessage(g_LNYP, WM_SETFONT, WPARAM(hFont), NULL);
	g_LDV = CreateWindow(L"STATIC", L"Dịch vụ", WS_CHILD | WS_VISIBLE, 390, 515, 90, 40, hwnd, NULL, hInst, NULL);
	SendMessage(g_LDV, WM_SETFONT, WPARAM(hFont), NULL);

	//Tên biểu đồ
	hwndTemp = CreateWindow(L"STATIC", L"Biểu đồ thể hiện tỉ lệ chi tiêu", WS_CHILD | WS_VISIBLE, 70, 550, 300, 20, hwnd, NULL, hInst, NULL);
	SendMessage(hwndTemp, WM_SETFONT, WPARAM(hFontNameChart), NULL);

	//Tạo Button delete
	g_Bdelete = CreateWindow(L"BUTTON", L"Xóa", WS_CHILD | WS_VISIBLE | WS_BORDER | WS_DISABLED, WIDTH - 130, 10, 100, 20, hwnd, HMENU(IDC_BTN_DELETE), hInst, NULL);
	SendMessage(g_Bdelete, WM_SETFONT, WPARAM(hFont), NULL);

	//Tạo listview
	g_ListView = CreateWindow(WC_LISTVIEWW, L"", LVS_SINGLESEL| WS_CHILD | WS_VISIBLE | WS_VSCROLL | WS_HSCROLL | WS_TABSTOP | LVS_ICON | LVS_EDITLABELS | LVS_SHOWSELALWAYS | LVS_REPORT, WIDTH / 2, 40, WIDTH / 2 - 25, 3 * HEIGHT / 5 + 120, hwnd, NULL, hInst, NULL);
	SendMessage(g_ListView, WM_SETFONT, WPARAM(hFont), NULL);
	ListView_SetExtendedListViewStyle(g_ListView, LVS_EX_FULLROWSELECT|LVS_EX_GRIDLINES);
	//Khởi tạo cho LisView
	LVCOLUMN lvCol;
	lvCol.mask = LVCF_FMT | LVCF_TEXT | LVCF_WIDTH;
	lvCol.fmt = LVCFMT_LEFT;
	
	lvCol.cx = 90;
	lvCol.pszText = L"Loại chi tiêu";
	ListView_InsertColumn(g_ListView, 1, &lvCol);

	lvCol.cx = 150;
	lvCol.pszText = L"Nội dung chi";
	ListView_InsertColumn(g_ListView, 2, &lvCol);

	lvCol.cx = 140;
	lvCol.pszText = L"Số tiền";
	ListView_InsertColumn(g_ListView, 3, &lvCol);

	lvCol.cx = 90;
	lvCol.pszText = L"Ngày chi";
	ListView_InsertColumn(g_ListView, 4, &lvCol);
	//Nạp icon
	TaoReSourchIcon();
	//Lấy dữ liệu từ data lên và hiển thị lên List View
	readData(L"data.dat");
	for (int i = 0; i < g_DS_ChiTieu.size(); i++)
	{
		Insert_LVItem(g_DS_ChiTieu[i]);
	}

	//Tạo ô hiển thị tổng chi tiêu
	hwndTemp = CreateWindow(L"STATIC", L"TỔNG CHI TIÊU TRONG THÁNG:", WS_CHILD | WS_VISIBLE, WIDTH / 2, 3 * HEIGHT / 5 + 170, 350,40, hwnd, NULL, hInst, NULL);
	SendMessage(hwndTemp, WM_SETFONT, WPARAM(hFontTotal), NULL);
	//Tạo ô hiển thị ngày bắt đầu tính
	g_DateThongBao = CreateWindow(L"STATIC", L"12\\05\\2017", WS_CHILD | WS_VISIBLE, WIDTH / 2, 3 * HEIGHT / 5 + 210, 350, 40, hwnd, NULL, hInst, NULL);
	SendMessage(g_DateThongBao, WM_SETFONT, LPARAM(hFont), NULL);
	g_LssTongChiTieu = CreateWindow(L"STATIC", L"Chi tiêu so với tháng trước", WS_CHILD | WS_VISIBLE |SS_RIGHT, WIDTH/2 + 320, 3 * HEIGHT / 5 + 170,150, 40, hwnd, NULL, hInst, NULL);
	SendMessage(g_LssTongChiTieu, WM_SETFONT, WPARAM(hFontTotal), NULL);

	return TRUE;
}

VOID OnCommand(HWND hwnd, int id, HWND hwndCtl, UINT codeNotify) {
	// Parse the menu selections:
	switch (id)
	{
	case IDC_CBB_LOAICHITIEU:
	{
		switch (codeNotify)
		{
		case CBN_SELCHANGE:
		{
			ReLoadCBBNoiDungChiTieu();
		}
		default:
			break;
		}
		break;
	}
	case ID_FILE_IMPORT:
	{
		WCHAR fileName[255];
		if (DialogOpenFile(hwnd, fileName)) {
			if (MessageBox(hwnd, L"Bạn có muốn xóa hết dữ liệu hiện tại?", L"Thông báo", MB_YESNO) == IDYES)
			{
				//xóa toàn bộ dữ liệu
				while (g_DS_ChiTieu.size())
				{
					delete g_DS_ChiTieu[0];
					g_DS_ChiTieu.erase(g_DS_ChiTieu.begin());
				}
				readData(fileName);
				ReLoadListView();
				InvalidateRect(hwnd, NULL, false);
			}
		}
	}
		break;
	case ID_FILE_BACKUP:
	{
		WCHAR fileName[255];
		if (DiaLogSaveFile(hwnd, fileName) ){
			saveData(fileName);
		}
	}
		break;
	case ID_SETING_RESET:
	{
		int result = MessageBox(hwnd, L"Dữ liệu của bạn sẽ bị xóa sạch?", L"Cảnh báo", MB_YESNO);
		switch (result)
		{
		case IDYES:
		{
			WCHAR path[255];
			WCHAR fileName[255];
			GetModuleFileNameW(NULL, path, 255);
			while (path[lstrlenW(path) - 1] != L'\\')
			{
				path[lstrlenW(path) - 1] = L'\0';
			}
			path[lstrlenW(path) - 1] = L'\0';
			//xóa file .exe
			wsprintf(fileName, L"%s\\%s", path, L"data.dat");

			std::fstream fout;
			fout.open(fileName, std::fstream::out);
			fout.close();
			wsprintf(fileName, L"%s\\%s", path, L"datasub.dat");
			fout.open(fileName, std::fstream::out);
			fout.close();
			MessageBeep(MB_ICONWARNING);
			MessageBox(hwnd, L"Đã xóa hết dữ liệu, vui lòng khởi động lại ứng dụng", L"Thông báo", MB_OK);
			DestroyWindow(hwnd);
		}
			break;
		case IDNO:
			break;
		default:
			break;
		}
		break;
	}
	case IDC_BTN_DELETE:
	{
		if (ListView_GetSelectionMark(g_ListView) != -1)
		{
			INT id = 0;
			//lấy chi tiêu được chọn
			LVITEM lv;
			lv.mask = LVIF_PARAM;
			lv.iItem = ListView_GetSelectionMark(g_ListView);
			lv.iSubItem = 0;
			ListView_GetItem(g_ListView, &lv);
			id = lv.lParam;
			//Tìm chi tiêu được chọn
			for (int i = 0; i < g_DS_ChiTieu.size(); i++)
			{
				if (id == g_DS_ChiTieu[i]->MACHITIEU) {
					id = i;
					break;
				}
			}
			WCHAR *buff = new WCHAR[500];
			wsprintf(buff, L"Bạn có muốn xóa chi tiêu %s, %s ?", g_DS_ChiTieu[id]->LoaiChiTieu, g_DS_ChiTieu[id]->NoiDungChi);
			if (MessageBox(hwnd, buff, L"Thông báo", MB_YESNO) == IDYES)
			{
				delete g_DS_ChiTieu[id];
				g_DS_ChiTieu.erase(g_DS_ChiTieu.begin() + id);
				ReLoadListView();
				InvalidateRect(hwnd, NULL, false);
			}
			delete []buff;
		}
		else {
			MessageBox(hwnd, L"Bạn chưa chọn nội dung xóa!", L"Cảnh báo", MB_OK);
			MessageBeep(MB_ICONWARNING);
			break;
		}
	}
		break;
	case IDC_BTN_INSERT:
	{
		CHITIEU *ChiTieu = new CHITIEU();
		//Lấy loại chi tiêu
		INT length = GetWindowTextLength(g_LoaiChiTieu);
		WCHAR buff[500];
		GetWindowText(g_LoaiChiTieu, buff, length + 1);
		if (!lstrcmpiW(buff, L""))
		{
			SetWindowText(g_Noti, L"Không được để trống Loại chi tiêu!");
			SetFocus(g_LoaiChiTieu);
			MessageBeep(MB_ICONWARNING);
			delete ChiTieu;
			break;
		}
		ChiTieu->SetLoaiChiTieu(buff);

		//Lấy nội dung chi
		length = GetWindowTextLength(g_NoiDungChi);
		GetWindowText(g_NoiDungChi, buff, length + 1);
		if (!lstrcmpiW(buff, L""))
		{
			SetWindowText(g_Noti, L"Không được để trống Nội dung chi!");
			SetFocus(g_NoiDungChi);
			MessageBeep(MB_ICONWARNING);
			delete ChiTieu;
			break;
		}
		ChiTieu->SetNoiDungChi(buff);

		//Lấy số tiền
		length = GetWindowTextLength(g_SoTien);
		GetWindowText(g_SoTien, buff, length + 1);
		if (!lstrcmpiW(buff, L""))
		{
			SetWindowText(g_Noti, L"Không được để trống Số tiền chi!");
			SetFocus(g_SoTien);
			MessageBeep(MB_ICONWARNING);
			delete ChiTieu;
			break;
		}
		if(!is_Number(buff))
		{
			SetWindowText(g_Noti, L"Nhập số tiền phải là số!");
			SetFocus(g_SoTien);
			MessageBeep(MB_ICONWARNING);
			delete ChiTieu;
			break;
		}
		ChiTieu->SetSoTien(buff);

		length = GetWindowTextLength(g_NgayChi);
		GetWindowText(g_NgayChi, buff, length +1);
		if (!lstrcmpiW(buff, L""))
		{
			SetWindowText(g_Noti, L"Không được để trống Ngày chi!");
			SetFocus(g_NgayChi);
			MessageBeep(MB_ICONWARNING);
			delete ChiTieu;
			break;
		}
		ChiTieu->SetDate(buff);

		//Thêm chi tiêu vào Danh sách
		g_DS_ChiTieu.push_back(ChiTieu);
		//xóa nội dung trong các ô input
		SetWindowText(g_LoaiChiTieu, L"");
		SetWindowText(g_NoiDungChi, L"");
		SetWindowText(g_SoTien, L"");
		SetWindowText(g_NgayChi, L"");
		SetWindowText(g_Noti, L"");
		SetFocus(g_LoaiChiTieu);

		//Thêm chi tiêu vào ListView
		Insert_LVItem(ChiTieu);
		InvalidateRect(hwnd, NULL, TRUE);


		//Tiến hành update combobox Noi Dung Chi
		int index = SendMessage(g_LoaiChiTieu, CB_GETCURSEL, NULL, NULL);
		switch (index) // so sánh kí tự thứ 5 của Loại chi tiêu
		{
		case 0://ăn uống
		{
			if (!findData(g_DS_SUB_AU, ChiTieu->NoiDungChi)) {
				WCHAR *temp = new WCHAR[50];
				lstrcpyW(temp, ChiTieu->NoiDungChi);
				g_DS_SUB_AU.push_back(temp);
			}
			break;
		}
		case 1://Di chuyển
		{
			if (!findData(g_DS_SUB_DC, ChiTieu->NoiDungChi)) {
				WCHAR *temp = new WCHAR[50];
				lstrcpyW(temp, ChiTieu->NoiDungChi);
				g_DS_SUB_DC.push_back(temp);
			}
			break;
		}
		case 2: //Nhà cửa
		{
			if (!findData(g_DS_SUB_NC, ChiTieu->NoiDungChi)) {
				WCHAR *temp = new WCHAR[50];
				lstrcpyW(temp, ChiTieu->NoiDungChi);
				g_DS_SUB_NC.push_back(temp);
			}
			break;
		}
		case 3: //Xe cộ
		{
			if (!findData(g_DS_SUB_XC, ChiTieu->NoiDungChi)) {
				WCHAR *temp = new WCHAR[50];
				lstrcpyW(temp, ChiTieu->NoiDungChi);
				g_DS_SUB_XC.push_back(temp);
			}
			break;
		}
		case 4: //Nhu yếu phẩm
		{
			if (!findData(g_DS_SUB_NYP, ChiTieu->NoiDungChi)) {
				WCHAR *temp = new WCHAR[50];
				lstrcpyW(temp, ChiTieu->NoiDungChi);
				g_DS_SUB_NYP.push_back(temp);
			}
			break;
		}
		case 5: //Dịch vụ
		{
			if (!findData(g_DS_SUB_DV, ChiTieu->NoiDungChi)) {
				WCHAR *temp = new WCHAR[50];
				lstrcpyW(temp, ChiTieu->NoiDungChi);
				g_DS_SUB_DV.push_back(temp);
			}
			break;
		}
		default:
			break;
		}
		ReLoadCBBNoiDungChiTieu();
		break;
	}
	case IDM_ABOUT:
		DialogBox(hInst, MAKEINTRESOURCE(IDD_DIALOG1), hwnd, About);
		break;
	case IDM_EXIT:
		SendMessage(hwnd, WM_CLOSE, 0, 0);
		break;
	case ID_SETING_STARTDAY:
		DialogBox(hInst, MAKEINTRESOURCE(IDD_STARTDAY),hwnd, StartDay);
		break;
	default:
		break;
	}
}

VOID OnPaint(HWND hwnd) {
	PAINTSTRUCT ps;
	HDC hdc = BeginPaint(hwnd, &ps);

	Graphics *graphics = new Graphics(hdc);
	Pen      *pen = new Pen(Color(0,255, 0, 0));
	SolidBrush *mySolidBrush = new SolidBrush(Color(0,0,0));

	RectF ellipseRect(20, 240, 300, 300);
	REAL startAngle = 0.0f;
	REAL sweepAngle = 0.0f;

	WCHAR temp[50];

	// Draw the pie. 
	//Ăn uống
	startAngle += sweepAngle;
	sweepAngle = CaculatePercent(L"Ăn uống")*360;

	wsprintf(temp, L"%s%% Ăn uống", FloatToW(sweepAngle/3.6,2));
	SetWindowText(g_LAU, temp);
	mySolidBrush->SetColor(Color(g_C_AU[0], g_C_AU[1], g_C_AU[2]));
	graphics->FillPie(mySolidBrush, ellipseRect, startAngle, sweepAngle);
	graphics->DrawPie(pen, ellipseRect, startAngle, sweepAngle);
	//Di chuyển
	startAngle += sweepAngle;
	sweepAngle = CaculatePercent(L"Di chuyển") * 360;
	wsprintf(temp, L"%s%% Di chuyển", FloatToW(sweepAngle / 3.6,2));
	SetWindowText(g_LDC, temp);
	mySolidBrush->SetColor(Color(g_C_DC[0], g_C_DC[1], g_C_DC[2]));
	graphics->FillPie(mySolidBrush, ellipseRect, startAngle, sweepAngle);
	graphics->DrawPie(pen, ellipseRect, startAngle, sweepAngle);
	//Nhà cửa
	startAngle += sweepAngle;
	sweepAngle = CaculatePercent(L"Nhà cửa") * 360;
	wsprintf(temp, L"%s%% Nhà cửa", FloatToW(sweepAngle / 3.6, 2));
	SetWindowText(g_LNC, temp);
	mySolidBrush->SetColor(Color(g_C_NC[0], g_C_NC[1], g_C_NC[2]));
	graphics->FillPie(mySolidBrush, ellipseRect, startAngle, sweepAngle);
	graphics->DrawPie(pen, ellipseRect, startAngle, sweepAngle);
	//Xe cộ
	startAngle += sweepAngle;
	sweepAngle = CaculatePercent(L"Xe cộ") * 360;
	wsprintf(temp, L"%s%% Xe cộ", FloatToW(sweepAngle / 3.6, 2));
	SetWindowText(g_LXC, temp);
	mySolidBrush->SetColor(Color(g_C_XC[0], g_C_XC[1], g_C_XC[2]));
	graphics->FillPie(mySolidBrush, ellipseRect, startAngle, sweepAngle);
	graphics->DrawPie(pen, ellipseRect, startAngle, sweepAngle);
	//Nhu yếu phẩm
	startAngle += sweepAngle;
	sweepAngle = CaculatePercent(L"Nhu yếu phẩm") * 360;
	wsprintf(temp, L"%s%% Nhu yếu phẩm", FloatToW(sweepAngle / 3.6, 2));
	SetWindowText(g_LNYP, temp);
	mySolidBrush->SetColor(Color(g_C_NYP[0], g_C_NYP[1], g_C_NYP[2]));
	graphics->FillPie(mySolidBrush, ellipseRect, startAngle, sweepAngle);
	graphics->DrawPie(pen, ellipseRect, startAngle, sweepAngle);
	//Dịch vụ
	startAngle += sweepAngle;
	sweepAngle = CaculatePercent(L"Dịch vụ") * 360;
	wsprintf(temp, L"%s%% Dịch vụ", FloatToW(sweepAngle / 3.6, 2));
	SetWindowText(g_LDV, temp);
	mySolidBrush->SetColor(Color(g_C_DV[0], g_C_DV[1], g_C_DV[2]));
	graphics->FillPie(mySolidBrush, ellipseRect, startAngle, sweepAngle);
	graphics->DrawPie(pen, ellipseRect, startAngle, sweepAngle);

	//vẽ bảng chú thích
	ellipseRect.X = 320;
	ellipseRect.Y = 250;
	ellipseRect.Width = 60;
	ellipseRect.Height = 60;
	startAngle = 315.0f;
	sweepAngle = 45.0f;

	//chú thích ăn uống
	graphics->DrawPie(pen, ellipseRect, startAngle, sweepAngle);
	mySolidBrush->SetColor(Color(g_C_AU[0], g_C_AU[1], g_C_AU[2]));
	graphics->FillPie(mySolidBrush, ellipseRect, startAngle, sweepAngle);

	//chú thích di chuyển
	ellipseRect.Y = 300;
	mySolidBrush->SetColor(Color(g_C_DC[0], g_C_DC[1], g_C_DC[2]));
	graphics->FillPie(mySolidBrush, ellipseRect, startAngle, sweepAngle);
	graphics->DrawPie(pen, ellipseRect, startAngle, sweepAngle);

	//chú thích nhà cửa
	ellipseRect.Y = 350;
	mySolidBrush->SetColor(Color(g_C_NC[0], g_C_NC[1], g_C_NC[2]));
	graphics->FillPie(mySolidBrush, ellipseRect, startAngle, sweepAngle);
	graphics->DrawPie(pen, ellipseRect, startAngle, sweepAngle);

	//chú thích xe cộ
	ellipseRect.Y = 400;
	mySolidBrush->SetColor(Color(g_C_XC[0], g_C_XC[1], g_C_XC[2]));
	graphics->FillPie(mySolidBrush, ellipseRect, startAngle, sweepAngle);
	graphics->DrawPie(pen, ellipseRect, startAngle, sweepAngle);

	//chú thích nhu yếu phẩm
	ellipseRect.Y = 450;
	mySolidBrush->SetColor(Color(g_C_NYP[0], g_C_NYP[1], g_C_NYP[2]));
	graphics->FillPie(mySolidBrush, ellipseRect, startAngle, sweepAngle);
	graphics->DrawPie(pen, ellipseRect, startAngle, sweepAngle);

	//chú thích dịch vụ
	ellipseRect.Y = 500;
	mySolidBrush->SetColor(Color(g_C_DV[0], g_C_DV[1], g_C_DV[2]));
	graphics->FillPie(mySolidBrush, ellipseRect, startAngle, sweepAngle);
	graphics->DrawPie(pen, ellipseRect, startAngle, sweepAngle);
	// TODO: Add any drawing code that uses hdc here...

	//Cập nhật tổng chi tiêu
	UpdateTotalForMount();
	delete graphics;
	delete pen;
	EndPaint(hwnd, &ps);
}

VOID OnDestroy(HWND hwnd) {
	PostQuitMessage(0);
}

VOID OnClose(HWND hwnd) {
	saveData(L"data.dat");
	saveDataSubThemChiTieu(L"datasub.dat");

	//xóa danh sách chi tiêu
	for (int i = 0; i < g_DS_ChiTieu.size(); i++)
	{
		delete g_DS_ChiTieu[i];
	}
	//xóa sub
	for (int i = 0; i < g_DS_SUB_AU.size(); i++)
	{
		delete g_DS_SUB_AU[i];
	}
	for (int i = 0; i < g_DS_SUB_DC.size(); i++)
	{
		delete g_DS_SUB_DC[i];
	}
	for (int i = 0; i < g_DS_SUB_DV.size(); i++)
	{
		delete g_DS_SUB_DV[i];
	}
	for (int i = 0; i < g_DS_SUB_NC.size(); i++)
	{
		delete g_DS_SUB_NC[i];
	}
	for (int i = 0; i < g_DS_SUB_NYP.size(); i++)
	{
		delete g_DS_SUB_NYP[i];
	}
	for (int i = 0; i < g_DS_SUB_XC.size(); i++)
	{
		delete g_DS_SUB_XC[i];
	}
	DestroyWindow(hwnd);
}
BOOL is_Number(WCHAR *str) {
	for (int i = 0; i < lstrlenW(str); i++)
	{
		if (str[i]<L'0' || str[i]>'9')
			return FALSE;
	}
	return TRUE;
}

BOOL saveData(WCHAR *_fileName) {
	std::fstream fout;
	WCHAR path[255];
	WCHAR fileName[255];
	GetModuleFileNameW(NULL, path, 255);
	while (path[lstrlenW(path) -1] != L'\\')
	{
		path[lstrlenW(path) - 1] = L'\0';
	}
	path[lstrlenW(path) - 1] = L'\0';
	//xóa file .exe
	wsprintf(fileName, L"%s\\%s", path, _fileName);
	if (_fileName[2] != L'\\') //là đường dẫn tương đối
	{
		fout.open(fileName, std::fstream::out);
	}
	else// là dường dẫn tuyệt đối
	{
		fout.open(_fileName, std::fstream::out);
	}

	if (!fout.is_open())
	{
		return FALSE;
	}

	//Lưu Start date xuống file
	fout.write((char*)&g_StartDate, 4);

	for (int i = 0; i < g_DS_ChiTieu.size(); i++)
	{
		g_DS_ChiTieu[i]->Save(fout);
	}
	fout.close();
	return TRUE;
}

BOOL readData(WCHAR *_fileName) {
	WCHAR path[255];
	WCHAR fileName[255];
	GetModuleFileNameW(NULL, path, 255);
	while (path[lstrlenW(path) - 1] != L'\\')
	{
		path[lstrlenW(path) - 1] = L'\0';
	}
	path[lstrlenW(path) - 1] = L'\0';
	//xóa file .exe
	wsprintf(fileName, L"%s\\%s", path, _fileName);

	std::fstream fin;
	if (_fileName[2] != L'\\') //là đường dẫn tương đối
	{
		fin.open(fileName, std::fstream::in);
	}
	else// là dường dẫn tuyệt đối
	{
		fin.open(_fileName, std::fstream::in);
	}
	if (!fin.is_open())
		return FALSE;

	//Lưu Start date xuống file
	fin.read((char*)&g_StartDate, 4);

	CHITIEU *temp;
	BOOL is_end = FALSE;
	do{
		temp = new CHITIEU();
		if (!temp->Read(fin))
		{
			is_end = TRUE;
			continue;
		}
		g_DS_ChiTieu.push_back(temp);
	} while (!is_end);
	fin.close();
	return TRUE;
}

BOOL findData(std::vector<WCHAR*> vt, WCHAR*data) {
	for (int i = 0; i < vt.size(); i++)
	{
		if (!lstrcmpiW(vt[i], data))
			return TRUE;
	}
	return FALSE;
}

BOOL loadDataSubThemChiTieu(WCHAR* _fileName) {
	WCHAR path[255];
	WCHAR fileName[255];
	GetModuleFileNameW(NULL, path, 255);
	while (path[lstrlenW(path) - 1] != L'\\')
	{
		path[lstrlenW(path) - 1] = L'\0';
	}
	path[lstrlenW(path) - 1] = L'\0';
	//xóa file .exe
	wsprintf(fileName, L"%s\\%s", path, _fileName);

	std::fstream fin;
	fin.open(fileName, std::fstream::in);
	if (!fin.is_open())
		return FALSE;
	WCHAR *temp = NULL;
	while (1) {
		temp = new WCHAR[50];
		temp[0] = 0;
		fin.read((char*)temp, 50);
		if (temp[0] == 0 || (!lstrcmpiW(temp, L"end")))
		{
			delete temp;
			break;
		}
		g_DS_SUB_AU.push_back(temp);
	}
	while (1) {
		temp = new WCHAR[50];
		temp[0] = 0;
		fin.read((char*)temp, 50);
		if (temp[0] == 0 || (!lstrcmpiW(temp, L"end")))
		{
			delete temp;
			break;
		}
		g_DS_SUB_DC.push_back(temp);
	}
	while (1) {
		temp = new WCHAR[50];
		temp[0] = 0;
		fin.read((char*)temp, 50);
		if (temp[0] == 0 || (!lstrcmpiW(temp, L"end")))
		{
			delete temp;
			break;
		}
		g_DS_SUB_NC.push_back(temp);
	}
	while (1) {
		temp = new WCHAR[50];
		temp[0] = 0;
		fin.read((char*)temp, 50);
		if (temp[0] == 0 || (!lstrcmpiW(temp, L"end")))
		{
			delete temp;
			break;
		}
		g_DS_SUB_XC.push_back(temp);
	}
	while (1) {
		temp = new WCHAR[50];
		temp[0] = 0;
		fin.read((char*)temp, 50);
		if (temp[0] == 0 || (!lstrcmpiW(temp, L"end")))
		{
			delete temp;
			break;
		}
		g_DS_SUB_NYP.push_back(temp);
	}
	while (1) {
		temp = new WCHAR[50];
		temp[0] = 0;
		fin.read((char*)temp, 50);
		if (temp[0] == 0 || (!lstrcmpiW(temp, L"end")))
		{
			delete temp;
			break;
		}
		g_DS_SUB_DV.push_back(temp);
	}

	fin.close();
	return TRUE;
}

BOOL saveDataSubThemChiTieu(WCHAR* _fileName) {
	WCHAR path[255];
	WCHAR fileName[255];
	GetModuleFileNameW(NULL, path, 255);
	while (path[lstrlenW(path) - 1] != L'\\')
	{
		path[lstrlenW(path) - 1] = L'\0';
	}
	path[lstrlenW(path) - 1] = L'\0';
	//xóa file .exe
	wsprintf(fileName, L"%s\\%s", path, _fileName);

	std::fstream fout;
	fout.open(fileName, std::fstream::out);

	if (!fout.is_open())
		return FALSE;
	
	for (int i = 7; i < g_DS_SUB_AU.size(); i++) {
		fout.write((char*)g_DS_SUB_AU[i], 50);
	}
	fout.write((char*)L"end", 50);
	for (int i = 5; i < g_DS_SUB_DC.size(); i++) {
		fout.write((char*)g_DS_SUB_DC[i], 50);
	}
	fout.write((char*)L"end", 50);
	for (int i = 4; i < g_DS_SUB_NC.size(); i++) {
		fout.write((char*)g_DS_SUB_NC[i], 50);
	}
	fout.write((char*)L"end", 50);
	for (int i = 4; i < g_DS_SUB_XC.size(); i++) {
		fout.write((char*)g_DS_SUB_XC[i], 50);
	}
	fout.write((char*)L"end", 50);
	for (int i = 3; i < g_DS_SUB_NYP.size(); i++) {
		fout.write((char*)g_DS_SUB_NYP[i], 50);
	}
	fout.write((char*)L"end", 50);
	for (int i = 2; i < g_DS_SUB_DV.size(); i++) {
		fout.write((char*)g_DS_SUB_DV[i], 50);
	}
	fout.write((char*)L"end", 50);

	fout.close();
	return TRUE;
}

VOID Insert_LVItem(CHITIEU *chitieu)
{
	LV_ITEM lv;
	lv.mask = LVIF_TEXT | LVIF_PARAM | LVIF_IMAGE;
	lv.iItem = g_CountLV++;

	lv.iSubItem = 0; //cột Loại chi tiêu
	lv.pszText = chitieu->LoaiChiTieu;
	lv.lParam = chitieu->MACHITIEU;
	switch (chitieu->LoaiChiTieu[4])
	{
	case L'ố': //ăn uống
		lv.iImage = 0;
		break; 
	case L'h': //di chuyển
		lv.iImage = 1;
		break;
	case L'c': //nhà cửa
		lv.iImage = 3;
		break;
	case L'ộ': //xe cộ
		lv.iImage = 5;
		break;
	case L'y': //Nhu yếu phẩm
		lv.iImage = 4;
		break;
	case L' ': //dịch vụ
		lv.iImage = 2;
		break;
	default:
		break;
	}
	ListView_InsertItem(g_ListView, &lv);

	lv.mask = LVIF_TEXT;
	lv.iSubItem = 1; //Cột Nội Dung chi
	lv.pszText = chitieu->NoiDungChi;
	ListView_SetItem(g_ListView, &lv);

	lv.mask = LVIF_TEXT;
	lv.iSubItem = 2; //Cột SỐ tiền
	WCHAR buff[100];
	NbStrToStr(buff, chitieu->SoTien);
	lv.pszText = buff;
	ListView_SetItem(g_ListView, &lv);

	lv.mask = LVIF_TEXT;
	lv.iSubItem = 3; //Cột Ngày chi
	lv.pszText = chitieu->Date;
	ListView_SetItem(g_ListView, &lv);
}

FLOAT CaculatePercent(WCHAR *LoaiChiTieu) {
	INT sum = 0;
	INT temp = 0;
	for (int i = 0; i < g_DS_ChiTieu.size(); i++)
	{
		sum += g_DS_ChiTieu[i]->GetSoTien();
		if(!lstrcmpW(LoaiChiTieu,g_DS_ChiTieu[i]->LoaiChiTieu))
			temp+= g_DS_ChiTieu[i]->GetSoTien();
	}
	if (temp == 0)
		return 0;
	return (FLOAT)temp / sum;
}

WCHAR *FloatToW(float number,int moRong) {
	int extra, c = 0, i = 0, count = 0;
	while (number != int(number))
	{
		number = number * 10;
		c++;
	} // * lan luot len de thanh mot so nguyen
	extra = number; //gan cho so nguyen de doi thanh chuoi de hon
	WCHAR *temp = new WCHAR[30];
	WCHAR *result = new WCHAR[30];
	while (extra > 0) // gan tung phan tu chia duoc vao mang
	{
		temp[i++] = extra % 10 + L'0';
		extra = extra / 10;
	}
	temp[i] = L'\0';
	int countm = 0;
	bool sauThapPhan = false;
	for (int j = i - 1; j >= 0; j--) // in ra mang theo dung thu tu
	{
		if (count == lstrlenW(temp) - c)
		{
			if (count == 0) {
				result[count++] = L'0';
			}
			countm = 0;
			sauThapPhan = true;
			result[count++] = L'.'; // gan ky tu '.'
			result[count++] = temp[j];
			countm++;
		}
		else {
			result[count++] = temp[j];
			if (sauThapPhan)
				countm++;
			if (countm >= moRong)
				break;
		}
	}
	if (count == 0)
	{
		result[count++] = L'0';
	}
	result[count] = L'\0';
	result[count + 1] = L'\0';
	return result;
}

VOID KhoiTaoCBBNoiDungChi() {
	//Thêm sub cho Ăn uống
	WCHAR *temp = new WCHAR[50];
	wsprintf(temp, L"Sáng");
	g_DS_SUB_AU.push_back(temp);
	temp = new WCHAR[50];
	wsprintf(temp, L"Trưa");
	g_DS_SUB_AU.push_back(temp);
	temp = new WCHAR[50];
	wsprintf(temp, L"Chiều");
	g_DS_SUB_AU.push_back(temp);
	temp = new WCHAR[50];
	wsprintf(temp, L"Xế");
	g_DS_SUB_AU.push_back(temp);
	temp = new WCHAR[50];
	wsprintf(temp, L"Tối");
	g_DS_SUB_AU.push_back(temp);
	temp = new WCHAR[50];
	wsprintf(temp, L"Khuya");
	g_DS_SUB_AU.push_back(temp);
	temp = new WCHAR[50];
	wsprintf(temp, L"Trà sữa");
	g_DS_SUB_AU.push_back(temp);

	//Thêm sub cho Di chuyển
	temp = new WCHAR[50];
	wsprintf(temp, L"Bus");
	g_DS_SUB_DC.push_back(temp);
	temp = new WCHAR[50];
	wsprintf(temp, L"Đổ xăng");
	g_DS_SUB_DC.push_back(temp);
	temp = new WCHAR[50];
	wsprintf(temp, L"Taxi");
	g_DS_SUB_DC.push_back(temp);
	temp = new WCHAR[50];
	wsprintf(temp, L"Bơm xe");
	g_DS_SUB_DC.push_back(temp);
	temp = new WCHAR[50];
	wsprintf(temp, L"Tàu");
	g_DS_SUB_DC.push_back(temp);

	//Thêm sub cho Nhà cửa
	temp = new WCHAR[50];
	wsprintf(temp, L"Tiền thuê nhà");
	g_DS_SUB_NC.push_back(temp);
	temp = new WCHAR[50];
	wsprintf(temp, L"Nước");
	g_DS_SUB_NC.push_back(temp);
	temp = new WCHAR[50];
	wsprintf(temp, L"Điện");
	g_DS_SUB_NC.push_back(temp);
	temp = new WCHAR[50];
	wsprintf(temp, L"Nước lau nhà");
	g_DS_SUB_NC.push_back(temp);

	//Thêm sub cho Xe cộ
	temp = new WCHAR[50];
	wsprintf(temp, L"Đổ xăng");
	g_DS_SUB_XC.push_back(temp);
	temp = new WCHAR[50];
	wsprintf(temp, L"Vá xe");
	g_DS_SUB_XC.push_back(temp);
	temp = new WCHAR[50];
	wsprintf(temp, L"Bảo dưỡng");
	g_DS_SUB_XC.push_back(temp);
	temp = new WCHAR[50];
	wsprintf(temp, L"Gởi xe");
	g_DS_SUB_XC.push_back(temp);

	//Thêm sub cho Nhu yếu phẩm
	temp = new WCHAR[50];
	wsprintf(temp, L"Xà bông");
	g_DS_SUB_NYP.push_back(temp);
	temp = new WCHAR[50];
	wsprintf(temp, L"Sữa tắm");
	g_DS_SUB_NYP.push_back(temp);
	temp = new WCHAR[50];
	wsprintf(temp, L"Dao cạo râu");
	g_DS_SUB_NYP.push_back(temp);

	//Thêm sub cho Dịch vụ
	temp = new WCHAR[50];
	wsprintf(temp, L"Internet");
	g_DS_SUB_DV.push_back(temp); temp = new WCHAR[50];
	wsprintf(temp, L"Thẻ cào điện thoại");
	g_DS_SUB_DV.push_back(temp);
}

VOID ReLoadCBBNoiDungChiTieu() {
	WCHAR LoaiChiTieu[50];
	SendMessage(g_NoiDungChi, CB_RESETCONTENT, NULL, NULL);//xóa sạch dữ liệu ở mục Nội dung chi
	int index = SendMessage(g_LoaiChiTieu, CB_GETCURSEL, NULL, NULL); //Lấy Loại chi tiêu
	switch (index) // so sánh kí tự thứ 5 của Loại chi tiêu
	{
	case 0://ăn uống
	{
		for (int i = 0; i < g_DS_SUB_AU.size(); i++) {
			ComboBox_AddString(g_NoiDungChi, g_DS_SUB_AU[i]);
		}
		break;
	}
	case 1://Di chuyển
	{
		for (int i = 0; i < g_DS_SUB_DC.size(); i++) {
			ComboBox_AddString(g_NoiDungChi, g_DS_SUB_DC[i]);
		}
		break;
	}
	case 2: //Nhà cửa
	{
		for (int i = 0; i < g_DS_SUB_NC.size(); i++) {
			ComboBox_AddString(g_NoiDungChi, g_DS_SUB_NC[i]);
		}
		break;
	}
	case 3: //Xe cộ
	{
		for (int i = 0; i < g_DS_SUB_XC.size(); i++) {
			ComboBox_AddString(g_NoiDungChi, g_DS_SUB_XC[i]);
		}
		break;
	}
	case 4: //Nhu yếu phẩm
	{
		for (int i = 0; i < g_DS_SUB_NYP.size(); i++) {
			ComboBox_AddString(g_NoiDungChi, g_DS_SUB_NYP[i]);
		}
		break;
	}
	case 5: //Dịch vụ
	{
		for (int i = 0; i < g_DS_SUB_DV.size(); i++) {
			ComboBox_AddString(g_NoiDungChi, g_DS_SUB_DV[i]);
		}
		break;
	}
	default:
		break;
	}
}

VOID ReLoadListView() {
	SendMessage(g_ListView, LVM_DELETEALLITEMS, 0, 0);
	g_CountLV = 0;
	for (int i = 0; i < g_DS_ChiTieu.size(); i++) {
		Insert_LVItem(g_DS_ChiTieu[i]);
	}
}

//Load các icon lên danh sách image
void TaoReSourchIcon()
{
	HICON s;
	g_hml32 = ImageList_Create(16, 16, ILC_MASK | ILC_COLOR32, 1, 1);

	s = LoadIcon(hInst, MAKEINTRESOURCE(IDI_ICON1));//Ăn uống
	ImageList_AddIcon(g_hml32, s);
	s = LoadIcon(hInst, MAKEINTRESOURCE(IDI_ICON2));//Di chuyển
	ImageList_AddIcon(g_hml32, s);
	s = LoadIcon(hInst, MAKEINTRESOURCE(IDI_ICON3));//Dịch vụ
	ImageList_AddIcon(g_hml32, s);
	s = LoadIcon(hInst, MAKEINTRESOURCE(IDI_ICON4));//Nhà cửa
	ImageList_AddIcon(g_hml32, s);
	s = LoadIcon(hInst, MAKEINTRESOURCE(IDI_ICON5));//Nhu yếu phẩm
	ImageList_AddIcon(g_hml32, s);
	s = LoadIcon(hInst, MAKEINTRESOURCE(IDI_ICON6));//Xe cộ
	ImageList_AddIcon(g_hml32, s);

	ListView_SetImageList(g_ListView, g_hml32,1);

	DeleteObject(s);
}

BOOL DialogOpenFile(HWND hwnd, WCHAR fileName[])
{
	OPENFILENAMEW ofn = { 0 };
	WCHAR szFile[260];
	ofn.lStructSize = sizeof(ofn);
	ofn.hwndOwner = hwnd;
	ofn.lpstrFile = szFile;
	ofn.lpstrFile[0] = '\0';
	ofn.nMaxFile = sizeof(szFile);
	ofn.lpstrFilter = L"File name (*.bak)\0*.bak\0All File (*.*)\0*.*\0";
	ofn.nFilterIndex = 1;
	ofn.lpstrFileTitle = NULL;
	ofn.nMaxFileTitle = 0;
	ofn.lpstrInitialDir = NULL;
	ofn.Flags = OFN_PATHMUSTEXIST | OFN_FILEMUSTEXIST;
	if (!GetOpenFileNameW(&ofn))
		return FALSE;
	wsprintf(fileName, L"%s", ofn.lpstrFile);
	//wcsncpy(chuoiten,ofn.lpstrFile,lstrlenW(ofn.lpstrFile));
	return TRUE;
}

BOOL DiaLogSaveFile(HWND hwnd, WCHAR fileName[])
{
	OPENFILENAMEW ofn = { 0 };
	WCHAR szFile[260] = TEXT("File name.bak");
	ofn.lStructSize = sizeof(ofn);
	ofn.hwndOwner = hwnd;
	ofn.lpstrFile = szFile;
	ofn.nMaxFile = sizeof(szFile);
	ofn.lpstrFilter = TEXT("File name (*.bak)\0*.bak\0All File (*.*)\0*.*\0");
	ofn.nFilterIndex = 1;
	ofn.lpstrFileTitle = NULL;
	ofn.nMaxFileTitle = 0;
	ofn.lpstrInitialDir = NULL;
	ofn.Flags = OFN_PATHMUSTEXIST | OFN_FILEMUSTEXIST;
	if (GetSaveFileName(&ofn) == FALSE)
		return FALSE;
	wsprintf(fileName, L"%s", ofn.lpstrFile);
	return TRUE;
}

VOID NbStrToStr(WCHAR *numberStrTo,WCHAR *numberStrSrc) {
	int length = lstrlenW(numberStrSrc);
	//độ dài số dưới 3
	if (length <= 3) {
		for (int i = 0; i <= length; i++) {
			numberStrTo[i] = numberStrSrc[i];
		}
		return;
	}
	//độ dài số trên 3
	int first = length % 3;
	int count = 0;
	int j = 0;
	int i = 0;
	for (; i <first; i++) {
		numberStrTo[j++] = numberStrSrc[i];
	}
	if(first!=0)
		numberStrTo[j++] = L'.';
	for (; i < length; i++) {
		if (count == 3) {
			numberStrTo[j++] = L'.';
			count = 0;
		}
		numberStrTo[j++] = numberStrSrc[i];
		count++;
	}
	numberStrTo[j++] = L'\0';
}

VOID UpdateTotalForMount() {
	int TongChiTieu = 0;
	WCHAR buff2[100];
	SYSTEMTIME *sysTime = new SYSTEMTIME();
	GetSystemTime(sysTime);
	MYDATE dataTemp;
	if (sysTime->wDay < g_StartDate)//Nếu ThisDate < g_StartDate thì tính tổng từ g_StarDate tháng trước đến Now
	{
		wsprintf(buff2, L"Tính từ ngày %d/%d/%d", sysTime->wMonth -1, g_StartDate, sysTime->wYear);
		for (int i = 0; i < g_DS_ChiTieu.size(); i++) {
			dataTemp.SetDate(g_DS_ChiTieu[i]->Date);
			if (dataTemp.mm == sysTime->wMonth - 1)
			{
				if(dataTemp.dd >= g_StartDate)
					TongChiTieu += g_DS_ChiTieu[i]->GetSoTien();
			}
			else
			if (dataTemp.mm == sysTime->wMonth)
			{
				TongChiTieu += g_DS_ChiTieu[i]->GetSoTien();
			}
		}
	}
	else //Nếu ThisDate >= g_StartDate thì tính tổng từ g_StarDate tháng này đến giờ
	{
		wsprintf(buff2, L"Tính từ ngày %d/%d/%d", sysTime->wMonth, g_StartDate, sysTime->wYear);
		for (int i = 0; i < g_DS_ChiTieu.size(); i++) {
			dataTemp.SetDate(g_DS_ChiTieu[i]->Date);
			if (dataTemp.mm == sysTime->wMonth && dataTemp.dd >= g_StartDate)
			{
				TongChiTieu += g_DS_ChiTieu[i]->GetSoTien();
			}
		}
	}
	WCHAR buff[100];
	WCHAR buff1[100];
	wsprintf(buff, L"%d", TongChiTieu);
	NbStrToStr(buff1, buff);
	wsprintf(buff1, L"%s Đ", buff1);
	SetWindowText(g_LssTongChiTieu, buff1);
	SetWindowText(g_DateThongBao, buff2);

	delete sysTime;
}