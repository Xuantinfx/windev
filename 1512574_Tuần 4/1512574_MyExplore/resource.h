//{{NO_DEPENDENCIES}}
// Microsoft Visual C++ generated include file.
// Used by 1512574MyExplore.rc
//
#define IDC_MYICON                      2
#define IDD_1512574MYEXPLORE_DIALOG     102
#define IDS_APP_TITLE                   103
#define IDD_ABOUTBOX                    103
#define IDM_ABOUT                       104
#define IDM_EXIT                        105
#define IDI_1512574MYEXPLORE            107
#define IDI_SMALL                       108
#define IDC_1512574MYEXPLORE            109
#define IDC_TREEVIEW                    110
#define IDC_LISTVIEW                    111
#define IDC_BACK                        112
#define IDR_MAINFRAME                   128
#define IDI_ICON1                       129
#define IDI_ICON2                       130
#define IDI_ICON3                       131
#define IDI_ICON4                       132
#define IDI_ICON5                       133
#define IDI_ICON6                       134
#define IDI_ICON7                       135
#define IDI_ICON8                       141
#define IDC_STATIC                      -1

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NO_MFC                     1
#define _APS_NEXT_RESOURCE_VALUE        142
#define _APS_NEXT_COMMAND_VALUE         32771
#define _APS_NEXT_CONTROL_VALUE         1000
#define _APS_NEXT_SYMED_VALUE           114
#endif
#endif
