#pragma once

#include "resource.h"

#define MAX_LOADSTRING 100

// Global Variables:
static HINSTANCE hInst;                                // current instance
static WCHAR szTitle[MAX_LOADSTRING];                  // The title bar text
static WCHAR szWindowClass[MAX_LOADSTRING];            // the main window class name

struct Drive
{
	WCHAR buffer[300] = { 0 };
	WCHAR driver[26][4] = { 0 };
	WCHAR driveType[26][30] = { 0 };
	WCHAR displayName[26][300] = { 0 };
	int icon[8] = { 0 };
	int count = 0;
};

static Drive g_Drive;
static WCHAR g_Address[2048] = { 0 };
static HWND g_EditAddress;
static HWND g_HwndTreeView;
static HWND g_HwndListView;
static HWND g_HwndButton;
static LOGFONT g_lf;
static HFONT g_hfont;
static HICON g_s;
extern HIMAGELIST g_hml16;
extern HIMAGELIST g_hml32;