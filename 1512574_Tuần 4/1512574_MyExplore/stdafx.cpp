﻿// stdafx.cpp : source file that includes just the standard includes
// $safeprojectname$.pch will be the pre-compiled header
// stdafx.obj will contain the pre-compiled type information

#include "stdafx.h"
#include "1512574MyExplore.h"

// TODO: reference any additional headers you need in STDAFX.H
// and not in this file


//Khởi tạo các cột cho thư mục và tập tin
void ColForFolder(HWND hWndListView)
{
	LVCOLUMN lvCol;

	lvCol.mask = LVCF_FMT | LVCF_TEXT | LVCF_WIDTH;
	lvCol.fmt = LVCFMT_LEFT;

	lvCol.cx = 200;
	lvCol.pszText = _T("Tên");
	ListView_InsertColumn(hWndListView, 0, &lvCol);

	lvCol.cx = 70;
	lvCol.fmt = LVCFMT_RIGHT;

	lvCol.pszText = _T("Kích thước");
	ListView_InsertColumn(hWndListView, 1, &lvCol);

	lvCol.cx = 80;
	lvCol.fmt = LVCFMT_LEFT;
	lvCol.pszText = _T("Loại");
	ListView_InsertColumn(hWndListView, 2, &lvCol);


	lvCol.cx = 300;
	lvCol.pszText = _T("Thời gian chỉnh sửa");
	ListView_InsertColumn(hWndListView, 3, &lvCol);

}
//Khởi tạo các cột cho Ổ đĩa
void ColForDrive(HWND hWndListView)
{
	LVCOLUMN lvCol;
	lvCol.mask = LVCF_FMT | LVCF_TEXT | LVCF_WIDTH;
	lvCol.fmt = LVCFMT_LEFT;

	lvCol.cx = 200;
	lvCol.pszText = _T("Tên");
	ListView_InsertColumn(hWndListView, 0, &lvCol);

	lvCol.cx = 80;
	lvCol.pszText = _T("Kích thước");
	ListView_InsertColumn(hWndListView, 2, &lvCol);

	lvCol.cx = 100;
	lvCol.pszText = _T("Loại");
	ListView_InsertColumn(hWndListView, 1, &lvCol);


	lvCol.cx = 300;
	lvCol.pszText = _T("Dung lượng trống");
	ListView_InsertColumn(hWndListView, 3, &lvCol);

}

//Load treeview và ổ đĩa
void LoadTreeView(HWND hWndTreeView)
{
	HTREEITEM PC;
	TV_INSERTSTRUCT tv;
	tv.hParent = NULL;
	tv.hInsertAfter = TVI_LAST;
	tv.item.mask = TVIF_TEXT | TVIF_IMAGE | TVIF_PARAM;
	tv.item.iImage = IDI_MYCOMPUTER;
	tv.item.iSelectedImage = IDI_MYCOMPUTER;
	tv.item.pszText = L"This PC";
	tv.item.lParam = (LPARAM)L"This PC";

	PC = TreeView_InsertItem(hWndTreeView, &tv);

	GetLogicalDriveStrings(300, g_Drive.buffer);

	for (int i = 0; !(g_Drive.buffer[i] == 0 && g_Drive.buffer[i + 1] == 0); i++)
		if (g_Drive.buffer[i] == 0)
		{
			g_Drive.count++;
		}
	g_Drive.count++;

	int m = 0;;
	for (int i = 0; m < g_Drive.count; i += 4)
	{
		wcscpy_s(g_Drive.driver[m], &g_Drive.buffer[i]);
		m++;
	}

	HTREEITEM it_child;
	for (int i = 0; i < g_Drive.count; i++)
	{
		memset(g_Drive.buffer, 0, 300);
		tv.hParent = PC;
		tv.hInsertAfter = TVI_LAST;
		tv.item.mask = TVIF_TEXT | TVIF_IMAGE | TVIF_SELECTEDIMAGE | TVIF_PARAM;
		int type = GetDriveType(g_Drive.driver[i]);
		if (type == DRIVE_FIXED)
		{
			g_Drive.icon[i] = IDI_HDD;
			wcscpy_s(g_Drive.driveType[i], L"Local Disk");
			GetVolumeInformation(g_Drive.driver[i], g_Drive.buffer, 300, NULL, NULL, NULL, NULL, 0);
			wcscpy_s(g_Drive.displayName[i], g_Drive.buffer);
			if (g_Drive.buffer[0] == 0)
				wcscat_s(g_Drive.displayName[i], L"Local Disk");
		}
		else if (type == DRIVE_CDROM)
		{
			g_Drive.icon[i] = IDI_CD;
			wcscpy_s(g_Drive.displayName[i], L"CD Drive");
			wcscpy_s(g_Drive.driveType[i], L"CD Drive");
		}
		else if (i > 1 && type == DRIVE_REMOVABLE)
		{
			g_Drive.icon[i] = IDI_USB;
			wcscpy_s(g_Drive.displayName[i], L"USB");
			wcscpy_s(g_Drive.driveType[i], L"Removeble Disk");

		}

		wcscat_s(g_Drive.displayName[i], L" (");
		wcsncat_s(g_Drive.displayName[i], g_Drive.driver[i], 1);
		wcscat_s(g_Drive.displayName[i], L":)");

		tv.item.iImage = g_Drive.icon[i];
		tv.item.iSelectedImage = g_Drive.icon[i];
		tv.item.pszText = g_Drive.displayName[i];
		tv.item.lParam = (LPARAM)g_Drive.driver[i];

		it_child = TreeView_InsertItem(hWndTreeView, &tv);

		if (type == DRIVE_FIXED || (type == DRIVE_REMOVABLE))
		{
			tv.hParent = it_child;
			tv.item.pszText = NULL; //Them
			tv.item.lParam = (LPARAM)L"Vitual";
			TreeView_InsertItem(hWndTreeView, &tv);
		}
	}
	//TreeView_Expand(hWndTreeView, PC, TVE_EXPAND);
}
//Load This PC và con là các ổ đĩa vào ListView
void LoadListView(HWND hWndListView)
{
	for (int i = 0; i < 4; i++)
		ListView_DeleteColumn(hWndListView, 0);
	ColForDrive(hWndListView);
	for (int i = 0; i < g_Drive.count; i++)
	{
		LV_ITEM lv;
		lv.mask = LVIF_TEXT | LVIF_IMAGE | LVIF_PARAM;
		lv.iItem = i;
		lv.iImage = g_Drive.icon[i];

		lv.iSubItem = 0;//cột tên
		lv.pszText = g_Drive.displayName[i];
		lv.lParam = (LPARAM)g_Drive.driver[i];
		ListView_InsertItem(hWndListView, &lv);
		lv.mask = LVIF_TEXT;

		lv.iSubItem = 1;//Cột loại
		lv.pszText = g_Drive.driveType[i];
		ListView_SetItem(hWndListView, &lv);

		if (g_Drive.icon[i] != 3)
		{
			lv.iSubItem = 2;//cột kích thước
			__int64 size;
			SHGetDiskFreeSpaceEx(g_Drive.driver[i], NULL, (PULARGE_INTEGER)&size, NULL);
			lv.pszText = ConvertSize(size);
			ListView_SetItem(hWndListView, &lv);

			lv.iSubItem = 3;//Dung lượng trống
			SHGetDiskFreeSpaceEx(g_Drive.driver[i], NULL, NULL, (PULARGE_INTEGER)&size);
			lv.pszText = ConvertSize(size);
			ListView_SetItem(hWndListView, &lv);
		}

	}
}

//Load thư mục và tập tin vào list view
void LoadListView(HWND hWndListView, LPCWSTR path)
{
	for (int i = 0; i < 4; i++)
		ListView_DeleteColumn(hWndListView, 0);
	if (path == NULL)
		return;
	if (wcscmp(path, L"This PC") == 0)
	{
		LoadListView(hWndListView);
		return;
	}
	ColForFolder(hWndListView);

	WCHAR buffer[1024];
	wcscpy_s(buffer, path);

	if (wcslen(path) == 3)
		wcscat_s(buffer, _T("*"));
	else
		wcscat_s(buffer, _T("\\*"));

	WIN32_FIND_DATA fd;
	HANDLE hFile;
	BOOL bFound = true;
	TCHAR * folderPath;
	int count = 0;

	//Chạy lần thứ nhất lấy các thư mục
	hFile = FindFirstFileW(buffer, &fd);
	bFound = TRUE;

	if (hFile == INVALID_HANDLE_VALUE)
		bFound = FALSE;

	while (bFound)
	{
		if ((fd.dwFileAttributes & FILE_ATTRIBUTE_DIRECTORY) &&
			((fd.dwFileAttributes & FILE_ATTRIBUTE_HIDDEN) != FILE_ATTRIBUTE_HIDDEN) &&
			(wcscmp(fd.cFileName, L".") != 0) && (wcscmp(fd.cFileName, L"..") != 0))
		{
			folderPath = new WCHAR[wcslen(path) + wcslen(fd.cFileName) + 2];
			wcscpy_s(folderPath, wcslen(path) + wcslen(fd.cFileName) + 2, path);

			if (wcslen(path) != 3)
				wcscat_s(folderPath, wcslen(path) + wcslen(fd.cFileName) + 2, L"\\");

			wcscat_s(folderPath, wcslen(path) + wcslen(fd.cFileName) + 2, fd.cFileName);
			LV_ITEM lv;
			lv.mask = LVIF_TEXT | LVIF_IMAGE | LVIF_PARAM;
			lv.iItem = count;
			lv.iImage = IDI_FOLDER;
			lv.iSubItem = 0;		//Côt tên thư mục
			lv.pszText = fd.cFileName;
			lv.lParam = (LPARAM)folderPath;
			ListView_InsertItem(hWndListView, &lv);

			ListView_SetItemText(hWndListView, count, 2, L"Thư mục");//Cột loại

			ListView_SetItemText(hWndListView, count, 3, GetDateModified(fd.ftLastWriteTime));//Cột Thời gian
			count++;
		}
		bFound = FindNextFileW(hFile, &fd);
	}

	DWORD folderCount = count;// Số lượng thư mục

	//Chạy lần thứ hai lấy các tập tin, thầy Quang bảo đây là cách chuỗi nhất từng thấy, 
	//nhưng mà k còn cách khác
	WCHAR * filePath;
	DWORD fileSizeCount = 0;
	DWORD fileCount = 0;

	hFile = FindFirstFileW(buffer, &fd);
	bFound = TRUE;

	if (hFile == INVALID_HANDLE_VALUE)
		bFound = FALSE;

	while (bFound)
	{
		if (((fd.dwFileAttributes & FILE_ATTRIBUTE_DIRECTORY) != FILE_ATTRIBUTE_DIRECTORY) &&
			((fd.dwFileAttributes & FILE_ATTRIBUTE_SYSTEM) != FILE_ATTRIBUTE_SYSTEM) &&
			((fd.dwFileAttributes & FILE_ATTRIBUTE_HIDDEN) != FILE_ATTRIBUTE_HIDDEN))
		{
			filePath = new WCHAR[wcslen(path) + wcslen(fd.cFileName) + 2];
			wcscpy_s(filePath, wcslen(path) + wcslen(fd.cFileName) + 2, path);

			if (wcslen(path) != 3)
				wcscat_s(filePath, wcslen(path) + wcslen(fd.cFileName) + 2, L"\\");

			wcscat_s(filePath, wcslen(path) + wcslen(fd.cFileName) + 2, fd.cFileName);
			LVITEM lv;
			lv.mask = LVIF_TEXT | LVIF_IMAGE | LVIF_PARAM;
			lv.iItem = count;
			lv.iImage = IDI_FILE;
			if (wcscmp(GetTypeFile(fd.cFileName), L"exe") == 0)
				lv.iImage = IDI_EXE;
			lv.iSubItem = 0;	//Cột tên
			lv.pszText = fd.cFileName;
			lv.lParam = (LPARAM)filePath;

			ListView_InsertItem(hWndListView, &lv);

			ListView_SetItemText(hWndListView, count, 1,
				ConvertSize(fd.nFileSizeLow)); // Cột size

			ListView_SetItemText(hWndListView, count, 2, GetTypeFile(fd.cFileName)); // Cột loại

			ListView_SetItemText(hWndListView, count, 3, GetDateModified(fd.ftLastWriteTime)); //Cột ngày chỉnh sửa

			++count;
			++fileCount;
		}
		bFound = FindNextFileW(hFile, &fd);
	}
}

//Đổi thời gian sang chuỗi
LPWSTR GetDateModified(const FILETIME &ftLastWrite)
{
	SYSTEMTIME t;
	FileTimeToSystemTime(&ftLastWrite, &t);
	WCHAR *buffer = new WCHAR[50];
	swprintf_s(buffer, 50, L"%d/%02d/%02d %02d:%02d",
		t.wDay, t.wMonth, t.wYear, t.wHour,
		t.wMinute);
	return buffer;
}

//Lấy đuôi của tập tin
LPWSTR GetTypeFile(LPWSTR name)
{
	int i;
	int len = wcslen(name);
	WCHAR* res = new WCHAR[30];
	for (i = len - 1; i >= 0 && name[i] != '.'; i--);
	if (i == -1)
		return L"File";
	wsprintf(res, L"%s", &name[i + 1]);
	return res;
}

//Trở về thư mục trước
LPCWSTR GoBack(LPCWSTR path)
{
	if (wcscmp(path, L"This PC") == 0)
		return path;
	int len = wcslen(path);
	if (len <= 3)//ổ đĩa
	{
		swprintf_s((WCHAR*)path, 8, L"This PC");
		return path;
	}
	int i;
	for (i = len - 1; i > 0 && path[i] != '\\'; i--);
	if (i > 2)
		(WCHAR)path[i] = 0;
	else
		(WCHAR)path[i + 1] = 0;
	return path;
}

#define KB 1
#define MB 2
#define GB 3
#define TB 4
#define RADIX 10

//Hàm chuyển đổi kích thước lấy từ code của thầy Quang
LPWSTR ConvertSize(__int64 nSize)
{
	int nType = 0; //Bytes

	while (nSize >= 1048576) //
	{
		nSize /= 1024;
		++nType;
	}

	__int64 nRight;

	if (nSize >= 1024)
	{
		//Lấy một chữ số sau thập phân của nSize chứa trong nRight
		nRight = nSize % 1024;

		while (nRight > 99)
			nRight /= 10;

		nSize /= 1024;
		++nType;
	}
	else
		nRight = 0;

	TCHAR *buffer = new TCHAR[11];
	_itow_s(nSize, buffer, 11, RADIX);

	if (nRight != 0 && nType > KB)
	{
		StrCat(buffer, _T("."));
		TCHAR *right = new TCHAR[3];
		_itow_s(nRight, right, 3, RADIX);
		StrCat(buffer, right);
	}

	switch (nType)
	{
	case 0://Bytes
		StrCat(buffer, _T(" bytes"));
		break;
	case KB:
		StrCat(buffer, _T(" KB"));
		break;
	case MB:
		StrCat(buffer, _T(" MB"));
		break;
	case GB:
		StrCat(buffer, _T(" GB"));
		break;
	case TB:
		StrCat(buffer, _T(" TB"));
		break;
	}

	return buffer;
}

//Xóa hCurSel nếu nó là item ảo mang tên Virtual và load lại nó với hàm Load
void PreloadExpanding(HWND hWndTV, HTREEITEM hCurSel)
{
	HTREEITEM hCurSelChild = TreeView_GetChild(hWndTV, hCurSel);

	if (!wcscmp(GetPath(hWndTV, hCurSelChild), L"Vitual"))
	{
		TreeView_DeleteItem(hWndTV, hCurSelChild);
		Load(hWndTV, hCurSel);
	}
}

//Lấy đường dẫn của đối tượng hItem
LPCWSTR GetPath(HWND hWndTV, HTREEITEM hItem)
{
	WCHAR * buffer = new WCHAR[1024];
	TVITEM tv;
	tv.mask = TVIF_PARAM;
	tv.hItem = hItem;
	tv.lParam = (LPARAM)buffer;
	TreeView_GetItem(hWndTV, &tv);
	return (LPCWSTR)tv.lParam;
}

//Lấy đường dẫn từ thứ tự trong listview
LPCWSTR GetPath(HWND hWndListView, int iItem)
{
	WCHAR * buffer = new WCHAR[1024];
	LVITEM lv;
	lv.mask = LVIF_PARAM;
	lv.iItem = iItem;
	lv.iSubItem = 0;
	lv.lParam = (LPARAM)buffer;
	ListView_GetItem(hWndListView, &lv);
	return (LPCWSTR)lv.lParam;
}

//Load các item con của item tree hParent
void Load(HWND hWndTreeView, HTREEITEM hParent)
{
	LPCWSTR path = GetPath(hWndTreeView, hParent);
	if (path == NULL)
		return;
	WCHAR buffer[1024];
	wcscpy_s(buffer, path);

	if (wcslen(path) == 3) //Nếu quét các ổ đĩa
		wcscat_s(buffer, _T("*"));
	else
		wcscat_s(buffer, _T("\\*"));

	TV_INSERTSTRUCT tvInsert;
	tvInsert.hParent = hParent;
	tvInsert.hInsertAfter = TVI_LAST;
	tvInsert.item.mask = TVIF_TEXT | TVIF_IMAGE | TVIF_SELECTEDIMAGE | TVIF_PARAM;
	

	WIN32_FIND_DATA fd;
	HANDLE hFile = FindFirstFileW(buffer, &fd);
	BOOL bFound = 1;

	if (hFile == INVALID_HANDLE_VALUE)
		bFound = FALSE;

	WCHAR * folderPath;
	while (bFound)
	{
		if ((fd.dwFileAttributes & FILE_ATTRIBUTE_DIRECTORY)
			&& ((fd.dwFileAttributes & FILE_ATTRIBUTE_HIDDEN) != FILE_ATTRIBUTE_HIDDEN)
			&& ((fd.dwFileAttributes & FILE_ATTRIBUTE_SYSTEM) != FILE_ATTRIBUTE_SYSTEM)
			&& (wcscmp(fd.cFileName, L".") != 0) && wcscmp(fd.cFileName, L"..") != 0)
		{
			tvInsert.item.pszText = fd.cFileName;
			tvInsert.item.iImage = IDI_FOLDER;
			tvInsert.item.iSelectedImage = IDI_FOLDER;
			folderPath = new WCHAR[wcslen(path) + wcslen(fd.cFileName) + 2];
			wcscpy_s(folderPath, wcslen(path) + wcslen(fd.cFileName) + 2, path);
			if (wcslen(path) != 3)
				wcscat_s(folderPath, wcslen(path) + wcslen(fd.cFileName) + 2, L"\\");
			wcscat_s(folderPath, wcslen(path) + wcslen(fd.cFileName) + 2, fd.cFileName);

			tvInsert.item.lParam = (LPARAM)folderPath;
			HTREEITEM hItem = TreeView_InsertItem(hWndTreeView, &tvInsert);

			//Gọi hàm để tạo dấu expand nếu hItem có con 
			PreLoad(hWndTreeView, hItem);
		}

		bFound = FindNextFileW(hFile, &fd);
	}
}

//Tạo item giả con cho hItem nếu hItem có con
//Mục đích: Cho hItem có dấu + 
void PreLoad(HWND hWndTV, HTREEITEM hItem)
{
	WCHAR buffer[1024];
	wcscpy_s(buffer, GetPath(hWndTV, hItem));
	wcscat_s(buffer, L"\\");
	wcscat_s(buffer, L"*");
	WIN32_FIND_DATA fd;
	HANDLE hFile = FindFirstFileW(buffer, &fd);
	if (hFile == INVALID_HANDLE_VALUE)
		return;
	BOOL bFound = true;
	while (bFound)
	{
		if ((fd.dwFileAttributes & FILE_ATTRIBUTE_DIRECTORY)
			&& (wcscmp(fd.cFileName, L".") != 0) && (wcscmp(fd.cFileName, L"..") != 0))
		{
			TV_INSERTSTRUCT tvInsert;
			tvInsert.hParent = hItem;
			tvInsert.hInsertAfter = TVI_LAST;
			tvInsert.item.mask = TVIF_TEXT | TVIF_PARAM;
			tvInsert.item.pszText = NULL;
			tvInsert.item.lParam = (LPARAM)L"Vitual";
			TreeView_InsertItem(hWndTV, &tvInsert);
			bFound = FALSE;
		}
		else
			bFound = FindNextFileW(hFile, &fd);
	}
}

//Click vào item ở listview
int ClickItem(HWND hWndListView, LPCWSTR path)
{
	if (!path)
		return 0;
	WIN32_FIND_DATA fd;
	GetFileAttributesEx(path, GetFileExInfoStandard, &fd);
	if (fd.dwFileAttributes & FILE_ATTRIBUTE_DIRECTORY)
	{
		ListView_DeleteAllItems(hWndListView);
		LoadListView(hWndListView, path);
		return 0;
	}
	ShellExecute(NULL, L"open", path, NULL, NULL, SW_SHOWNORMAL);
	return 1;
}
