﻿// 1512574MyExplore.cpp : Defines the entry point for the application.
//

#include "stdafx.h"
#include "1512574MyExplore.h"

// Forward declarations of functions included in this code module:
ATOM                MyRegisterClass(HINSTANCE hInstance);
BOOL                InitInstance(HINSTANCE, int);
LRESULT CALLBACK    WndProc(HWND, UINT, WPARAM, LPARAM);
INT_PTR CALLBACK    About(HWND, UINT, WPARAM, LPARAM);


BOOL OnCreate(HWND hwnd, LPCREATESTRUCT lpCreateStruct);
void OnCommand(HWND hwnd, int id, HWND hwndCtl, UINT codeNotify);
void OnPaint(HWND hwnd);
void OnSize(HWND hwnd, UINT state, int cx, int cy);
void TaoReSourchIcon();
DWORD WINAPI TreeViewExpand(LPVOID lpParameter);
DWORD WINAPI ListViewLoad(LPVOID lpParameter);

HIMAGELIST g_hml16;
HIMAGELIST g_hml32;
HTREEITEM g_HtreeViewChild;
LPCWSTR m_path_ListView;

int APIENTRY wWinMain(_In_ HINSTANCE hInstance,
                     _In_opt_ HINSTANCE hPrevInstance,
                     _In_ LPWSTR    lpCmdLine,
                     _In_ int       nCmdShow)
{
    UNREFERENCED_PARAMETER(hPrevInstance);
    UNREFERENCED_PARAMETER(lpCmdLine);

    // TODO: Place code here.

    // Initialize global strings
    LoadStringW(hInstance, IDS_APP_TITLE, szTitle, MAX_LOADSTRING);
    LoadStringW(hInstance, IDC_1512574MYEXPLORE, szWindowClass, MAX_LOADSTRING);
    MyRegisterClass(hInstance);

    // Perform application initialization:
    if (!InitInstance (hInstance, nCmdShow))
    {
        return FALSE;
    }

    HACCEL hAccelTable = LoadAccelerators(hInstance, MAKEINTRESOURCE(IDC_1512574MYEXPLORE));

    MSG msg;

    // Main message loop:
    while (GetMessage(&msg, nullptr, 0, 0))
    {
        if (!TranslateAccelerator(msg.hwnd, hAccelTable, &msg))
        {
            TranslateMessage(&msg);
            DispatchMessage(&msg);
        }
    }

    return (int) msg.wParam;
}

//
//  FUNCTION: MyRegisterClass()
//
//  PURPOSE: Registers the window class.
//
ATOM MyRegisterClass(HINSTANCE hInstance)
{
    WNDCLASSW wcex;

 //   wcex.cbSize = sizeof(WNDCLASSEX);
	WORD A = 0;
    wcex.style          = CS_HREDRAW | CS_VREDRAW;
    wcex.lpfnWndProc    = WndProc;
    wcex.cbClsExtra     = 0;
    wcex.cbWndExtra     = 0;
    wcex.hInstance      = hInstance;
//	wcex.hIcon = ExtractAssociatedIcon(hInstance, L"C:", &A);
//	wcex.hIcon = ExtractIcon(hInstance, L"%SystemRoot%\\System32\\shell32.dll", 18);
	wcex.hIcon = LoadIcon(wcex.hInstance, MAKEINTRESOURCE(IDI_1512574MYEXPLORE));

    wcex.hCursor        = LoadCursor(nullptr, IDC_ARROW);
    wcex.hbrBackground  = (HBRUSH)(COLOR_WINDOW+1);
    wcex.lpszMenuName   = MAKEINTRESOURCEW(IDC_1512574MYEXPLORE);
    wcex.lpszClassName  = szWindowClass;
//	wcex.hIconSm        = LoadIcon(wcex.hInstance, MAKEINTRESOURCE(IDI_1512574MYEXPLORE));

    return RegisterClassW(&wcex);
}

//
//   FUNCTION: InitInstance(HINSTANCE, int)
//
//   PURPOSE: Saves instance handle and creates main window
//
//   COMMENTS:
//
//        In this function, we save the instance handle in a global variable and
//        create and display the main program window.
//

BOOL InitInstance(HINSTANCE hInstance, int nCmdShow)
{
   hInst = hInstance; // Store instance handle in our global variable

   HWND hWnd = CreateWindowW(szWindowClass, szTitle, WS_OVERLAPPEDWINDOW,
      CW_USEDEFAULT, 0, CW_USEDEFAULT, 0, nullptr, nullptr, hInstance, nullptr);

   if (!hWnd)
   {
      return FALSE;
   }

   ShowWindow(hWnd, nCmdShow);
   UpdateWindow(hWnd);

   return TRUE;
}

//
//  FUNCTION: WndProc(HWND, UINT, WPARAM, LPARAM)
//
//  PURPOSE:  Processes messages for the main window.
//
//  WM_COMMAND  - process the application menu
//  WM_PAINT    - Paint the main window
//  WM_DESTROY  - post a quit message and return
//
//


LRESULT CALLBACK WndProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam)
{
    switch (message)
    {
		HANDLE_MSG(hWnd, WM_CREATE, OnCreate);
		HANDLE_MSG(hWnd, WM_COMMAND, OnCommand);
		HANDLE_MSG(hWnd, WM_PAINT, OnPaint);
		HANDLE_MSG(hWnd, WM_SIZE, OnSize);
	case WM_NOTIFY:
	{
		//lParam chứa đối tượng và mã sự kiện
		UINT code = ((LPNMHDR)lParam)->code;
		switch (code)
		{
		case TVN_ITEMEXPANDING:

			// đưa item muốn expand vào biến cục bộ để xử lý
			g_HtreeViewChild = ((LPNMTREEVIEW)(LPNMHDR)lParam)->itemNew.hItem;

			//Tạo một tiểu trình để nạp con cho TreeView
			CreateThread(NULL, 0, TreeViewExpand, 0, 0, 0);
			break;
		case TVN_SELCHANGED:
		{
			ListView_DeleteAllItems(g_HwndListView); //Xóa sạch List View

			//lấy đường dẫn từ file được chọn
			m_path_ListView = (LPCWSTR)((LPNMTREEVIEW)(LPNMHDR)lParam)->itemNew.lParam;
			
			//bắt đầu tiểu trình để nạp con cho listview
			CreateThread(0, 0,ListViewLoad, 0, 0, 0);

			SendMessage(g_EditAddress, WM_SETTEXT, 0, (LPARAM)g_Address);
			break;
		}
		case NM_DBLCLK:
		{
			if ((((LPNMHDR)lParam)->hwndFrom) == g_HwndListView)
			{
				if (ListView_GetSelectionMark(g_HwndListView) != -1)
				{
					LPCWSTR m_path = GetPath(g_HwndListView, ListView_GetSelectionMark(g_HwndListView));
					if (!ClickItem(g_HwndListView, m_path))
						wcscpy_s(g_Address, 2048, m_path);
					SendMessage(g_EditAddress, WM_SETTEXT, 0, (LPARAM)g_Address);
				}
			}
		}
			break;
		}
	}
		break;
    case WM_DESTROY:
        PostQuitMessage(0);
        break;
    default:
        return DefWindowProc(hWnd, message, wParam, lParam);
    }
    return 0;
}

// Message handler for about box.
INT_PTR CALLBACK About(HWND hDlg, UINT message, WPARAM wParam, LPARAM lParam)
{
    UNREFERENCED_PARAMETER(lParam);
    switch (message)
    {
    case WM_INITDIALOG:
        return (INT_PTR)TRUE;

    case WM_COMMAND:
        if (LOWORD(wParam) == IDOK || LOWORD(wParam) == IDCANCEL)
        {
            EndDialog(hDlg, LOWORD(wParam));
            return (INT_PTR)TRUE;
        }
        break;
    }
    return (INT_PTR)FALSE;
}

BOOL OnCreate(HWND hwnd, LPCREATESTRUCT lpCreateStruct)
{
	//đặt lại tên cho ứng dụng
	wsprintf(szTitle, L"My Explorer");
	SetWindowText(hwnd, szTitle);
	/*****************/

	//Khởi tạo font chữ cho ứng dụng
	g_lf.lfHeight = 16;
	g_lf.lfWidth = 5;
	wcscpy_s(g_lf.lfFaceName, L"Times New Roman");
	g_hfont = CreateFontIndirect(&g_lf);

	//Lấy kích thước khung cửa sổ hiện tại
	RECT main;
	GetWindowRect(hwnd, &main);

	//Khởi tạo TreeView
	g_HwndTreeView = CreateWindowEx(NULL, WC_TREEVIEW, _T("Tree View"),
		WS_CHILD | WS_VISIBLE | WS_BORDER | WS_VSCROLL | WS_HSCROLL | PGS_AUTOSCROLL | TVS_HASBUTTONS | TVS_LINESATROOT | WS_TABSTOP,
		0, 0, main.right / 3, main.bottom, hwnd,
		(HMENU)IDC_TREEVIEW, hInst, NULL);
	SendMessage(g_HwndTreeView, WM_SETFONT, (WPARAM)g_hfont, NULL);

	//khởi tạo ListView
	g_HwndListView = CreateWindowEx(NULL, WC_LISTVIEW, _T("List View"),
		WS_CHILD | WS_VISIBLE | WS_VSCROLL | WS_HSCROLL | WS_TABSTOP | LVS_ICON | LVS_EDITLABELS | LVS_SHOWSELALWAYS | LVS_REPORT, main.right / 3, 0, main.right - main.right / 3, main.bottom, hwnd, (HMENU)IDC_LISTVIEW, hInst, NULL);
	SendMessage(g_HwndListView, WM_SETFONT, (WPARAM)g_hfont, NULL);

	//Khởi tạo nút back
	g_HwndButton = CreateWindowEx(NULL, _T("button"), _T("Go Back"), BS_FLAT| WS_CHILD | LVS_ICON | WS_VISIBLE | BS_PUSHBUTTON, (main.right - main.left) / 3 - 50, 0, 50, 20, hwnd, (HMENU)IDC_BACK, hInst, NULL);
	SendMessage(g_HwndButton, WM_SETFONT, (WPARAM)g_hfont, TRUE);

	//Khởi tạo ô edittext address
	g_EditAddress = CreateWindowEx(NULL, L"edit", L"This PC", WS_CHILD | WS_VISIBLE, 0, 0, 0, 0, hwnd, NULL, hInst, NULL);
	SendMessage(g_EditAddress, WM_SETFONT, (WPARAM)g_hfont, TRUE);

	LoadTreeView(g_HwndTreeView);
	LoadListView(g_HwndListView);

	//*********************************
	TaoReSourchIcon();

	return TRUE;
}

void OnCommand(HWND hwnd, int id, HWND hwndCtl, UINT codeNotify)
{
	// Parse the menu selections:
	switch (id)
	{
	case IDC_BACK:
		ListView_DeleteAllItems(g_HwndListView);
		LoadListView(g_HwndListView, GoBack(g_Address));
		SendMessage(g_EditAddress, WM_SETTEXT, 0, (LPARAM)g_Address);
		SendMessage(g_HwndTreeView, TVM_SELECTITEM, 0, (LPARAM)g_Address);
		break;
	case IDM_ABOUT:
		DialogBox(hInst, MAKEINTRESOURCE(IDD_ABOUTBOX), hwnd, About);
		break;
	case IDM_EXIT:
		DestroyWindow(hwnd);
		break;
	default:
		break;
	}
}

void OnPaint(HWND hwnd)
{
	PAINTSTRUCT ps;
	HDC hdc = BeginPaint(hwnd, &ps);
	// TODO: Add any drawing code that uses hdc here...
	EndPaint(hwnd, &ps);
}

void OnSize(HWND hwnd, UINT state, int cx, int cy)
{
	//Lấy các kích thước của cửa sổ hiện hành
	RECT main;
	GetWindowRect(hwnd, &main);

	MoveWindow(g_HwndTreeView, 0, 20, (main.right - main.left) / 3, (main.bottom - main.top) - 100, SWP_SHOWWINDOW);
	MoveWindow(g_HwndListView, (main.right - main.left) / 3, 20, main.right - main.left - (main.right - main.left) / 3, (main.bottom - main.top) - 100, SWP_SHOWWINDOW);
	MoveWindow(g_HwndButton, (main.right - main.left) / 3 - 70, 0, 70, 20, SWP_SHOWWINDOW);
	MoveWindow(g_EditAddress, (main.right - main.left) / 3, 0, main.right - main.left - (main.right - main.left) / 3, 20, SWP_SHOWWINDOW);
}


//Load các icon lên danh sách image
void TaoReSourchIcon()
{
	g_hml32 = ImageList_Create(32, 32, ILC_MASK | ILC_COLOR32, 1, 1);
	g_hml16 = ImageList_Create(16, 16, ILC_MASK | ILC_COLOR16, 1, 1);

	g_s = LoadIcon(hInst, MAKEINTRESOURCE(IDI_ICON1));//PC
	ImageList_AddIcon(g_hml32, g_s);
	ImageList_AddIcon(g_hml16, g_s);

	g_s = LoadIcon(hInst, MAKEINTRESOURCE(IDI_ICON2));//exe
	ImageList_AddIcon(g_hml32, g_s);
	ImageList_AddIcon(g_hml16, g_s);


	g_s = LoadIcon(hInst, MAKEINTRESOURCE(IDI_ICON3));//file
	ImageList_AddIcon(g_hml32, g_s);
	ImageList_AddIcon(g_hml16, g_s);

	g_s = LoadIcon(hInst, MAKEINTRESOURCE(IDI_ICON4));//floppy
	ImageList_AddIcon(g_hml32, g_s);
	ImageList_AddIcon(g_hml16, g_s);

	g_s = LoadIcon(hInst, MAKEINTRESOURCE(IDI_ICON5));//folder
	ImageList_AddIcon(g_hml32, g_s);
	ImageList_AddIcon(g_hml16, g_s);

	g_s = LoadIcon(hInst, MAKEINTRESOURCE(IDI_ICON6));//hdd
	ImageList_AddIcon(g_hml32, g_s);
	ImageList_AddIcon(g_hml16, g_s);

	g_s = LoadIcon(hInst, MAKEINTRESOURCE(IDI_ICON7));//usb
	ImageList_AddIcon(g_hml32, g_s);
	ImageList_AddIcon(g_hml16, g_s);

	g_s = LoadIcon(hInst, MAKEINTRESOURCE(IDI_ICON8));//cd
	ImageList_AddIcon(g_hml32, g_s);
	ImageList_AddIcon(g_hml16, g_s);

	SendMessage(g_HwndTreeView, TVM_SETIMAGELIST, 0, (LPARAM)g_hml16);
	ListView_SetImageList(g_HwndListView, g_hml32, 1);

	DeleteObject(g_s);
}

//tiểu trình dùng để nạp con cho treeview
DWORD WINAPI TreeViewExpand(LPVOID lpParameter)
{
	PreloadExpanding(g_HwndTreeView, g_HtreeViewChild);
	return 0;
}

//tiểu trình dùng để nạp con cho Listview
DWORD WINAPI ListViewLoad(LPVOID lpParameter)
{
	LoadListView(g_HwndListView, m_path_ListView);

	//copy địa chỉ mới cho thanh địa chỉ
	wcscpy_s(g_Address, 2048, m_path_ListView);
	return 0;
}