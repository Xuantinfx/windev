﻿// stdafx.h : include file for standard system include files,
// or project specific include files that are used frequently, but
// are changed infrequently
//

#pragma once

#include "targetver.h"

#define WIN32_LEAN_AND_MEAN             // Exclude rarely-used stuff from Windows headers
// Windows Header Files:
#include <windows.h>

// C RunTime Header Files
#include <stdlib.h>
#include <malloc.h>
#include <memory.h>
#include <tchar.h>
#include <commctrl.h>
#include <windowsx.h>
#include <Dbt.h>
#include <shellapi.h>
#include <windowsx.h>
#include <shlwapi.h>//Dùng để sử dụng hàm StrCpy, StrNCat
#pragma comment(lib, "shlwapi.lib")
#pragma comment(lib, "comctl32.lib")

#include "resource.h"
#include "1512574MyExplore.h"


//Vị trí trong image list sau khi add
#define IDI_FOLDER 4
#define IDI_EXE 1
#define IDI_FILE 2
#define IDI_MYCOMPUTER 0

#define IDI_FLOPPY 3
#define IDI_USB 6
#define IDI_HDD 5
#define IDI_CD  8

void ColForFolder(HWND hWndListView);
void ColForDrive(HWND hWndListView);
void LoadTreeView(HWND hWndTreeView);
void LoadListView(HWND hWndListView);
void LoadListView(HWND hWndListView, LPCWSTR path);
LPWSTR GetDateModified(const FILETIME &ftLastWrite);
LPWSTR GetTypeFile(LPWSTR name);
LPCWSTR GoBack(LPCWSTR path);
LPWSTR ConvertSize(__int64 nSize);
void PreloadExpanding(HWND hWndTV, HTREEITEM hCurSel);
LPCWSTR GetPath(HWND hWndTV, HTREEITEM hItem);
LPCWSTR GetPath(HWND hWndListView, int iItem);
void Load(HWND hWndTreeView, HTREEITEM hParent);
void PreLoad(HWND hWndTV, HTREEITEM hItem);
int ClickItem(HWND hWndListView, LPCWSTR path);