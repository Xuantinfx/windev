# 1512574 - Đào Xuân Tin - xuantinfx@gmail.com

## 1. Tạo ra TreeView bên trái, ListView bên phải.

## 2. Xét TreeView
- [x] 1. Tạo node **root** là **This PC**
- [x] 2. Lấy danh sách các ổ đĩa trong máy bằng hàm **GetLogicalDrives** hoặc **GetLogicalDriveStrings**, thêm các ổ đĩa vào node **This PC**. Gán giá trị của ổ đĩa ví dụ **C:\\** vào **PARAM** (tag) để lấy lên xài lại.
- [x] 3. Bắt sự kiện **Expanding**, lấy ra đường dẫn dấu ở **PARAM** để biết mình phải xư lí thư mục nào, duyệt nội dung thư mục bằng **FindFirstFile** & **FindNextFile**, chỉ lấy các thư mục để thêm vào làm node con.
- [x] 4. Xử lí cho các sự kiện có thể tốn nhiều thời gian làm cho chương trình bị khựng, nên đã đưa các hàm như **Load** vào các **tiểu trình**. 
- [x] 5. Click vào một **item** ở **TreeView** thì sẽ load các con của **item** được click vào **ListView**.
- [x] 6. Nạp **Icon** cố định cho một vài loại **Thư mục** và **File**.  

## 3. Xét ListView
- [x] 1. Hiển thị toàn bộ thư mục và tập tin tương ứng với một đường dẫn
- [x] 2. Bấm đôi vào một thư mục sẽ thấy toàn bộ thư mục con và tập tin.
- [x] 3. Tạo ra **ListView** có 4 cột: **Tên**, **Loại**, **Thời gian chỉnh sửa** và **Dung lượng**. Với thư mục chỉ cần set text cho 2 cột **Tên** và **Loại**. Với tập tin cần hiển thị tối thiểu là 3 cột: **Tên**, **Thời gian chỉnh sửa**, **Dung lượng**.
- [x] 4. Load **Icon** cho **Thư mục** và một vài **File**.
- [x] 5. Double click vào một **File** thì sẽ gọi lệnh **Shell** để mở **File**.

# Các luồng sự kiện chính
- Tạo nút gốc của **TreeView** là **This PC** và con của **This PC** là các ổ đĩa, đồng thời nạp các ổ đĩa vào **ListView**
- Khi người dùng nhấn + (**Expand**) thì sẽ tiến hành gọi hàm **Load** để load con của nó lên và show ra.
- Khi người dùng **click** chuột vào một **Item** thì đồng thời sẽ load các con của nó lên **ListView** và thay đổi địa chỉ của ô **Edittext Address**.

# Các luồng sự kiện phụ 
-

# Link đến repo:
[https://Xuantinfx@bitbucket.org/Xuantinfx/windev.git]()

# Video demo hướng dẫn
[https://www.youtube.com/watch?v=sIVh3BsB3eQ]()