﻿// HOOK.cpp : Defines the exported functions for the DLL application.
//

#include "stdafx.h"
#define EXPORT __declspec(dllexport)

HHOOK hHook = NULL;
HWND hWnd = NULL;
HWND hWndSerch = NULL;
HINSTANCE	hInstLib;

LRESULT CALLBACK HookProc(int nCode, WPARAM wParam, LPARAM lParam) {
	PKBDLLHOOKSTRUCT  p = (PKBDLLHOOKSTRUCT)lParam;
	if (nCode == HC_ACTION)
	{
		switch (wParam)
		{
		case WM_KEYDOWN:
		{
			if (p->vkCode == L'N' || p->vkCode == L'n')
			{
				int Win = GetAsyncKeyState(VK_LWIN);

				if ((Win) < 0) { //nhấn tổ hợp WIN + N
					ShowWindow(hWnd,SW_SHOWNORMAL);
					SetForegroundWindow(hWnd);
					BringWindowToTop(hWnd);
					SetFocus(hWndSerch);
					return -1;
				}
			}
			break;
		}
		default:
			break;
		}
	}
	return CallNextHookEx(0, nCode, wParam, lParam);
}

EXPORT BOOL InstallHook(HWND hwnd,HWND hwndSerch) {
	hWnd = hwnd;
	hWndSerch = hwndSerch;
	hHook = SetWindowsHookEx(WH_KEYBOARD_LL, HookProc,hInstLib,0);
	if (hHook != NULL)
	{
		return 1;
	}
	return 0;
}

EXPORT BOOL UnInstallHook() {
	if (hHook != NULL) {
		UnhookWindowsHookEx(hHook);
		return 1;
	}
	return 0;
}