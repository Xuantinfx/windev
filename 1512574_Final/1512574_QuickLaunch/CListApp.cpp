﻿#include "stdafx.h"
#include "CListApp.h"


void CListApp::Insert(CApp *capp)
{
	//Thêm phần tử mới vào đầu danh sách
	L_App.insert(L_App.begin(), capp);

	//duyệt qua key để chèn vào vị trí thích hợp
	int index = 0;
	for (int i = 1; i < L_App.size(); i++) { //Duyệt qua mảng các App
		int j;
		for (j = 0; j < lstrlenW(capp->nameLow); j++) { //So sánh từng App vs App mới, nếu App mới xếp sai thì swap
			if (capp->nameLow[j] > L_App[i]->nameLow[j]) { //Nếu tên đứng sau L_App[i] thì swap
				CApp *temp = L_App[index];
				L_App[index] = L_App[i];
				L_App[i] = temp;
				index = i; // Gán capp hiện tại là i
				break;
			}
			if (capp->nameLow[j] == L_App[i]->nameLow[j])
				continue;
			else
			{
				break;
			}
		}
		if (j == lstrlenW(capp->nameLow)) { //đã đúng vị trí
			break;
		}
	}
	//đếm nsame nếu chèn vào không phải vị trí đầu tiên
	if (index != 0) {
		for (int i = 0; i < lstrlenW(L_App[index]->nameLow) && i < lstrlenW(L_App[index - 1]->nameLow); i++) {
			if (L_App[index - 1]->nameLow[i] == L_App[index]->nameLow[i])
				L_App[index]->nSame++;
			else
				break;
		}
	}//đến nsame nếu chèn vào vị trí đầu tiên và có 2 phần tử trở lên
	else {
		if (L_App.size() >= 2) {
			//cập nhật lại thằng thứ 2 đề phòng cái mới đc chèn đầu
			for (int i = 0; i < lstrlenW(L_App[index]->nameLow) && i < lstrlenW(L_App[index + 1]->nameLow); i++) {
				if (L_App[index + 1]->nameLow[i] == L_App[index]->nameLow[i])
					L_App[index+1]->nSame++;
				else
					break;
			}
		}
	}
}

bool CListApp::Search(vector<CApp *> *result, WCHAR *_key,BOOL _showAll)
{
	if (lstrlenW(_key) == 0 && _showAll == 0)
	{
		// Xóa hết phần tử đã tồn tại
		result->erase(result->begin(), result->end());
		return 0;
	}
	//Lower _key
	WCHAR key[50];
	for (int i = 0; i <= lstrlenW(_key); i++) {
		key[i] = _key[i];
		if (key[i] >= L'A' && key[i] <= L'Z') {
			key[i] = key[i] + (L'a' - L'A');
		}
	}
	// Xóa hết phần tử đã tồn tại
	result->erase(result->begin(), result->end());

	//duyệt list
	for (int i = 0; i < L_App.size(); i++) {
		int j;
		for (j = 0; j < lstrlenW(key); j++) { //Duyệt qua _key xem có khớp hết không
			if (key[j] != L_App[i]->nameLow[j])
				break;
		}
		if (j == lstrlenW(key)) //Khớp tất cả
		{
			//bỏ phần tử thứ i vào kết quả
			result->push_back(L_App[i++]);
			//bỏ các phần tử theo sau có số lượng giống với kí tự được thêm lớn hơn size của _key
			if(i < L_App.size())
				while (L_App[i]->nSame >= lstrlenW(key))
				{
					result->push_back(L_App[i++]);
					if (i >= L_App.size() || result->size()>30)
						break;
				}

			//Sắp xếp theo độ ưu tiên mở App
			for (int k = 0; k < result->size() - 1; k++) {
				for (int l = k+1; l < result->size(); l++) {
					if ((*result)[k]->openCounter < (*result)[l]->openCounter) {
						CApp *temp = (*result)[k];
						(*result)[k] = (*result)[l];
						(*result)[l] = temp;
					}
				}
			}
			//trả về kích thước của kết quả
			return result->size();
		}
	}
	return result->size();
}

CListApp::CListApp()
{
}


CListApp::~CListApp()
{
}
