#pragma once
#include "stdafx.h"
#include <wchar.h>
class CApp
{
public:
	int nSame;
	int openCounter;
	WCHAR nameLow[50];
	WCHAR name[50];
	WCHAR des[512];
	CApp() {
		nSame = 0;
		openCounter = 0;
		StrCpyW(nameLow, L"");
		StrCpyW(name, L"");
		StrCpyW(des, L"");
	}
	CApp(WCHAR *_name,WCHAR* _des) {
		nSame = 0;
		openCounter = 0;
		//wsprintf(name,L"%s", _name);
		StrCpyW(name, _name);
		//wsprintf(des, L"%s",_des);
		StrCpyW(des,_des);
		//lower _name
		WCHAR temp[50];
		for (int i = 0; i < lstrlenW(_name); i++) {
			temp[i] = _name[i];
			if (temp[i] >= L'A' && temp[i] <= L'Z') {
				temp[i] = temp[i] + (L'a' - L'A');
			}
		}
		temp[lstrlenW(_name)] = 0;
		wsprintf(nameLow,L"%s", temp);
	}
	~CApp(){}
};

class CListApp
{
public:
	vector<CApp *> L_App;
	void Insert(CApp *capp);
	bool Search(vector<CApp *> *result,WCHAR *_key,BOOL _showAll);
	CListApp();
	~CListApp();
};

