﻿#include "stdafx.h"
#include "CShowResult.h"


CShowResult::CShowResult()
{
	index = 0;
	top = 0;
	bottom = 9;
}


CShowResult::~CShowResult()
{
}

void CShowResult::Update(HWND _hwnd,HINSTANCE _hInst,BOOL _showAll)
{
	//Mở rộng cửa sổ chính theo số lượng các item
	POINT client;
	client.x = 0; client.y = 0;
	ClientToScreen(_hwnd,&client);
	int height;
	if (LApp.size() == 0) 
	{
		if(_showAll != 0)
			height = 30 + HEIGHT;
		else
			height = HEIGHT;
	}
	else
	if (LApp.size() > 10) {
		height = 10 * 30 + HEIGHT;
	}
	else
	{
		height = LApp.size() * 30 + HEIGHT;
	}
	SetWindowPos(_hwnd, NULL, client.x, client.y, WIDTH, height, SWP_NOMOVE | SWP_NOOWNERZORDER | SWP_NOZORDER);
	//xóa hết hwnd và các cửa sổ đã tạo trước đó
	for (int i = 0; i < hwnd.size(); i++) {
		DestroyWindow(hwnd[i]);
	}
	hwnd.erase(hwnd.begin(), hwnd.end());

	HWND hwndtemp;
	HFONT hFont = CreateFont(20, 0, 0, 0, 10, FALSE, FALSE, FALSE, ANSI_CHARSET,
		OUT_TT_PRECIS, CLIP_DEFAULT_PRECIS, DEFAULT_QUALITY,
		DEFAULT_PITCH | FF_DONTCARE, L"Arial");
	if (LApp.size() == 0) {
		index = -1;
		hwndtemp = CreateWindow(L"STATIC", L"< Không tìm thấy kết quả >", WS_VISIBLE | WS_CHILD, WIDTH / 2 - 100, HEIGHT, 200, 30, _hwnd, NULL, _hInst, NULL);
		SendMessage(hwndtemp, WM_SETFONT, (WPARAM)hFont, NULL);
		hwnd.push_back(hwndtemp);
	}

	if (_showAll == 0)
	{
		InvalidateRect(_hwnd, NULL, 1);
		return;
	}

	//Vẽ các item
	for (int i = top; i < LApp.size()&&i<=bottom; i++) {
		hwndtemp = CreateWindow(L"STATIC", LApp[i]->name, WS_VISIBLE | WS_CHILD, 10, (i-top) * 30 + HEIGHT, WIDTH - 20, 30, _hwnd, NULL, _hInst, NULL);
		SendMessage(hwndtemp, WM_SETFONT, (WPARAM)hFont, NULL);
		hwnd.push_back(hwndtemp);
	}
	InvalidateRect(_hwnd, NULL, 1);
}

void CShowResult::Down(HWND hwnd, HINSTANCE _hInst, BOOL _showAll)
{
	if (index < (int)(LApp.size() - 1))
	{
		index++;
		//Update(hwnd,_hInst,_showAll);
		if (index > bottom) {
			top++;
			bottom++;
			Update(hwnd, _hInst, _showAll);
		}
		InvalidateRect(hwnd, NULL, 1);
	}
}

void CShowResult::Up(HWND hwnd,HINSTANCE _hInst, BOOL _showAll)
{
	if (index > 0)
	{
		index--;
		//Update(hwnd,_hInst,_showAll);
		if (index < top) {
			top--;
			bottom--;
			Update(hwnd, _hInst, _showAll);
		}
		InvalidateRect(hwnd, NULL, 1);
	}
}
