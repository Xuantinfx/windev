﻿// 1512574QuickLaunch.cpp : Defines the entry point for the application.
//

#include "stdafx.h"
#include "1512574QuickLaunch.h"

#define TRAYICONID	1//				ID number for the Notify Icon
#define SWM_TRAYMSG	WM_APP//		the message ID sent to our window

#define SWM_SCAN	WM_APP + 1//	show the window
#define SWM_VIEW	WM_APP + 2//	hide the window
#define SWM_EXIT	WM_APP + 3//	close the window

#define MAX_LOADSTRING 100

// Global Variables:
HINSTANCE hInst;                                // current instance
WCHAR szTitle[MAX_LOADSTRING];                  // The title bar text
WCHAR szWindowClass[MAX_LOADSTRING];            // the main window class name

/********************* Dev variable ************************/
BOOL show = TRUE;
HWND g_hWnd = NULL;
HWND g_hWndChar = NULL;
HWND g_hWndInsert = NULL;
HWND g_hWndProcess = NULL;
WCHAR g_charProcess[1000];
CListApp g_CLAPP;
CShowResult g_CShResult;
NOTIFYICONDATA	niData;	// notify icon data
HWND g_search = NULL;
BOOL g_showAll = 0;
HWND g_lvInsert = NULL;
//Tạo bảng màu định sẵn
int g_ColorChart[10][3] = { { 66, 134, 244 },
							{ 65, 244, 103 },
							{ 229, 244, 65 },
							{ 244, 175, 65 },
							{ 244, 67, 65 },
							{ 211, 65, 244 },
							{ 65, 244, 205 },
							{ 242, 65, 183 },
							{ 53, 242, 239 },
							{ 241, 144, 53 } };
/***********************************************************/
// Forward declarations of functions included in this code module:
ATOM                MyRegisterClass(HINSTANCE hInstance);
BOOL                InitInstance(HINSTANCE, int);
LRESULT CALLBACK    WndProc(HWND, UINT, WPARAM, LPARAM);
INT_PTR CALLBACK    InsertEXE(HWND, UINT, WPARAM, LPARAM);
INT_PTR CALLBACK    Process(HWND, UINT, WPARAM, LPARAM);
INT_PTR CALLBACK	DlgProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam);

/************************ Dev Function **********************/
bool installHook(); 
bool unInstallHook();
BOOL InitInstanceDlg(HINSTANCE hInstance, int nCmdShow);
ULONGLONG GetDllVersion(LPCTSTR lpszDllName);
BOOL OnInitDialog(HWND hWnd);
void ShowContextMenu(HWND hWnd);
BOOL isEXE(LPWSTR _name);
BOOL BuildData(WCHAR *path);
VOID getNameEXE(WCHAR* _path, WCHAR *name);
BOOL saveData(WCHAR* _fileName);
BOOL readData(WCHAR* _fileName);
VOID cleanData();
VOID searchTopUseApp(vector<CApp *> &result, int size);
WCHAR* BrowseFolder();
VOID showListDir(WCHAR *fileName, HWND &hwndLv);
/************************************************************/

int APIENTRY wWinMain(_In_ HINSTANCE hInstance,
                     _In_opt_ HINSTANCE hPrevInstance,
                     _In_ LPWSTR    lpCmdLine,
                     _In_ int       nCmdShow)
{
    UNREFERENCED_PARAMETER(hPrevInstance);
    UNREFERENCED_PARAMETER(lpCmdLine);

    // TODO: Place code here.
	GdiplusStartupInput gdiplusStartupInput;
	ULONG_PTR           gdiplusToken;
	GdiplusStartup(&gdiplusToken, &gdiplusStartupInput, NULL);

    // Initialize global strings
    LoadStringW(hInstance, IDS_APP_TITLE, szTitle, MAX_LOADSTRING);
    LoadStringW(hInstance, IDC_1512574QUICKLAUNCH, szWindowClass, MAX_LOADSTRING);
    MyRegisterClass(hInstance);

    // Perform application initialization:
    if (!InitInstance (hInstance, nCmdShow))
    {
        return FALSE;
    }

    HACCEL hAccelTable = LoadAccelerators(hInstance, MAKEINTRESOURCE(IDC_1512574QUICKLAUNCH));

    MSG msg;

    // Main message loop:
    while (GetMessage(&msg, nullptr, 0, 0))
    {
		if (msg.message == WM_KEYDOWN && msg.wParam == 13) SendMessage(g_hWnd, WM_COMMAND, IDC_RUN, NULL);
		if (msg.message == WM_KEYDOWN &&  (msg.wParam == VK_DOWN || msg.wParam == VK_UP) ) { SendMessage(g_hWnd, WM_KEYDOWN, msg.wParam, msg.lParam); continue; };
        if (!TranslateAccelerator(msg.hwnd, hAccelTable, &msg))
        {
            TranslateMessage(&msg);
            DispatchMessage(&msg);
        }
    }

    return (int) msg.wParam;
}



//
//  FUNCTION: MyRegisterClass()
//
//  PURPOSE: Registers the window class.
//
ATOM MyRegisterClass(HINSTANCE hInstance)
{
    WNDCLASSEXW wcex;

    wcex.cbSize = sizeof(WNDCLASSEX);

    wcex.style          = CS_HREDRAW | CS_VREDRAW;
    wcex.lpfnWndProc    = WndProc;
    wcex.cbClsExtra     = 0;
    wcex.cbWndExtra     = 0;
    wcex.hInstance      = hInstance;
    wcex.hIcon          = LoadIcon(hInstance, MAKEINTRESOURCE(IDI_1512574QUICKLAUNCH));
    wcex.hCursor        = LoadCursor(nullptr, IDC_ARROW);
    wcex.hbrBackground  = (HBRUSH)CreateSolidBrush(RGB(255,255,255));
    //wcex.lpszMenuName   = MAKEINTRESOURCEW(IDC_1512574QUICKLAUNCH);
	wcex.lpszMenuName = 0;
    wcex.lpszClassName  = szWindowClass;
    wcex.hIconSm        = LoadIcon(wcex.hInstance, MAKEINTRESOURCE(IDI_SMALL));

    return RegisterClassExW(&wcex);
}

//
//   FUNCTION: InitInstance(HINSTANCE, int)
//
//   PURPOSE: Saves instance handle and creates main window
//
//   COMMENTS:
//
//        In this function, we save the instance handle in a global variable and
//        create and display the main program window.
//
BOOL InitInstance(HINSTANCE hInstance, int nCmdShow)
{
   hInst = hInstance; // Store instance handle in our global variable

   RECT desktop;
   GetWindowRect(GetDesktopWindow(), &desktop);

   HWND hWnd = CreateWindowEx(WS_EX_CLIENTEDGE,szWindowClass, szTitle,WS_POPUPWINDOW,
      desktop.right/2-WIDTH/2, desktop.bottom/2-HEIGHT/2 - 100,WIDTH, HEIGHT, nullptr, nullptr, hInstance, nullptr);
   if (!hWnd)
   {
      return FALSE;
   }

   ShowWindow(hWnd, 1);
   
   UpdateWindow(hWnd);

   return TRUE;
}

//
//  FUNCTION: WndProc(HWND, UINT, WPARAM, LPARAM)
//
//  PURPOSE:  Processes messages for the main window.
//
//  WM_COMMAND  - process the application menu
//  WM_PAINT    - Paint the main window
//  WM_DESTROY  - post a quit message and return
//
//
LRESULT CALLBACK WndProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam)
{
    switch (message)
    {
	case WM_CREATE:
	{
		HFONT hFont = CreateFont(26, 0, 0, 0, 10, FALSE, FALSE, FALSE, ANSI_CHARSET,
			OUT_TT_PRECIS, CLIP_DEFAULT_PRECIS, DEFAULT_QUALITY,
			DEFAULT_PITCH | FF_DONTCARE, L"Arial");

		g_hWnd = hWnd;
		// Tạo 4 cửa sổ rồi để đó
		InitInstanceDlg(hInst, 0);
		g_hWndInsert = CreateDialog(hInst, MAKEINTRESOURCE(IDD_INSERTBOX), NULL, InsertEXE);
		g_hWndProcess = CreateDialog(hInst, MAKEINTRESOURCE(IDD_PROCESS), NULL, Process);
		ShowWindow(g_hWndInsert, 0);
		ShowWindow(g_hWndProcess, 0);

		//vẽ edit box full màn hình
		RECT client;
		GetClientRect(hWnd, &client);
		g_search = CreateWindow(L"EDIT", L"", WS_CHILD | WS_VISIBLE, 10, 10, client.right - 20, client.bottom - 20, hWnd, (HMENU)IDC_SEARCH, hInst, NULL);
		SendMessage(g_search, WM_SETFONT, (WPARAM)hFont, NULL);
		SetFocus(g_search);
		installHook();
		readData(L"data.txt");

	}
	break;
	case WM_KEYDOWN:
	{
		if ((GetAsyncKeyState(VK_DOWN) < 0)) {
			g_CShResult.Down(hWnd,hInst,g_showAll);
			return -1;
		}
		if ((GetAsyncKeyState(VK_UP) < 0)) {
			g_CShResult.Up(hWnd, hInst, g_showAll);
			return -1;
		}
	}
		break;
	case WM_ACTIVATE:
	{
		SetWindowText(g_search, L"");
		if (wParam == WA_INACTIVE)
		{
			ShowWindow(hWnd, 0);
			g_showAll = 0;
			g_CShResult.index = 0;
			g_CShResult.top = 0;
			g_CShResult.bottom = 9;
		}
		break;
	}
	case WM_CTLCOLOREDIT:
	{
		HDC hdc = (HDC)wParam;
		SetTextColor(hdc, RGB(145, 141, 130));  // yourColor is a WORD and it's format is 0x00BBGGRR
		return (LRESULT)GetStockObject(DC_BRUSH); // return a DC brush.
	}
	case WM_CTLCOLORSTATIC:
	{
		HDC hdc = (HDC)wParam;
		HWND temp = WindowFromDC(hdc);
		if (g_CShResult.index != -1)
		{
			if (temp != g_CShResult.hwnd[g_CShResult.index - g_CShResult.top])
				SetTextColor(hdc, RGB(145, 141, 130));  // yourColor is a WORD and it's format is 0x00BBGGRR
			else
			{
				SetTextColor(hdc, RGB(66, 134, 244));
				SetBkColor(hdc, RGB(64,64,64));
				return (LRESULT)GetStockObject(DKGRAY_BRUSH);
			}
		}
		else
		{
			SetTextColor(hdc, RGB(145, 141, 130));  // yourColor is a WORD and it's format is 0x00BBGGRR
		}
		return (LRESULT)GetStockObject(DC_BRUSH); // return a DC brush.
	}
	case WM_CHAR:
	{
		break;
	}
    case WM_COMMAND:
        {
            int wmId = LOWORD(wParam);
            // Parse the menu selections:
            switch (wmId)
            {
			case IDC_RUN:
			{
				if (g_CShResult.index == -1)
					g_CShResult.index = 0;
				g_CShResult.LApp[g_CShResult.index]->openCounter++;
				InvalidateRect(g_hWndChar, 0, 1);
				ShellExecute(NULL, L"Open", g_CShResult.LApp[g_CShResult.index]->des, NULL, NULL, SW_DENORMAL);
				ShowWindow(g_hWnd, 0);
				break;
			}
			case IDC_SEARCH:
			{
				switch (HIWORD(wParam))
				{
				case EN_CHANGE:
				{
					WCHAR content[50];
					GetWindowText(g_search, content, 50);
					g_CLAPP.Search(&g_CShResult.LApp, content, g_showAll);
					g_CShResult.top = 0;
					g_CShResult.bottom = 9;
					g_CShResult.index = 0;
					g_CShResult.Update(hWnd,hInst,g_showAll);
					g_showAll = 1;
								
				}
					break;
				default:
					break;
				}
				break;
			}
            case IDM_ABOUT:
                break;
            case IDM_EXIT:
                DestroyWindow(hWnd);
				break;
            default:
                return DefWindowProc(hWnd, message, wParam, lParam);
            }
        }
        break;
    case WM_PAINT:
        {
            PAINTSTRUCT ps;
            HDC hdc = BeginPaint(hWnd, &ps);
            // TODO: Add any drawing code that uses hdc here...
            EndPaint(hWnd, &ps);
        }
        break;
    case WM_DESTROY:
	{
		unInstallHook();
		PostQuitMessage(0);
		break;
	}
	case WM_CLOSE:
	{
		PostMessage(hWnd, WM_DESTROY, 0, 0);
		break;
	}
    default:
        return DefWindowProc(hWnd, message, wParam, lParam);
    }
    return 0;
}

// Message handler for about box.
INT_PTR CALLBACK InsertEXE(HWND hDlg, UINT message, WPARAM wParam, LPARAM lParam)
{
    UNREFERENCED_PARAMETER(lParam);
    switch (message)
    {
    case WM_INITDIALOG:
		g_hWndInsert = hDlg;
        return (INT_PTR)TRUE;

    case WM_COMMAND:
        if (LOWORD(wParam) == IDC_INSERTMOREEXE)
        {
			//Lấy thư mục từ người dùng
			WCHAR *dir = BrowseFolder();
			// Nếu là chuỗi rỗng thì bỏ qua
			if (StrCmpW(dir, L"") != 0) {
				//Mở file và bổ sung vào đường dẫn mới
				wfstream fin;
				fin.open(L"dictionary.txt", ios_base::app);
				if (!fin.is_open())
					break;
				WCHAR buff[1024];
				wsprintf(buff, L"%s\n", dir);
				fin << buff;
				fin.close();

				ShowWindow(g_hWndProcess, 1);
				BuildData(dir);
				ShowWindow(g_hWndProcess, 0);

				saveData(L"data.txt");
				MessageBeep(MB_OK);

				if (g_lvInsert != NULL)
					DestroyWindow(g_lvInsert);
				showListDir(L"dictionary.txt", g_lvInsert);
				MessageBox(g_hWnd, L"Build database and add more Exe success!", L"Thông báo", MB_OK);
			}
			delete []dir;
        }
		break;
	case WM_CLOSE:
		if(g_hWndInsert !=NULL)
			DestroyWindow(g_lvInsert);
		ShowWindow(hDlg, 0);
        break;
    }
    return (INT_PTR)FALSE;
}

// Message handler for about box.
INT_PTR CALLBACK Process(HWND hDlg, UINT message, WPARAM wParam, LPARAM lParam)
{
	UNREFERENCED_PARAMETER(lParam);
	switch (message)
	{
	case WM_INITDIALOG:
		g_hWndProcess = hDlg;
		return (INT_PTR)TRUE;

	case WM_PAINT:
	{
		SetForegroundWindow(g_hWndProcess);
		BringWindowToTop(g_hWndProcess);

		//Nếu chuỗi là NULL thì break
		if (g_charProcess == NULL)
			break;
		RECT client;
		GetClientRect(hDlg, &client);


		// Bắt đầu vẽ
		PAINTSTRUCT ps;
		HDC hdc = BeginPaint(hDlg, &ps);

		Graphics *graphics = new Graphics(hdc);
		Pen      *pen = new Pen(Color(0, 255, 0, 0));
		SolidBrush *mySolidBrush = new SolidBrush(Color(0, 0, 0));

		// Tạo font
		FontFamily  fontFamily(L"Arial");
		Font        font(&fontFamily, 12, FontStyleItalic, Gdiplus::UnitPixel);
		SolidBrush  solidBrushStr(Color(28, 26, 26));
		PointF pointf(10, 30);
		//Vẽ cái chuỗi
		graphics->DrawString(g_charProcess, -1, &font, pointf, &solidBrushStr);

		delete pen;
		delete mySolidBrush;
		delete graphics;
		break;
	}
	case WM_COMMAND:
		break;
	case WM_CLOSE:
		break;
	}
	return (INT_PTR)FALSE;
}

// Message handler for the app
INT_PTR CALLBACK DlgProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam)
{
	int wmId, wmEvent;

	switch (message)
	{
	case SWM_TRAYMSG:
		switch (lParam)
		{
		case WM_LBUTTONDBLCLK:
			ShowWindow(g_hWnd, SW_RESTORE);
			break;
		case WM_RBUTTONDOWN:
		case WM_CONTEXTMENU:
			ShowContextMenu(hWnd);
		}
		break;
	case WM_SYSCOMMAND:
		if ((wParam & 0xFFF0) == SC_MINIMIZE)
		{
			ShowWindow(hWnd, SW_HIDE);
			return 1;
		}
		break;
	case WM_COMMAND:
		wmId = LOWORD(wParam);
		wmEvent = HIWORD(wParam);

		switch (wmId)
		{
		case IDC_INSERT:
		{
			// Hiển thị danh sách thư mục ra listview
			showListDir(L"dictionary.txt", g_lvInsert);
			//Hiển thị cửa sổ thêm
			ShowWindow(g_hWndInsert, 1);
		}
			break;
		case SWM_SCAN:
		{
			cleanData();
			wfstream fin;
			fin.open(L"dictionary.txt", ios_base::in);
			if (!fin.is_open())
				break;
			WCHAR buff[1024];
			while (!fin.eof())
			{
				ShowWindow(g_hWndProcess, 1);
				fin.getline(buff, 1024, L'\n');
				if (StrCmpW(buff, L"") != 0)
					BuildData(buff);

				ShowWindow(g_hWndProcess, 0);
			}

			fin.close();
			saveData(L"data.txt");
			MessageBeep(MB_OK);
			MessageBox(g_hWnd,L"Scan to build database success!", L"Thông báo", MB_OK);
			ShowWindow(g_hWnd, 1);
		}
			break;
		case SWM_VIEW:
		{
			InvalidateRect(hWnd, 0, 1);
		}
			ShowWindow(hWnd, SW_RESTORE);
			break;
		case IDOK:
			ShowWindow(hWnd, SW_HIDE);
			break;
		case SWM_EXIT:
			saveData(L"data.txt");
			cleanData();
			DestroyWindow(hWnd);
			DestroyWindow(g_hWnd);
			break;
		}
		return 1;
	case WM_PAINT:
	{
		// Lấy danh sách và tần suât
		vector<CApp *> result;
		searchTopUseApp(result, 6);

		// Bắt đầu vẽ
		PAINTSTRUCT ps;
		HDC hdc = BeginPaint(hWnd, &ps);

		Graphics *graphics = new Graphics(hdc);
		Pen      *pen = new Pen(Color(0, 255, 0, 0));
		SolidBrush *mySolidBrush = new SolidBrush(Color(0, 0, 0));

		// Tạo font
		FontFamily  fontFamily(L"Arial");
		Font        font(&fontFamily, 18, FontStyleItalic, UnitPixel);
		SolidBrush  solidBrushStr(Color(28, 26, 26));

		RectF ellipseRect(10, 10, 260, 260);
		REAL startAngle = 0.0f;
		REAL sweepAngle = 0.0f;

		RectF ellipseRectCT(270, 10, 60, 60);
		REAL startAngleCT = 315.0f;
		REAL sweepAngleCT = 45.0f;

		// Tính tổng lần chạy
		int sum = 0;
		for (int i = 0; i < result.size(); i++) {
			sum += result[i]->openCounter;
		}
		// Vẽ từng phần
		PointF pointCT;
		WCHAR buff[1024];
		for (int i = 0; i < result.size(); i++)
		{
			// Vẽ pipe
			startAngle += sweepAngle;
			sweepAngle = ((float)result[i]->openCounter /(float) sum)*360;
			mySolidBrush->SetColor(Color(g_ColorChart[i][0], g_ColorChart[i][1], g_ColorChart[i][2]));
			graphics->FillPie(mySolidBrush, ellipseRect, startAngle, sweepAngle);
			graphics->DrawPie(pen, ellipseRect, startAngle, sweepAngle);

			//Không vẽ nếu ứng dụng chưa được chạy
			if (result[i]->openCounter > 0 || (i == result.size()-1))
			{
				// Vẽ bánh chú thích
				mySolidBrush->SetColor(Color(g_ColorChart[i][0], g_ColorChart[i][1], g_ColorChart[i][2]));
				graphics->FillPie(mySolidBrush, ellipseRectCT, startAngleCT, sweepAngleCT);
				graphics->DrawPie(pen, ellipseRectCT, startAngleCT, sweepAngleCT);

				// Vẽ tên ứng dụng
				if (result[i]->openCounter > 1)
					wsprintf(buff, L"%d times open %s", result[i]->openCounter, result[i]->name);
				else
					wsprintf(buff, L"%d time open %s", result[i]->openCounter, result[i]->name);
				if(i == result.size() - 1)
					wsprintf(buff, L"%d times for %s", result[i]->openCounter, result[i]->name);
				pointCT = PointF(ellipseRectCT.X + 60, ellipseRectCT.Y + 10);
				graphics->DrawString(buff, -1, &font, pointCT, &solidBrushStr);
				ellipseRectCT.Y += 30;
			}
		}
		delete pen;
		delete mySolidBrush;
		delete graphics;
		break;
	}
	case WM_INITDIALOG:
		g_hWndChar = hWnd;
		return OnInitDialog(hWnd);
	case WM_CLOSE:
		ShowWindow(hWnd, 0);
		break;
	case WM_DESTROY:
		niData.uFlags = 0;
		Shell_NotifyIcon(NIM_DELETE, &niData);
		PostQuitMessage(0);
		break;
	}
	return 0;
}

bool installHook() {
	typedef BOOL(*PROC)(HWND,HWND);

	HINSTANCE hInstLib = NULL;
	PROC procAddr = NULL;
	hInstLib = LoadLibrary(L"HOOK.dll");
	if (hInstLib != NULL) {
		procAddr = (PROC)GetProcAddress(hInstLib, "InstallHook");
		if (procAddr != NULL) {
			return procAddr(g_hWnd,g_search);
		}
	}
	return false;
}

bool unInstallHook() {
	typedef BOOL(*PROC)();

	HINSTANCE hInstLib = NULL;
	PROC procAddr = NULL;
	hInstLib = LoadLibrary(L"HOOK.dll");
	if (hInstLib != NULL) {
		procAddr = (PROC)GetProcAddress(hInstLib, "UnInstallHook");
		if (procAddr != NULL) {
			return procAddr();
		}
	}
	return false;
}

//	Initialize the window and tray icon
BOOL InitInstanceDlg(HINSTANCE hInstance, int nCmdShow)
{
	// prepare for XP style controls
	//InitCommonControls();

	// store instance handle and create dialog
	HWND hWnd = CreateDialog(hInstance, MAKEINTRESOURCE(IDD_DLG_DIALOG),
		NULL, (DLGPROC)DlgProc);
	if (!hWnd) return FALSE;

	// Fill the NOTIFYICONDATA structure and call Shell_NotifyIcon

	// zero the structure - note:	Some Windows funtions require this but
	//								I can't be bothered which ones do and
	//								which ones don't.
	ZeroMemory(&niData, sizeof(NOTIFYICONDATA));

	// get Shell32 version number and set the size of the structure
	//		note:	the MSDN documentation about this is a little
	//				dubious and I'm not at all sure if the method
	//				bellow is correct
	ULONGLONG ullVersion = GetDllVersion(_T("Shell32.dll"));
	if (ullVersion >= MAKEDLLVERULL(5, 0, 0, 0))
		niData.cbSize = sizeof(NOTIFYICONDATA);
	else niData.cbSize = NOTIFYICONDATA_V2_SIZE;

	// the ID number can be anything you choose
	niData.uID = TRAYICONID;

	// state which structure members are valid
	niData.uFlags = NIF_ICON | NIF_MESSAGE | NIF_TIP;

	// load the icon
	niData.hIcon = (HICON)LoadImage(hInstance, MAKEINTRESOURCE(IDI_1512574QUICKLAUNCH),
		IMAGE_ICON, GetSystemMetrics(SM_CXSMICON), GetSystemMetrics(SM_CYSMICON),
		LR_DEFAULTCOLOR);

	// the window to send messages to and the message to send
	//		note:	the message value should be in the
	//				range of WM_APP through 0xBFFF
	niData.hWnd = hWnd;
	niData.uCallbackMessage = SWM_TRAYMSG;

	// tooltip message
	lstrcpyn(niData.szTip, _T("Quick Launch (Win + N)!"), sizeof(niData.szTip) / sizeof(TCHAR));

	Shell_NotifyIcon(NIM_ADD, &niData);

	// free icon handle
	if (niData.hIcon && DestroyIcon(niData.hIcon))
		niData.hIcon = NULL;

	// call ShowWindow here to make the dialog initially visible

	return TRUE;
}

// Get dll version number
ULONGLONG GetDllVersion(LPCTSTR lpszDllName)
{
	ULONGLONG ullVersion = 0;
	HINSTANCE hinstDll;
	hinstDll = LoadLibrary(lpszDllName);
	if (hinstDll)
	{
		DLLGETVERSIONPROC pDllGetVersion;
		pDllGetVersion = (DLLGETVERSIONPROC)GetProcAddress(hinstDll, "DllGetVersion");
		if (pDllGetVersion)
		{
			DLLVERSIONINFO dvi;
			HRESULT hr;
			ZeroMemory(&dvi, sizeof(dvi));
			dvi.cbSize = sizeof(dvi);
			hr = (*pDllGetVersion)(&dvi);
			if (SUCCEEDED(hr))
				ullVersion = MAKEDLLVERULL(dvi.dwMajorVersion, dvi.dwMinorVersion, 0, 0);
		}
		FreeLibrary(hinstDll);
	}
	return ullVersion;
}

BOOL OnInitDialog(HWND hWnd)
{
	HMENU hMenu = GetSystemMenu(hWnd, FALSE);
	if (hMenu)
	{
		AppendMenu(hMenu, MF_SEPARATOR, 0, NULL);
		AppendMenu(hMenu, MF_STRING, IDM_ABOUT, _T("About"));
	}
	HICON hIcon = (HICON)LoadImage(hInst,
		MAKEINTRESOURCE(IDI_1512574QUICKLAUNCH),
		IMAGE_ICON, 0, 0, LR_SHARED | LR_DEFAULTSIZE);
	SendMessage(hWnd, WM_SETICON, ICON_BIG, (LPARAM)hIcon);
	SendMessage(hWnd, WM_SETICON, ICON_SMALL, (LPARAM)hIcon);
	return TRUE;
}

// Name says it all
void ShowContextMenu(HWND hWnd)
{
	POINT pt;
	GetCursorPos(&pt);
	HMENU hMenu = CreatePopupMenu();
	if (hMenu)
	{
		/*if (IsWindowVisible(g_hWnd))
			InsertMenu(hMenu, -1, MF_BYPOSITION, SWM_HIDE, _T("Hide"));
		else
			InsertMenu(hMenu, -1, MF_BYPOSITION, SWM_SHOW, _T("Show"));
		InsertMenu(hMenu, -1, MF_BYPOSITION, SWM_EXIT, _T("Exit"));*/
		InsertMenu(hMenu, -1, MF_BYPOSITION, SWM_SCAN, _T("Scan to build database"));
		InsertMenu(hMenu, -1, MF_BYPOSITION, SWM_VIEW, _T("View statitistics"));
		InsertMenu(hMenu, -1, MF_BYPOSITION, SWM_EXIT, _T("Exit"));
		

		// note:	must set window to the foreground or the
		//			menu won't disappear when it should
		SetForegroundWindow(hWnd);

		TrackPopupMenu(hMenu, TPM_BOTTOMALIGN,
			pt.x, pt.y, 0, hWnd, NULL);
		DestroyMenu(hMenu);
	}
}

BOOL BuildData(WCHAR *path) {
	if (StrCmpW(L"C:\\Program Files (x86)\\Microsoft Visual Studio", path) == 0)
		return true;
	CApp * capp;
	WCHAR buffer[1024];
	wcscpy_s(buffer, path);

	if (wcslen(path) == 3)
		wcscat_s(buffer, _T("*"));
	else
		wcscat_s(buffer, _T("\\*"));

	WIN32_FIND_DATA fd;
	HANDLE hFile;
	BOOL bFound = true;
	TCHAR * folderPath;

	//Chạy lần thứ nhất lấy các thư mục
	hFile = FindFirstFileW(buffer, &fd);
	bFound = TRUE;

	if (hFile == INVALID_HANDLE_VALUE)
		bFound = FALSE;

	while (bFound)
	{
		if ((fd.dwFileAttributes & FILE_ATTRIBUTE_DIRECTORY) &&
			((fd.dwFileAttributes & FILE_ATTRIBUTE_HIDDEN) != FILE_ATTRIBUTE_HIDDEN) &&
			(wcscmp(fd.cFileName, L".") != 0) && (wcscmp(fd.cFileName, L"..") != 0))
		{
			folderPath = new WCHAR[wcslen(path) + wcslen(fd.cFileName) + 2];
			wcscpy_s(folderPath, wcslen(path) + wcslen(fd.cFileName) + 2, path);

			if (wcslen(path) != 3)
				wcscat_s(folderPath, wcslen(path) + wcslen(fd.cFileName) + 2, L"\\");

			wcscat_s(folderPath, wcslen(path) + wcslen(fd.cFileName) + 2, fd.cFileName);
			
			//duyệt vào trong
			BuildData(folderPath);

			delete []folderPath;
		}
		bFound = FindNextFileW(hFile, &fd);
	}
							  //Chạy lần thứ hai lấy các tập tin, thầy Quang bảo đây là cách chuỗi nhất từng thấy, 
							  //nhưng mà k còn cách khác
	WCHAR * filePath;

	hFile = FindFirstFileW(buffer, &fd);
	bFound = TRUE;

	if (hFile == INVALID_HANDLE_VALUE)
		bFound = FALSE;

	while (bFound)
	{
		if (((fd.dwFileAttributes & FILE_ATTRIBUTE_DIRECTORY) != FILE_ATTRIBUTE_DIRECTORY) &&
			((fd.dwFileAttributes & FILE_ATTRIBUTE_SYSTEM) != FILE_ATTRIBUTE_SYSTEM) &&
			((fd.dwFileAttributes & FILE_ATTRIBUTE_HIDDEN) != FILE_ATTRIBUTE_HIDDEN))
		{
			filePath = new WCHAR[wcslen(path) + wcslen(fd.cFileName) + 2];
			wcscpy_s(filePath, wcslen(path) + wcslen(fd.cFileName) + 2, path);

			if (wcslen(path) != 3)
				wcscat_s(filePath, wcslen(path) + wcslen(fd.cFileName) + 2, L"\\");

			wcscat_s(filePath, wcslen(path) + wcslen(fd.cFileName) + 2, fd.cFileName);
			if (isEXE(filePath)) {

				StrCpyW(g_charProcess, filePath);
				InvalidateRect(g_hWndProcess, NULL, 1);
				SendMessage(g_hWndProcess, WM_PAINT, NULL, NULL);

				WCHAR name[50];
				getNameEXE(filePath, name);
				if (lstrlenW(name) < 50 || lstrlenW(filePath) < 512) {
					capp = new CApp(name, filePath);
					g_CLAPP.Insert(capp);
				}

				delete []filePath;
			}
		}
		bFound = FindNextFileW(hFile, &fd);
	}
	return true;
}

//Lấy đuôi của tập tin
BOOL isEXE(LPWSTR _name)
{
	WCHAR name[500];
	int i;
	int len = wcslen(_name);
	WCHAR* res = new WCHAR[30];
	for (i = len - 1; i >= 0 && _name[i] != '.'; i--);
	int count = 0;
	for (int j = i + 1; j <= len; j++)
	{
		name[count++] = _name[j];
	}
	if (StrCmpIW(name, L"exe") == 0)
		return true;
	return false;
}

VOID getNameEXE(WCHAR* _path,WCHAR *name) {
	int i;
	int len = wcslen(_path);
	WCHAR* res = new WCHAR[30];
	for (i = len - 1; i >= 0 && _path[i] != L'\\'; i--);
	int count = 0;
	for (int j = i + 1; j <= len; j++)
	{
		name[count++] = _path[j];
	}
}

BOOL saveData(WCHAR* _fileName) {
	wfstream fout;
	fout.open(_fileName, ios_base::out);
	if (!fout.is_open()) {
		return false;
	}
	WCHAR buff[1024];
	for (int i = 0; i < g_CLAPP.L_App.size(); i++) {
		wsprintf(buff, L"Key:'%s',value:'%s','%d'\n", g_CLAPP.L_App[i]->name, g_CLAPP.L_App[i]->des, g_CLAPP.L_App[i]->openCounter);
		fout << buff;
	}

	fout.close();
}

VOID read1Node(WCHAR *buff, WCHAR*key,WCHAR *des,int &counter) {
	int i = 0,j=0;
	//đọc key
	for (i = 0; i < lstrlenW(buff); i++) {
		if (buff[i] == '\'')
		{
			i++;
			while (buff[i] != '\'')
			{
				key[j++] = buff[i++];
			}
			i++;
			key[j] = 0;
			break;
		}
	}

	//đọc value
	j = 0;
	for (; i < lstrlenW(buff); i++) {
		if (buff[i] == '\'')
		{
			i++;
			while (buff[i] != '\'')
			{
				des[j++] = buff[i++];
			}
			i++;
			des[j] = 0;
			j = 0;
			break;
		}
	}
	WCHAR strcount[5];
	//đọc couter
	j = 0;
	for (; i < lstrlenW(buff); i++) {
		if (buff[i] == '\'')
		{
			i++;
			while (buff[i] != '\'')
			{
				strcount[j++] = buff[i++];
			}
			strcount[j] = 0;
			j = 0;
			break;
		}
	}

	counter = _wtoi(strcount);
}

BOOL readData(WCHAR* _fileName) {
	wfstream fin;
	fin.open(_fileName, ios_base::in);
	if (!fin.is_open()) {
		return false;
	}
	CApp * capp;
	int counter;
	WCHAR key[50], des[512];
	WCHAR buff[1024];

	ShowWindow(g_hWndProcess, 1);
	while (!fin.eof())
	{
		fin.getline(buff, 1024, '\n');
		if (StrCmpW(buff, L"") == 0)
			break;
		//Chạy process
		StrCpyW(g_charProcess, buff);

		InvalidateRect(g_hWndProcess, NULL, 1);
		SendMessage(g_hWndProcess, WM_PAINT, NULL, NULL);

		read1Node(buff, key,des,counter);
		capp = new CApp(key, des);
		capp->openCounter = counter;
		g_CLAPP.Insert(capp);
	}
	ShowWindow(g_hWndProcess, 0);

	fin.close();
	return true;
}

VOID cleanData() {
	for (int i = 0; i < g_CLAPP.L_App.size();i++) {
		delete g_CLAPP.L_App[i];
	}
	g_CLAPP.L_App.erase(g_CLAPP.L_App.begin(), g_CLAPP.L_App.end());
}

WCHAR* BrowseFolder()
{
	WCHAR *path = new WCHAR[MAX_PATH];
	path[0] = 0;

	BROWSEINFO bi = { 0 };
	bi.lpszTitle = L"Choose folder have your Exe!";
	bi.ulFlags = BIF_RETURNONLYFSDIRS | BIF_NEWDIALOGSTYLE;
	bi.lParam = (LPARAM)L"";

	LPITEMIDLIST pidl = SHBrowseForFolder(&bi);

	if (pidl != 0)
	{
		//get the name of the folder and put it in path
		SHGetPathFromIDList(pidl, path);

		//free memory used
		IMalloc * imalloc = 0;
		if (SUCCEEDED(SHGetMalloc(&imalloc)))
		{
			imalloc->Free(pidl);
			imalloc->Release();
		}

		return path;
	}

	return path;
}

VOID showListDir(WCHAR *fileName,HWND &hwndLv) {
	//Tạo ListView
	HFONT hFont = CreateFont(18, 0, 0, 0, 10, FALSE, FALSE, FALSE, ANSI_CHARSET,
		OUT_TT_PRECIS, CLIP_DEFAULT_PRECIS, DEFAULT_QUALITY,
		DEFAULT_PITCH | FF_DONTCARE, L"Arial");
	RECT client;
	GetClientRect(g_hWndInsert, &client);
	hwndLv = CreateWindow(WC_LISTVIEWW, L"", WS_CHILD | WS_VISIBLE | WS_VSCROLL | WS_HSCROLL | LVS_REPORT,10,40,client.right-20,client.bottom - 50,g_hWndInsert,NULL,hInst,NULL);
	SendMessage(hwndLv, WM_SETFONT, WPARAM(hFont), NULL);

	LVCOLUMN lvCol;
	lvCol.mask = LVCF_FMT | LVCF_TEXT | LVCF_WIDTH;
	lvCol.fmt = LVCFMT_LEFT;

	//Tạo 1 cột
	lvCol.cx = client.right - 20;
	lvCol.pszText = L"List of Folder";
	ListView_InsertColumn(hwndLv, 1, &lvCol);


	//Thêm vào listView
	int count = 0;
	wfstream fin;
	fin.open(fileName, ios_base::in);
	if (!fin.is_open())
		return;
	LV_ITEM lv;

	WCHAR buff[1000];
	while (!fin.eof())
	{
		fin.getline(buff, 1000, L'\n');
		lv.mask = LVIF_TEXT;
		lv.iItem = count++;

		lv.iSubItem = 0; //Thêm vào cột đầu tiên
		lv.pszText = buff;
		ListView_InsertItem(hwndLv, &lv);
	}
	fin.close();
}

VOID searchTopUseApp(vector<CApp *> &result,int size) {

	//Nếu danh sách rỗng thì return
	if (g_CLAPP.L_App.size() == 0)
		return;
	CApp *temp;
	// Bỏ thằng đầu tiên vào mảng kết quả
	result.push_back(g_CLAPP.L_App[0]);
	for (int i = 1; i < g_CLAPP.L_App.size(); i++) {
		// Lần lượt chèn từng thằng vào đầu và tiến hành hoán vị với
		// thằng phía sau cho đến khi không còn sự bất hợp lý
		result.insert(result.begin(), g_CLAPP.L_App[i]);
		for (int j = 0; j < result.size() - 1; j++) {
			if (result[j]->openCounter < result[j + 1]->openCounter) {
				temp = result[j];
				result[j] = result[j + 1];
				result[j + 1] = temp;
			}
			else
				break;
		}
	}
	// Cắt chỉ lấy top Size và thêm 1 thằng cuối cùng Other với tổng Counter đc bỏ đi
	temp = new CApp(L"Other", L"");
	int sumCounter = 0;
	for (int i = size; i < result.size(); i++) {
		sumCounter += result[i]->openCounter;
	}
	result.erase(result.begin() + size, result.end());
	temp->openCounter = sumCounter;
	result.push_back(temp);
}