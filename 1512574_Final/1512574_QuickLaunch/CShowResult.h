#pragma once
#include "stdafx.h"
#include "CListApp.h"
class CShowResult
{
public:
	vector<CApp *> LApp;
	vector<HWND> hwnd;
	int top = 0, bottom = 9;
	int index = -1;
	CShowResult();
	~CShowResult();
	void Update(HWND, HINSTANCE,BOOL _showAll);
	void Down(HWND, HINSTANCE, BOOL _showAll);
	void Up(HWND, HINSTANCE, BOOL _showAll);
};

