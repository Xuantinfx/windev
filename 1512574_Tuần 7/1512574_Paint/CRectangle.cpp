#include "stdafx.h"
#include "CRectangle.h"


void CRectangle::Draw(Gdiplus::Graphics* _graphic, Gdiplus::Pen* _pen)
{

	if (position2.x > position1.x && position2.y > position1.y)
		_graphic->DrawRectangle(_pen, position1.x, position1.y, position2.x - position1.x, position2.y - position1.y);
	else
	if (position2.x > position1.x && position2.y < position1.y)
		_graphic->DrawRectangle(_pen, position1.x, position2.y, position2.x - position1.x, position1.y - position2.y);
	else
	if (position2.x < position1.x && position2.y < position1.y)
		_graphic->DrawRectangle(_pen, position2.x, position2.y, position1.x - position2.x, position1.y - position2.y);
	else
		_graphic->DrawRectangle(_pen, position2.x, position1.y, position1.x - position2.x, position2.y - position1.y);
}

TypeShape CRectangle::GetType()
{
	return rectangle;
}

void CRectangle::SaveToFile(fstream & f)
{
	f << rectangle << " ";
	CShape::SaveToFile(f);
}

CRectangle::CRectangle()
{
}

CRectangle::CRectangle(CPoint2D _point1, CPoint2D _point2)
{
	position1.x = _point1.x;
	position1.y = _point1.y;
	position2.x = _point2.x;
	position2.y = _point2.y;
}

CRectangle::CRectangle(int _x1, int _y1, int _x2, int _y2)
{
	position1.x = _x1;
	position1.y = _y1;
	position2.x = _x2;
	position2.y = _y2;
}


CRectangle::~CRectangle()
{
}
