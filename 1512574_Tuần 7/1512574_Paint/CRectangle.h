#pragma once
#include "CShape.h"
class CRectangle :
	public CShape
{
public:
	void Draw(Gdiplus::Graphics*, Gdiplus::Pen*);
	TypeShape GetType();
	void SaveToFile(fstream &f);
	CRectangle();
	CRectangle(CPoint2D _point1, CPoint2D _point2);
	CRectangle(int _x1, int _y1, int _x2, int _y2);
	~CRectangle();
};

