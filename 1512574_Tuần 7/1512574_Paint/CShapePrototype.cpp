#include "stdafx.h"
#include "CShapePrototype.h"


CShapePrototype::CShapePrototype()
{
	Prototype[square] = new CSquare();
	Prototype[rectangle] = new CRectangle();
	Prototype[circle] = new CCircle();
	Prototype[ellipse] = new CEllipse();
	Prototype[line] = new Cline();
}

CShapePrototype::~CShapePrototype()
{
	delete Prototype[square];
	delete Prototype[rectangle];
	delete Prototype[circle];
	delete Prototype[ellipse];
	delete Prototype[line];
}
