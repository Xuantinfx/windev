#pragma once
#include "CShape.h"
class Cline :
	public CShape
{
public:
	Cline();
	Cline(CPoint2D _d1, CPoint2D _d2);
	Cline(int _x1, int _y1, int _x2, int _y2);
	~Cline();
	void Draw(Gdiplus::Graphics*, Gdiplus::Pen*);
	TypeShape GetType();
	void SaveToFile(fstream &f);
};

