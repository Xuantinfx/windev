#pragma once
#include "CShape.h"
class CEllipse :
	public CShape
{	
public:
	void Draw(Gdiplus::Graphics*, Gdiplus::Pen*);
	TypeShape GetType();
	void SaveToFile(fstream &f);
	CEllipse(CPoint2D _point1, CPoint2D _point2);
	CEllipse(int _x1, int _y1, int _x2, int _y2);
	CEllipse();
	~CEllipse();
};

