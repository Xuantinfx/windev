#pragma once
#include "CShape.h"
#include "CRectangle.h"
#include "CSquare.h"
#include "CEllipse.h"
#include "CCircle.h"
#include "Cline.h"

class CShapePrototype
{
public:
	CShape *Prototype[10];

	CShapePrototype();
	~CShapePrototype();
};

