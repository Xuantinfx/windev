# MSSV: 1512574 - Đào Xuân Tin - xuantinfx@gmail.com

# Tuần 7: Paint với GDI+ và Ribbon

# Chức năng làm được
 1. Vẽ được 5 loại hình: đường thẳng, hình chữ nhật, hình vuông, hình ellipse, hình tròn bằng DGI+.
 2. Có 3 chế độ vẽ được chọn từ Tab **Shape**, chọn vẽ đường thẳng, hình chữ nhật hoặc ellipse, đang vẽ hình nào thì sẽ có **Toggle** trên loại hình cần vẽ.
 3. Nếu đang vẽ hình chữ nhật thì giữ **shift** sẽ vẽ hình vuông hoặc đang vẽ hình ellipse thì sẽ vẽ hình tròn.
 4. Đã bọc tất cả các lớp đối tượng vào từng **class** tương ứng, và class **cha** là class **CShape**.
 5. Có sử dụng mẫu Factory để tạo ra đối tượng mới và mẫu Prototype để vẽ preview.
 6. Lưu xuống file những gì đang vẽ và đọc từ file lên để vẽ tiếp.
 7. Lưu được dạng hình ảnh như bitmap hay png.
 8. Load được hình ảnh dạng bitmap hay png.
 9. Thay đổi kích thước và màu của nét vẽ

# Chức năng chưa làm được
 Không có

# Luồng sự kiện phụ
 1. Có nút Edit > Undo để thu hồi nét vừa vẽ.
 2. Xử lí tên file nhập thiếu tên mở rộng thì sẽ tự thêm phần mở rộng
 3. Sử dụng BitBlt và một số hàm khác để vẽ được mượt mà hơn, không bị giật.

# Link demo youtube: https://www.youtube.com/watch?v=RZwqSK79XC4&feature=youtu.be
# Link Bitbucket: https://Xuantinfx@bitbucket.org/Xuantinfx/windev.git