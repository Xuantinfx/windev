﻿Họ và tên:	Đào Xuân Tin
MSSV:		1512574
- Chức năng đã thực hiện được:
	1. Tạo ra giao diện có một edit control để nhập liệu.
	2. Lưu lại dưới dạng file text. Mở file và lưu file đều sử dụng OpenFileDialog.
	3. Mở một tập tin, đọc nội dung và hiển thị để chỉnh sửa tiếp.
	4. Hỗ trợ 3 thao tác Cut - Copy - Paste.
- Luồng sự kiện chính: mở file từ OpenFileDialog và hiển thị lên ô edit control và cho phép chỉnh sửa và lưu lại.
- Luồng sự kiện phụ:
	+ Phát hiện sự thay đổi trong edit control để bắt người dùng lưu lại trước khi thoát
	+ Chức năng thay đổi font chữ cho người dùng lựa chọn (View > Font).
	+ Hỗ trợ thao tác Cut - Copy - Paste ở trên Menu Edit.
- Link Bitbucket: git clone https://Xuantinfx@bitbucket.org/Xuantinfx/windev.git
