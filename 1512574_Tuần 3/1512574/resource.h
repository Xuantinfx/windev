//{{NO_DEPENDENCIES}}
// Microsoft Visual C++ generated include file.
// Used by 1512574.rc
//
#define IDC_MYICON                      2
#define IDD_1512574_DIALOG              102
#define IDS_APP_TITLE                   103
#define IDD_ABOUTBOX                    103
#define IDM_ABOUT                       104
#define IDM_EXIT                        105
#define IDI_1512574                     107
#define IDI_SMALL                       108
#define IDC_1512574                     109
#define ID_EDIT                         110
#define IDR_MAINFRAME                   128
#define ID_FILE_OPENFI                  32771
#define ID_FILE_SAVEFILE                32772
#define ID_VIEWS_CHOOSEFONT             32773
#define ID_EDIT_UNDO                    32774
#define ID_EDIT_COPY                    32775
#define ID_EDIT_CUT                     32776
#define ID_FILE_NEW                     32777
#define ID_VIEW_CHOOSEFONT              32778
#define ID_EDIT_PASTE                   32779
#define ID_FILE_SAVEAS                  32780
#define IDC_STATIC                      -1

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NO_MFC                     1
#define _APS_NEXT_RESOURCE_VALUE        129
#define _APS_NEXT_COMMAND_VALUE         32781
#define _APS_NEXT_CONTROL_VALUE         1000
#define _APS_NEXT_SYMED_VALUE           111
#endif
#endif
