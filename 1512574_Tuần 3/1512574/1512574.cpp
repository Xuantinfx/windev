﻿// 1512574.cpp : Defines the entry point for the application.
//

#include "stdafx.h"
#include "1512574.h"

#pragma comment(linker,"\"/manifestdependency:type='win32' name='Microsoft.Windows.Common-Controls' version='6.0.0.0' processorArchitecture='*' publicKeyToken='6595b64144ccf1df' language='*'\"")
#pragma comment(lib, "ComCtl32.lib")

#define MAX_LOADSTRING 100

// Global Variables:
HINSTANCE hInst;                                // current instance
WCHAR szTitle[MAX_LOADSTRING];                  // The title bar text
WCHAR szWindowClass[MAX_LOADSTRING];            // the main window class name

HWND mainEdit = NULL;							// Handle của của sổ nhập
BOOL edited = FALSE;							// Biến lưu sự thay đổi file đã mở

WCHAR fileNameNow[300] = TEXT("");							// Lưu địa chỉ file đang làm

CHOOSEFONT cf;									//Lưu font
static LOGFONT lf;
HFONT hfont;

BOOL OnCreate(HWND hwnd, LPCREATESTRUCT lpCreateStruct);
void OnCommand(HWND hwnd, int id, HWND hwndCtl, UINT codeNotify);
void OnPaint(HWND hwnd);
void OnSize(HWND hwnd, UINT state, int cx, int cy);
void OnClose(HWND hwnd);
void OnDestroy(HWND hwnd);


// Forward declarations of functions included in this code module:
ATOM                MyRegisterClass(HINSTANCE hInstance);
BOOL                InitInstance(HINSTANCE, int);
LRESULT CALLBACK    WndProc(HWND, UINT, WPARAM, LPARAM);
INT_PTR CALLBACK    About(HWND, UINT, WPARAM, LPARAM);

int APIENTRY wWinMain(_In_ HINSTANCE hInstance,
                     _In_opt_ HINSTANCE hPrevInstance,
                     _In_ LPWSTR    lpCmdLine,
                     _In_ int       nCmdShow)
{
    UNREFERENCED_PARAMETER(hPrevInstance);
    UNREFERENCED_PARAMETER(lpCmdLine);

    // TODO: Place code here.

    // Initialize global strings
    LoadStringW(hInstance, IDS_APP_TITLE, szTitle, MAX_LOADSTRING);
    LoadStringW(hInstance, IDC_1512574, szWindowClass, MAX_LOADSTRING);

	wsprintf(szTitle, L"Notepad");

    MyRegisterClass(hInstance);

    // Perform application initialization:
    if (!InitInstance (hInstance, nCmdShow))
    {
        return FALSE;
    }

    HACCEL hAccelTable = LoadAccelerators(hInstance, MAKEINTRESOURCE(IDC_1512574));

    MSG msg;

    // Main message loop
    while (GetMessage(&msg, nullptr, 0, 0))
    {
        if (!TranslateAccelerator(msg.hwnd, hAccelTable, &msg))
        {
            TranslateMessage(&msg);
            DispatchMessage(&msg);
        }
    }

    return (int) msg.wParam;
}



//
//  FUNCTION: MyRegisterClass()
//
//  PURPOSE: Registers the window class.
//
ATOM MyRegisterClass(HINSTANCE hInstance)
{
    WNDCLASSEXW wcex;

    wcex.cbSize = sizeof(WNDCLASSEX);

    wcex.style          = CS_HREDRAW | CS_VREDRAW;
    wcex.lpfnWndProc    = WndProc;
    wcex.cbClsExtra     = 0;
    wcex.cbWndExtra     = 0;
    wcex.hInstance      = hInstance;
    wcex.hIcon          = LoadIcon(hInstance, MAKEINTRESOURCE(IDI_1512574));
    wcex.hCursor        = LoadCursor(nullptr, IDC_ARROW);
    wcex.hbrBackground  = (HBRUSH)(COLOR_WINDOW + 1);
    wcex.lpszMenuName   = MAKEINTRESOURCEW(IDC_1512574);
    wcex.lpszClassName  = szWindowClass;
    wcex.hIconSm        = LoadIcon(wcex.hInstance, MAKEINTRESOURCE(IDI_SMALL));

    return RegisterClassExW(&wcex);
}

//
//   FUNCTION: InitInstance(HINSTANCE, int)
//
//   PURPOSE: Saves instance handle and creates main window
//
//   COMMENTS:
//
//        In this function, we save the instance handle in a global variable and
//        create and display the main program window.
//
BOOL InitInstance(HINSTANCE hInstance, int nCmdShow)
{
   hInst = hInstance; // Store instance handle in our global variable

   HWND hWnd = CreateWindowW(szWindowClass, szTitle, WS_OVERLAPPEDWINDOW,
      CW_USEDEFAULT, 0, CW_USEDEFAULT, 0, nullptr, nullptr, hInstance, nullptr);

   if (!hWnd)
   {
      return FALSE;
   }

   ShowWindow(hWnd, nCmdShow);
   UpdateWindow(hWnd);

   return TRUE;
}

//
//  FUNCTION: WndProc(HWND, UINT, WPARAM, LPARAM)
//
//  PURPOSE:  Processes messages for the main window.
//
//  WM_COMMAND  - process the application menu
//  WM_PAINT    - Paint the main window
//  WM_DESTROY  - post a quit message and return
//
//
LRESULT CALLBACK WndProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam)
{
    switch (message)
    {
		HANDLE_MSG(hWnd, WM_CREATE, OnCreate);
		HANDLE_MSG(hWnd, WM_COMMAND, OnCommand);
		HANDLE_MSG(hWnd, WM_PAINT, OnPaint);
		HANDLE_MSG(hWnd, WM_SIZE,OnSize);
		HANDLE_MSG(hWnd, WM_DESTROY, OnDestroy);
		HANDLE_MSG(hWnd, WM_CLOSE, OnClose);
    default:
        return DefWindowProc(hWnd, message, wParam, lParam);
    }
    return 0;
}

// Message handler for about box.
INT_PTR CALLBACK About(HWND hDlg, UINT message, WPARAM wParam, LPARAM lParam)
{
    UNREFERENCED_PARAMETER(lParam);
    switch (message)
    {
    case WM_INITDIALOG:
        return (INT_PTR)TRUE;

    case WM_COMMAND:
        if (LOWORD(wParam) == IDOK || LOWORD(wParam) == IDCANCEL)
        {
            EndDialog(hDlg, LOWORD(wParam));
            return (INT_PTR)TRUE;
        }
        break;
    }
    return (INT_PTR)FALSE;
}


BOOL OnCreate(HWND hwnd, LPCREATESTRUCT lpCreateStruct) {
	// lấy font hệ thống
	LOGFONT lf;
	GetObject(GetStockObject(DEFAULT_GUI_FONT), sizeof(LOGFONT), &lf);
	HFONT hFont = CreateFont(lf.lfHeight*1.2, lf.lfWidth*1.2,
		lf.lfEscapement, lf.lfOrientation, lf.lfWeight,
		lf.lfItalic, lf.lfUnderline, lf.lfStrikeOut, lf.lfCharSet,
		lf.lfOutPrecision, lf.lfClipPrecision, lf.lfQuality,
		lf.lfPitchAndFamily, lf.lfFaceName);

	mainEdit = CreateWindow(L"EDIT",L"", WS_CHILD | WS_VISIBLE | ES_MULTILINE | ES_AUTOHSCROLL |ES_AUTOVSCROLL | WS_HSCROLL | WS_VSCROLL ,5,0,10,10,hwnd,(HMENU)ID_EDIT,hInst,NULL);
	//mainEdit = CreateWindowEx(0, L"EDIT", L"abc", WS_CHILD | WS_VISIBLE, 10, 10, 100, 100, hwnd, NULL, hInst, NULL);
	SendMessage(mainEdit, WM_SETFONT, (WPARAM)hFont, NULL);

	return true;
}

void OnCommand(HWND hwnd, int id, HWND hwndCtl, UINT codeNotify) {
	switch (id)
	{
	case ID_EDIT:
		switch (LOWORD(codeNotify))
		{
		case EN_CHANGE:
			edited = TRUE;
			break;
		default:
			break;
		}
		break;
		
	case ID_FILE_NEW:
	{
		int choise;
		if (edited == TRUE) {
			 choise = MessageBox(hwnd, L"Bạn có muốn lưu trước khi mở file mới?", L"Thông báo", MB_YESNOCANCEL);
		}
		else {
			choise = IDNO;
		}
		if (choise == IDYES) {

			WCHAR *buffer;
			unsigned int size = GetWindowTextLength(mainEdit);
			buffer = new WCHAR[size + 1];
			GetWindowText(mainEdit, buffer, size + 1);
			if (size == 0) {
				buffer[0] = '\0';
			}
			if (!SaveFile(fileNameNow, buffer)) {
				MessageBox(0, L"Không thể lưu file!", L"Lỗi lưu file", 0);
				return;
			}
			delete buffer;
		}
		else if (choise == IDNO) {

		}
		else if (choise == IDCANCEL) {
			break;
		}
		SetWindowText(mainEdit, L"");
		SetWindowText(hwnd, L"Notepad");
		edited = FALSE;
		break;
	}
	case ID_FILE_OPENFI:
	{
		//lstrcpyW(fileNameNow, L"1512574.txt");
		if (!MyCreateOpenFile(hwnd, fileNameNow)) {
			break;
		}

		//đọc file và cho vào edittext
		WCHAR *buffer;
		if (!OpenFile(fileNameNow, buffer)) {
			MessageBox(0, L"Không thể mở file!", L"Lỗi", 0);
			break;
		}
		SetWindowText(mainEdit, buffer);
		SetWindowText(hwnd, fileNameNow);

		delete[]buffer;
		edited = FALSE;
		break;
	}
	case ID_FILE_SAVEFILE:
	{
		WCHAR *buffer;
		unsigned int size = GetWindowTextLength(mainEdit);
		buffer = new WCHAR[size + 1];
		GetWindowText(mainEdit, buffer, size + 1);
		if (size == 0) {
			buffer[0] = '\0';
		}
		if (lstrcmpW(fileNameNow, L"") == 0) {
			if (!MyCreateSaveFile(mainEdit, fileNameNow)) {
				break;
			}
		}
		if (!SaveFile(fileNameNow, buffer)) {
			MessageBox(0, L"Không thể lưu file!", L"Lỗi lưu file", 0);
		}
		SetWindowText(hwnd, fileNameNow);
		delete []buffer;
		edited = FALSE;
		break;
	}
	case ID_FILE_SAVEAS:
	{
		if (!MyCreateSaveFile(mainEdit, fileNameNow)) {
			break;
		}
		WCHAR *buffer;
		unsigned int size = GetWindowTextLength(mainEdit);
		buffer = new WCHAR[size + 1];
		GetWindowText(mainEdit, buffer, size + 1);
		if (size == 0) {
			buffer[0] = '\0';
		}
		if (!SaveFile(fileNameNow, buffer)) {
			MessageBox(0, L"Không thể lưu file!", L"Lỗi lưu file", 0);
		}
		delete[]buffer;
		edited = FALSE;
		break;
	}
	case ID_EDIT_COPY:
		SendMessage(mainEdit, WM_COPY, NULL, NULL);
		break;
	case ID_EDIT_CUT:
		SendMessage(mainEdit, WM_CUT, 0, 0);
		break;
	case ID_EDIT_UNDO:
		SendMessage(mainEdit, WM_UNDO, 0, 0);
		break;
	case ID_EDIT_PASTE:
		SendMessage(mainEdit, WM_PASTE, 0, 0);
		break;
	case IDM_ABOUT:
		DialogBox(hInst, MAKEINTRESOURCE(IDD_ABOUTBOX), hwnd, About);
		break;
	case ID_VIEW_CHOOSEFONT:
		MyCreateFont(hfont);
		SendMessage(mainEdit, WM_SETFONT,(WPARAM)hfont, 0);
		break;
	case IDM_EXIT:
		SendMessage(hwnd, WM_CLOSE, 0, 0);
		break;
	default:
		break;
	}
}

void OnPaint(HWND hwnd) {
	PAINTSTRUCT ps;
	HDC hdc = BeginPaint(hwnd, &ps);
	// TODO: Add any drawing code that uses hdc here...
	EndPaint(hwnd, &ps);
}

void OnSize(HWND hwnd, UINT state, int cx, int cy) {
	//chỉnh sửa kích thước edit
	SetWindowPos(mainEdit, 0, 0, 0, cx, cy, SWP_NOMOVE | SWP_NOZORDER | SWP_NOACTIVATE);
}

void OnClose(HWND hwnd) {
	int choise;
	if (edited == TRUE) {
		choise = MessageBox(hwnd, L"Bạn có muốn lưu trước khi thoát?", L"Thông báo", MB_YESNOCANCEL);
	}
	else {
		choise = IDNO;
	}

	if (choise == IDYES) {

		WCHAR *buffer;
		unsigned int size = GetWindowTextLength(mainEdit);
		buffer = new WCHAR[size + 1];
		GetWindowText(mainEdit, buffer, size + 1);
		if (size == 0) {
			buffer[0] = '\0';
		}
		if (lstrcmpW(fileNameNow, L"") == 0) {
			if (!MyCreateSaveFile(hwnd, fileNameNow)) {
				return;
			}
		}
		if (!SaveFile(fileNameNow, buffer)) {
			MessageBox(0, L"Không thể lưu file!", L"Lỗi lưu file", 0);
			return;
		}
		delete buffer;
		OnDestroy(hwnd);
	}
	else if (choise == IDNO) {
		OnDestroy(hwnd);
	}
	else if (choise == IDCANCEL) {
		return;
	}
}

void OnDestroy(HWND hwnd) {
	PostQuitMessage(0);
}