﻿#pragma once
#include "stdafx.h"
#include <cstring>

BOOL MyCreateSaveFile(HWND hwnd, WCHAR chuoiten[]);

BOOL FAR PASCAL MyCreateFont(HFONT &font);

BOOL MyCreateOpenFile(HWND hwnd, WCHAR chuoiten[]);

BOOL OpenFile(WCHAR *Link, WCHAR *&buffer);
BOOL SaveFile(WCHAR *Link, WCHAR *&buffer);