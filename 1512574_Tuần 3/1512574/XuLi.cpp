﻿#include "XuLi.h"
#include "stdafx.h"
#include <string>

BOOL MyCreateSaveFile(HWND hwnd, WCHAR chuoiten[])
{
	OPENFILENAMEW ofn = { 0 };
	WCHAR szFile[260] = TEXT("New Text Document.txt");
	ofn.lStructSize = sizeof(ofn);
	ofn.hwndOwner = hwnd;
	ofn.lpstrFile = szFile;
	ofn.nMaxFile = sizeof(szFile);
	ofn.lpstrFilter = TEXT("Text Documents (*.txt)\0*.txt\0All Files (*.*)\0*.*\0");
	ofn.nFilterIndex = 1;
	ofn.lpstrFileTitle = NULL;
	ofn.nMaxFileTitle = 0;
	ofn.lpstrInitialDir = NULL;
	ofn.Flags = OFN_PATHMUSTEXIST | OFN_FILEMUSTEXIST;
	if (GetSaveFileName(&ofn) == FALSE)
		return FALSE;
	wsprintf(chuoiten,L"%s", ofn.lpstrFile);
	return TRUE;
}

BOOL FAR PASCAL MyCreateFont(HFONT &font)
{
	CHOOSEFONT cf;
	LOGFONT lf;
	//Khởi tạo các trường trong cấu trúc CHOOSEFONT.
	cf.lStructSize = sizeof(CHOOSEFONT);
	cf.hwndOwner = (HWND)NULL;
	cf.hDC = (HDC)NULL;
	cf.lpLogFont = &lf;
	cf.iPointSize = 0;
	cf.Flags = CF_SCREENFONTS;
	cf.rgbColors = RGB(0, 0, 0);
	cf.lCustData = 0L;
	cf.lpfnHook = NULL;
	cf.lpTemplateName = NULL;
	cf.hInstance = NULL;
	cf.lpszStyle = NULL;
	cf.nFontType = SCREEN_FONTTYPE;
	cf.nSizeMin = 0;
	cf.nSizeMax = 0;
	//Hiển thị hộp thoại font.
	if (!ChooseFont(&cf))
		return FALSE;
	/* Tạo font logic dựa trên các chọn lựa của người dùng và trả vè handle của font logic */
	font = CreateFontIndirect(cf.lpLogFont);
	return TRUE;
}

BOOL MyCreateOpenFile(HWND hwnd, WCHAR chuoiten[])
{
	OPENFILENAMEW ofn = { 0 };
	WCHAR szFile[260];
	ofn.lStructSize = sizeof(ofn);
	ofn.hwndOwner = hwnd;
	ofn.lpstrFile = szFile;
	ofn.lpstrFile[0] = '\0';
	ofn.nMaxFile = sizeof(szFile);
	ofn.lpstrFilter = L"Text Documents (*.txt)\0*.txt\0All Files (*.*)\0*.*\0";
	ofn.nFilterIndex = 1;
	ofn.lpstrFileTitle = NULL;
	ofn.nMaxFileTitle = 0;
	ofn.lpstrInitialDir = NULL;
	ofn.Flags = OFN_PATHMUSTEXIST | OFN_FILEMUSTEXIST;
	if (!GetOpenFileNameW(&ofn))
		return FALSE;
	wsprintf(chuoiten, L"%s", ofn.lpstrFile);
	//wcsncpy(chuoiten,ofn.lpstrFile,lstrlenW(ofn.lpstrFile));
	return TRUE;
}

INT SizeFile(std::fstream &f) {
	f.seekg(0, f.end);
	int size = f.tellg();
	f.seekg(0, f.beg);
	return size;
}

BOOL OpenFile(WCHAR *Link,WCHAR *&buffer) {
	std::fstream fin;
	fin.open(Link, std::fstream::in);
	if (!fin.is_open())
		return FALSE;
	INT size = SizeFile(fin);
	buffer = new WCHAR[size + 1];
	char temp = 0;
	int dem = 0;
	while(!fin.eof()){
		if (fin.get(temp)) {
			if (temp == '\n') {
				buffer[dem++] = (WCHAR)'\r';
			}
			buffer[dem++] = (WCHAR)temp;
		}
	}
	buffer[dem] = '\0';
	fin.close();
	return TRUE;
}

BOOL SaveFile(WCHAR *Link, WCHAR *&buffer) {
	std::fstream fout;
	fout.open(Link, std::ios_base::out);
	if(!fout.is_open()){
		return FALSE;
	}

	unsigned char temp = 0;
	for (int i = 0; i < lstrlenW(buffer); i++){
		temp = buffer[i];
		if (temp == '\r')
			continue;
		fout << temp;
	}
	fout.close();
	return TRUE;
}

