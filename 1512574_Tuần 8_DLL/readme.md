# MSSV: 1512574 - Đào Xuân Tin - xuantinfx@gmail.com

# Tuần 8: Sử dụng DLL trong Paint

# Chức năng làm được
 1. Bỏ chức năng Opendialog, SaveDialog và SaveBitMap vào DLL
 2. Sử dụng load Run-time

# Chức năng chưa làm được
 Không có

# Luồng sự kiện phụ
 1. Nếu không có file dll hoặc dll khác thì thông báo lỗi rõ ràng

# Link demo youtube: https://www.youtube.com/watch?v=mVRhYl68xvA&feature=youtu.bebe
# Link Bitbucket: https://Xuantinfx@bitbucket.org/Xuantinfx/windev.git