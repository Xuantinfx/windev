#include "stdafx.h"

#define EXPORT	__declspec(dllexport)
//#define EXPORT  __declspec(dllexport)

EXPORT BOOL DialogOpenFile(HWND hwnd, WCHAR fileName[],int *type_Img)
{
	OPENFILENAMEW ofn = { 0 };
	WCHAR szFile[260];
	ofn.lStructSize = sizeof(ofn);
	ofn.hwndOwner = hwnd;
	ofn.lpstrFile = szFile;
	ofn.lpstrFile[0] = '\0';
	ofn.nMaxFile = sizeof(szFile);
	ofn.lpstrFilter = L"Paint Symple (*.paint)\0*.paint\0png (*.png)\0*.png\0Bitmap (*.bmp)\0*.bmp\0All File (*.*)\0*.*\0";
	ofn.nFilterIndex = 1;
	ofn.lpstrFileTitle = NULL;
	ofn.nMaxFileTitle = 0;
	ofn.lpstrInitialDir = NULL;
	ofn.Flags = OFN_PATHMUSTEXIST | OFN_FILEMUSTEXIST;
	if (!GetOpenFileNameW(&ofn))
		return FALSE;
	wsprintf(fileName, L"%s", ofn.lpstrFile);
	switch (ofn.nFilterIndex)
	{
	case 1:
	{
		*type_Img = 1;
		break;
	}
	case 2:
	{
		*type_Img = 2;
		break;
	}
	case 3:
	{
		*type_Img = 3;
		break;
	}
	case 4:
	{
		*type_Img = 4;
	}
	break;
	default:
		wsprintf(fileName, L"%s", ofn.lpstrFile);
		break;
	}
	//wcsncpy(chuoiten,ofn.lpstrFile,lstrlenW(ofn.lpstrFile));
	return TRUE;
}

EXPORT BOOL DiaLogSaveFile(HWND hwnd, WCHAR fileName[],int *type_Img)
{
	OPENFILENAMEW ofn = { 0 };
	WCHAR szFile[260] = TEXT("Paint Symple");
	ofn.lStructSize = sizeof(ofn);
	ofn.hwndOwner = hwnd;
	ofn.lpstrFile = szFile;
	ofn.nMaxFile = sizeof(szFile);
	ofn.lpstrFilter = TEXT("Paint Symple(*.paint)\0 * .paint\0png(*.png)\0 * .png\0Bitmap(*.bmp)\0 * .bmp\0All File(*.*)\0 * .*\0");
	ofn.nFilterIndex = 1;
	ofn.lpstrFileTitle = NULL;
	ofn.nMaxFileTitle = 0;
	ofn.lpstrInitialDir = NULL;
	ofn.Flags = OFN_PATHMUSTEXIST | OFN_FILEMUSTEXIST;
	if (GetSaveFileName(&ofn) == FALSE)
		return FALSE;
	switch (ofn.nFilterIndex)
	{
	case 1:
	{
		wsprintf(fileName, L"%s.paint", ofn.lpstrFile);
		*type_Img = 1;
		break;
	}
	case 2:
	{
		wsprintf(fileName, L"%s.png", ofn.lpstrFile);
		*type_Img = 2;
		break;
	}
	case 3:
	{
		wsprintf(fileName, L"%s.bmp", ofn.lpstrFile);
		*type_Img = 3;
		break;
	}
	case 4:
	{
		wsprintf(fileName, L"%s", ofn.lpstrFile);
		*type_Img = 4;
	}
	break;
	default:
		wsprintf(fileName, L"%s", ofn.lpstrFile);
		break;
	}
	return TRUE;
}

EXPORT bool saveBitmap(LPCWSTR filename, HBITMAP bmp, HPALETTE pal)
{
	bool result = false;
	PICTDESC pd;

	pd.cbSizeofstruct = sizeof(PICTDESC);
	pd.picType = PICTYPE_BITMAP;
	//pd.picType = PICTYPE_UNINITIALIZED;
	pd.bmp.hbitmap = bmp;
	pd.bmp.hpal = pal;

	LPPICTURE picture;
	HRESULT res = OleCreatePictureIndirect(&pd, IID_IPicture, false,
		reinterpret_cast<void**>(&picture));

	if (!SUCCEEDED(res))
		return false;

	LPSTREAM stream;
	res = CreateStreamOnHGlobal(0, true, &stream);

	if (!SUCCEEDED(res))
	{
		picture->Release();
		return false;
	}

	LONG bytes_streamed;
	res = picture->SaveAsFile(stream, true, &bytes_streamed);

	HANDLE file = CreateFile(filename, GENERIC_WRITE, FILE_SHARE_READ, 0,
		CREATE_ALWAYS, FILE_ATTRIBUTE_NORMAL, 0);

	if (!SUCCEEDED(res) || !file)
	{
		stream->Release();
		picture->Release();
		return false;
	}

	HGLOBAL mem = 0;
	GetHGlobalFromStream(stream, &mem);
	LPVOID data = GlobalLock(mem);

	DWORD bytes_written;

	result = !!WriteFile(file, data, bytes_streamed, &bytes_written, 0);
	result &= (bytes_written == static_cast<DWORD>(bytes_streamed));

	GlobalUnlock(mem);
	CloseHandle(file);

	stream->Release();
	picture->Release();

	return result;
}