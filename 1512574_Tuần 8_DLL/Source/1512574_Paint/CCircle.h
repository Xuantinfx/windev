#pragma once
#include "CShape.h"
class CCircle :
	public CShape
{
public:
	CCircle();
	CCircle(CPoint2D _point1, CPoint2D _point2);
	CCircle(int _x1, int _y1, int _x2,int _y2);
	void Draw(Gdiplus::Graphics*, Gdiplus::Pen*);
	TypeShape GetType();
	void SaveToFile(fstream &f);
	~CCircle();
};

