#pragma once
#include <vector>
#include "CShape.h"
#include "CRectangle.h"
#include "CSquare.h"
#include "CEllipse.h"
#include "CCircle.h"
#include "Cline.h"

using namespace std;

class CShapeFactory
{
public:
	CShape *GetShape(TypeShape _type,int _xStart, int _yStart, int _xEnd,int _yEnd,int _RGB[3],int _size);
	CShape *GetShape(TypeShape _type,CPoint2D position1,CPoint2D position2,int _RGB[3], int _size);

	CShapeFactory();
	~CShapeFactory();
};

