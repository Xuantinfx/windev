﻿// 1512574Paint.cpp : Defines the entry point for the application.
//

#include "stdafx.h"
#include "1512574Paint.h"

#define MAX_LOADSTRING 100


// Global Variables:
HINSTANCE hInst;                                // current instance
WCHAR szTitle[MAX_LOADSTRING];                  // The title bar text
WCHAR szWindowClass[MAX_LOADSTRING];            // the main window class name

												/******************* Khu vực khai báo biến toàn cục******************/
BOOL				g_isDrawing = FALSE;
int					g_xStart = 0, g_yStart = 0;
int					g_xEnd, g_yEnd;
TypeShape			g_ShapeChoise = line;
vector<CShape*>		g_ListObject;
CShapeFactory		g_ShapeFatory;
CShapePrototype		g_ShapePrototype;
int					g_RGB[3] = { 0 };
int					g_size = 0;
WCHAR				fileName[1000] = { 0 };
//1 là pain, 2 là png, 3 là bmp, 4 là all file
int					type_Img = 1;
HMENU				hMenu = NULL;
HMENU				hPopupMenu;
Gdiplus::GdiplusStartupInput gdiplusstarupinput;
ULONG_PTR			gdiplustoken;
BOOL				is_saving = FALSE;


int APIENTRY wWinMain(_In_ HINSTANCE hInstance,
	_In_opt_ HINSTANCE hPrevInstance,
	_In_ LPWSTR    lpCmdLine,
	_In_ int       nCmdShow)
{
	UNREFERENCED_PARAMETER(hPrevInstance);
	UNREFERENCED_PARAMETER(lpCmdLine);

	// TODO: Place code here.
	HRESULT hr = CoInitialize(NULL);
	if (FALSE(hr)) {
		return FALSE;
	}

	// Initialize global strings
	LoadStringW(hInstance, IDS_APP_TITLE, szTitle, MAX_LOADSTRING);
	LoadStringW(hInstance, IDC_1512574PAINT, szWindowClass, MAX_LOADSTRING);
	MyRegisterClass(hInstance);

	// Perform application initialization:
	if (!InitInstance(hInstance, nCmdShow))
	{
		return FALSE;
	}

	HACCEL hAccelTable = LoadAccelerators(hInstance, MAKEINTRESOURCE(IDC_1512574PAINT));

	MSG msg;

	// Main message loop:
	while (GetMessage(&msg, nullptr, 0, 0))
	{
		if (!TranslateAccelerator(msg.hwnd, hAccelTable, &msg))
		{
			TranslateMessage(&msg);
			DispatchMessage(&msg);
		}
	}

	CoUninitialize();
	return (int)msg.wParam;
}


//
//  FUNCTION: MyRegisterClass()
//
//  PURPOSE: Registers the window class.
//
ATOM MyRegisterClass(HINSTANCE hInstance)
{
	WNDCLASSEXW wcex;

	wcex.cbSize = sizeof(WNDCLASSEX);

	wcex.style = 0;//CS_HREDRAW | CS_VREDRAW;
	wcex.lpfnWndProc = WndProc;
	wcex.cbClsExtra = 0;
	wcex.cbWndExtra = 0;
	wcex.hInstance = hInstance;
	wcex.hIcon = LoadIcon(hInstance, MAKEINTRESOURCE(IDI_1512574PAINT));
	wcex.hCursor = LoadCursor(nullptr, IDC_ARROW);
	wcex.hbrBackground = (HBRUSH)(COLOR_WINDOW + 1);
	wcex.lpszMenuName = MAKEINTRESOURCEW(IDC_1512574PAINT);
	wcex.lpszClassName = szWindowClass;
	wcex.hIconSm = LoadIcon(wcex.hInstance, MAKEINTRESOURCE(IDI_SMALL));

	return RegisterClassExW(&wcex);
}

//
//   FUNCTION: InitInstance(HINSTANCE, int)
//
//   PURPOSE: Saves instance handle and creates main window
//
//   COMMENTS:
//
//        In this function, we save the instance handle in a global variable and
//        create and display the main program window.
//
BOOL InitInstance(HINSTANCE hInstance, int nCmdShow)
{
	hInst = hInstance; // Store instance handle in our global variable

	HWND hWnd = CreateWindowW(szWindowClass, szTitle, WS_OVERLAPPEDWINDOW,
		CW_USEDEFAULT, 0, CW_USEDEFAULT, 0, nullptr, nullptr, hInstance, nullptr);

	if (!hWnd)
	{
		return FALSE;
	}

	ShowWindow(hWnd, nCmdShow);
	UpdateWindow(hWnd);
	return TRUE;
}

//
//  FUNCTION: WndProc(HWND, UINT, WPARAM, LPARAM)
//
//  PURPOSE:  Processes messages for the main window.
//
//  WM_COMMAND  - process the application menu
//  WM_PAINT    - Paint the main window
//  WM_DESTROY  - post a quit message and return
//
//
LRESULT CALLBACK WndProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam)
{
	switch (message)
	{
	case WM_CREATE:
	{
		//khởi tạo file Ribbon
		InitializeFramework(hWnd);

		//Khởi tạo GDI+
		GdiplusStartup(&gdiplustoken, &gdiplusstarupinput, NULL);

		SetWindowText(hWnd, L"New Paint Symple (unsave)");

		hMenu = GetMenu(hWnd);

		//chỉnh chức năng mặc định lúc vào là vẽ đường thẳng
		HandleModePaint(line);

		//Thêm các thuộc tính cho Popup
		hPopupMenu = CreatePopupMenu();
		InsertMenu(hPopupMenu, 0, MF_CHECKED, ID_PAINT_LINE, L"Line");
		InsertMenu(hPopupMenu, 1, MF_UNCHECKED, ID_PAINT_Ellipse, L"Ellipse");
		InsertMenu(hPopupMenu, 2, MF_UNCHECKED, ID_PAINT_RECTANGLE, L"Rectangle");

		//Chỉnh toggle button
		{
			PROPVARIANT value;

			//lấy thông tin cho value
			g_pFramework->GetUICommandProperty(ID_BTN_LINE, UI_PKEY_BooleanValue, &value);
			value.boolVal = 0;

			//tắt đi rect và ellip
			g_pFramework->SetUICommandProperty(ID_BTN_RECT, UI_PKEY_BooleanValue, value);
			g_pFramework->SetUICommandProperty(ID_BTN_ELLIPSE, UI_PKEY_BooleanValue, value);

			//bật line
			value.boolVal = -1;
			g_pFramework->SetUICommandProperty(ID_BTN_LINE, UI_PKEY_BooleanValue, value);

			SendMessage(GetForegroundWindow(), WM_COMMAND, ID_PAINT_LINE, 0);
		}
		//bật size 1
		{
			g_size = 1;
			PROPVARIANT value;
			//lấy thông tin cho value
			g_pFramework->GetUICommandProperty(ID_BTN_SIZE_1, UI_PKEY_BooleanValue, &value);
			value.boolVal = 0;

			//tắt đi size2 và size3
			g_pFramework->SetUICommandProperty(ID_BTN_SIZE_2, UI_PKEY_BooleanValue, value);
			g_pFramework->SetUICommandProperty(ID_BTN_SIZE_3, UI_PKEY_BooleanValue, value);

			//bật size1
			value.boolVal = -1;
			g_pFramework->SetUICommandProperty(ID_BTN_SIZE_1, UI_PKEY_BooleanValue, value);
		}
	}
	break;
	case WM_MOUSEMOVE:
		if (g_isDrawing)
		{
			g_xEnd = GET_X_LPARAM(lParam);
			g_yEnd = GET_Y_LPARAM(lParam);
			g_ShapePrototype.Prototype[g_ShapeChoise]->SetPosition(CPoint2D(g_xStart, g_yStart), CPoint2D(g_xEnd, g_yEnd));
			g_ShapePrototype.Prototype[g_ShapeChoise]->RGB[0] = g_RGB[0];
			g_ShapePrototype.Prototype[g_ShapeChoise]->RGB[1] = g_RGB[1];
			g_ShapePrototype.Prototype[g_ShapeChoise]->RGB[2] = g_RGB[2];
			g_ShapePrototype.Prototype[g_ShapeChoise]->size = g_size;

			//SendMessage(hWnd, WM_PAINT, 0, 0);
			RECT rect;
			GetClientRect(hWnd, &rect);
			InvalidateRect(hWnd, &rect, false);
			//SendMessage(hWnd, WM_PAINT, 0, 0);
		}
		break;
	case WM_LBUTTONDOWN:
		SetCapture(hWnd);
		g_isDrawing = TRUE;
		g_xStart = GET_X_LPARAM(lParam);
		g_yStart = GET_Y_LPARAM(lParam);
		// file .xml may dau
		break;
	case WM_LBUTTONUP:
		ReleaseCapture();
		if (g_isDrawing)
		{
			g_xEnd = GET_X_LPARAM(lParam);
			g_yEnd = GET_Y_LPARAM(lParam);
			g_isDrawing = FALSE;
			//lưu hình vào
			g_ListObject.push_back(g_ShapeFatory.GetShape(g_ShapeChoise, g_xStart, g_yStart, g_xEnd, g_yEnd, g_RGB, g_size));
			RECT rect;
			GetClientRect(hWnd, &rect);
			InvalidateRect(hWnd, &rect, false);
			///// cho nay may cho false, tuc la khong can xoa ribbon. tuong cho cho nao may cung cho false. con gi nua khong
			//còn chỗ toglebutton làm sao để nhấn 1 cái thì 2 cái kia tự nhả ý.
			// trong windev thay co de cap ma
			//không có thấy, chỉ luôn đi
			//
			//SendMessage(hWnd, WM_PAINT, 0, 0);
		}
		break;
	case WM_KEYDOWN:
	{
		//Nếu nút shift được nhấn
		if (wParam == VK_SHIFT)
		{
			if (g_ShapeChoise == rectangle)
				g_ShapeChoise = square;
			if (g_ShapeChoise == ellipse)
				g_ShapeChoise = circle;
		}
	}
	break;
	case WM_KEYUP:
	{
		//nếu nút shift được thả ra
		if (wParam == VK_SHIFT)
		{
			if (g_ShapeChoise == square)
				g_ShapeChoise = rectangle;
			if (g_ShapeChoise == circle)
				g_ShapeChoise = ellipse;
		}
		break;
	}
	//case WM_RBUTTONUP:
	//{
	//	POINT point;

	//	//lấy tọa độ thả chuột phải trên màn hình chứa
	//	point.x = GET_X_LPARAM(lParam);
	//	point.y = GET_Y_LPARAM(lParam);

	//	//chuyển tọa độ màn hình chứa sang màn hình chính
	//	ClientToScreen(hWnd, &point);

	//	//Hiển thị popup
	//	TrackPopupMenu(hPopupMenu, TPM_BOTTOMALIGN, point.x, point.y, 0, hWnd, NULL);
	//}
	//	break;
	case WM_CONTEXTMENU:
		POINT pt;
		//POINTSTOPOINT(pt, lParam);

		//ShowContextualUI(pt, hWnd);
		break;
	case WM_COMMAND:
	{
		int wmId = LOWORD(wParam);
		// Parse the menu selections:
		switch (wmId)
		{
		case IDM_ABOUT:
			DialogBox(hInst, MAKEINTRESOURCE(IDD_DIALOG1), hWnd, About);
			break;
		case IDM_EXIT:
			SendMessage(hWnd, WM_CLOSE, NULL, NULL);
			break;
		case ID_FILE_NEW:
		{
			SetWindowText(hWnd, L"New Paint Symple (unsave)");
			//xóa tất cả phần tử trước khi tạo lại trang
			while (!g_ListObject.empty())
			{
				delete g_ListObject[g_ListObject.size() - 1];
				g_ListObject.pop_back();
			}
			//cập nhật lại tên file thành rỗng
			wsprintf(fileName, L"");

			//Vẽ lại windown
			RECT rect;
			GetClientRect(hWnd, &rect);
			InvalidateRect(hWnd, &rect, false);
			//SendMessage(hWnd, WM_PAINT, 0, 0);
		}
		break;
		case ID_FILE_LOAD:
		{
			//khai báo Prototype để của hàm trong DLL	
			typedef BOOL(*MYPROC)(HWND, WCHAR*, int*);
			HINSTANCE hinstLib;
			MYPROC Procaddr;
			//Load dll
			hinstLib = LoadLibrary(L"DLL.dll");
			//nếu load thành công thì lấy địa chỉ hàm DialogOpenFile
			if (hinstLib != NULL) {
				Procaddr = (MYPROC)GetProcAddress(hinstLib, "DialogOpenFile");
				if (Procaddr != NULL) {
					if (Procaddr(hWnd, fileName, &type_Img))
					{
						if (type_Img == 1)
						{
							LoadOject(fileName);
						}
						SetWindowText(hWnd, fileName);
						RECT rect;
						GetClientRect(hWnd, &rect);
						InvalidateRect(hWnd, &rect, false);
						//SendMessage(hWnd, WM_PAINT, 0, 0);
					}
					else
					{
						MessageBeep(MB_ICONEXCLAMATION);
						MessageBox(hWnd, L"Không thể lưu file, thử đường dẫn khác!", L"Thông báo", MB_OK);
					}
				}
				else
				{
					MessageBeep(MB_ICONEXCLAMATION);
					MessageBox(hWnd, L"File DLL.dll không đúng định dạng!", L"Thông báo", MB_OK);
				}
			}
			else
			{
				MessageBeep(MB_ICONEXCLAMATION);
				MessageBox(hWnd, L"Không thể tìm thấy file DLL.dll!", L"Thông báo", MB_OK);
			}

			FreeProcInstance(Procaddr);
			FreeLibrary(hinstLib);
		}
		break;
		case ID_FILE_SAVE:
		{
			if (lstrcmpW(fileName, L"") == 0)
			{
				//khai báo Prototype để của hàm trong DLL	
				typedef BOOL(*MYPROC)(HWND, WCHAR*, int*);
				HINSTANCE hinstLib;
				MYPROC Procaddr;
				//Load dll
				hinstLib = LoadLibrary(L"DLL.dll");
				//nếu load thành công thì lấy địa chỉ hàm DialogOpenFile
				if (hinstLib != NULL) {
					Procaddr = (MYPROC)GetProcAddress(hinstLib, "DiaLogSaveFile");
					if (Procaddr != NULL) {
						if ((MYPROC)Procaddr(hWnd, fileName, &type_Img))
						{
							is_saving = TRUE;
							SendMessage(hWnd, WM_PAINT, 0, 0);
							SetWindowText(hWnd, fileName);
						}
						else
						{
							is_saving = TRUE;
							SendMessage(hWnd, WM_PAINT, 0, 0);
						}
					}
					else
					{
						MessageBeep(MB_ICONEXCLAMATION);
						MessageBox(hWnd, L"File DLL.dll không đúng định dạng!", L"Thông báo", MB_OK);
					}
				}
				else
				{
					MessageBeep(MB_ICONEXCLAMATION);
					MessageBox(hWnd, L"Không thể tìm thấy file DLL.dll!", L"Thông báo", MB_OK);
				}
			}
			else
			{
				is_saving = TRUE;
				SendMessage(hWnd, WM_PAINT, 0, 0);
			}
			break;
		}
		case ID_FILE_SAVEASNEWPAINT:
		{
			//khai báo Prototype để của hàm trong DLL	
			typedef BOOL(*MYPROC)(HWND, WCHAR*, int*);
			HINSTANCE hinstLib;
			MYPROC Procaddr;
			//Load dll
			hinstLib = LoadLibrary(L"DLL.dll");
			//nếu load thành công thì lấy địa chỉ hàm DialogOpenFile
			if (hinstLib != NULL) {
				Procaddr = (MYPROC)GetProcAddress(hinstLib, "DiaLogSaveFile");
				if (Procaddr != NULL) {
					if ((MYPROC)Procaddr(hWnd, fileName, &type_Img))
					{
						is_saving = TRUE;
						SendMessage(hWnd, WM_PAINT, 0, 0);
						SetWindowText(hWnd, fileName);
					}
				}
				else
				{
					MessageBeep(MB_ICONEXCLAMATION);
					MessageBox(hWnd, L"File DLL.dll không đúng định dạng!", L"Thông báo", MB_OK);
				}
			}
			else
			{
				MessageBeep(MB_ICONEXCLAMATION);
				MessageBox(hWnd, L"Không thể tìm thấy file DLL.dll!", L"Thông báo", MB_OK);
			}
		}
			break;
		case ID_EDIT_UNDO:
		{
			if (!g_ListObject.empty())
			{
				delete g_ListObject[g_ListObject.size() - 1];
				g_ListObject.pop_back();
				RECT rect;
				GetClientRect(hWnd, &rect);
				InvalidateRect(hWnd, &rect, false);
				//SendMessage(hWnd, WM_PAINT, 0, 0);
			}
			break;
		}
		case ID_PAINT_LINE:
			HandleModePaint(line);
			break;
		case ID_PAINT_Ellipse:
			HandleModePaint(ellipse);
			break;
		case ID_PAINT_RECTANGLE:
			HandleModePaint(rectangle);
			break;
		default:
			return DefWindowProc(hWnd, message, wParam, lParam);
		}
	}
	break;
	case WM_PAINT:
	{
		PAINTSTRUCT ps;
		HDC hdc = BeginPaint(hWnd, &ps);

		RECT rect;
		GetClientRect(hWnd, &rect);
		int width = rect.right - rect.left;
		int height = rect.bottom - rect.top;

		HDC memDC = CreateCompatibleDC(hdc);
		HBITMAP hBitmap = CreateCompatibleBitmap(hdc, width, height);

		SelectObject(memDC, hBitmap);
		Gdiplus::Graphics * graphicmem = new Gdiplus::Graphics(memDC);
		graphicmem->SetSmoothingMode(Gdiplus::SmoothingMode::SmoothingModeAntiAlias);

		//vẽ màu trắng
		Gdiplus::SolidBrush back(Gdiplus::Color(255, 255, 255));
		graphicmem->FillRectangle(&back, rect.left, rect.top, width, height);

		//load BitMap
		if (lstrcmpW(fileName, L"") != 0)
		{
			switch (type_Img)
			{
			case 1:
				break;
			case 2:
			case 3:
			{
				Gdiplus::Image img(fileName);
				graphicmem->DrawImage(&img, 0, 0, width, height);
				break;
			}
			case 4:
				break;
			default:
				break;
			}
		}

		// Vẽ lại những gì trong bộ nhớ

		CShape *temp;
		for (int i = 0; i < g_ListObject.size(); i++)
		{
			temp = g_ListObject[i];
			Gdiplus::Pen* pen = new Gdiplus::Pen(Gdiplus::Color(255, temp->RGB[0], temp->RGB[1], temp->RGB[2]), temp->size);
			g_ListObject[i]->Draw(graphicmem, pen);
			delete pen;
		}

		// Vẽ preview nếu đang vẽ
		if (g_isDrawing)
		{
			Gdiplus::Pen* pen = new Gdiplus::Pen(Gdiplus::Color(255, g_RGB[0], g_RGB[1], g_RGB[2]), g_size);
			g_ShapePrototype.Prototype[g_ShapeChoise]->Draw(graphicmem, pen);
			delete pen;
		}

		UINT h = 177;

		GetRibbonHeight(&h);
		BitBlt(hdc, rect.left, h, width, height - h, memDC, rect.left, h, SRCCOPY);

		//nếu gọi đến đây để lưu thì lưu
		if (is_saving)
		{
			switch (type_Img)
			{
			case 1:
				SaveOject(fileName);
				break;
			case 2:

			case 3:
			{
				HPALETTE hpal = NULL;

				//khai báo Prototype để của hàm trong DLL	
				typedef bool(*MYPROC)(LPCWSTR , HBITMAP , HPALETTE );
				HINSTANCE hinstLib;
				MYPROC Procaddr;
				//Load dll
				hinstLib = LoadLibrary(L"DLL.dll");
				//nếu load thành công thì lấy địa chỉ hàm DialogOpenFile
				if (hinstLib != NULL) {
					Procaddr = (MYPROC)GetProcAddress(hinstLib, "saveBitmap");
					if (Procaddr != NULL) {
						Procaddr(fileName, hBitmap, hpal);
						is_saving = FALSE;
						break;
					}
				}
			}
			case 4:
				break;
			default:
				break;
			}
		}

		DeleteObject(hBitmap);
		DeleteObject(memDC);

		DeleteBitmap(hBitmap);
		DeleteDC(memDC);
		EndPaint(hWnd, &ps);
	}
	break;
	case WM_DESTROY:
		//Giải phóng farmwork Ribbon
		DestroyFramework();

		//Giải phóng GDI+
		Gdiplus::GdiplusShutdown(gdiplustoken);

		PostQuitMessage(0);
		break;
	case WM_CLOSE:
	{
		int choise = MessageBox(hWnd, L"Bạn có muốn lưu trước khi thoát?", L"Thông báo!", MB_YESNOCANCEL);
		switch (choise)
		{
		case IDNO:
			//xóa tất cả phần tử trước khi thoát
			for (int i = 0; i < g_ListObject.size(); i++)
			{
				delete g_ListObject[i];
			}

			//thoát
			PostQuitMessage(0);
			break;
		case IDYES:
		{
			//lưu lại
			//khai báo Prototype để của hàm trong DLL	
			typedef BOOL(*MYPROC)(HWND, WCHAR*, int*);
			HINSTANCE hinstLib;
			MYPROC Procaddr;
			//Load dll
			hinstLib = LoadLibrary(L"DLL.dll");
			//nếu load thành công thì lấy địa chỉ hàm DialogOpenFile
			if (hinstLib != NULL) {
				Procaddr = (MYPROC)GetProcAddress(hinstLib, "DiaLogSaveFile");
				if (Procaddr != NULL) {
					if ((MYPROC)Procaddr(hWnd, fileName,&type_Img))
					{
						SaveOject(fileName);
					}
					else
					{
						is_saving = TRUE;
						SendMessage(hWnd, WM_PAINT, 0, 0);
						break;
					}

					//xóa tất cả phần tử trước khi thoát
					for (int i = 0; i < g_ListObject.size(); i++)
					{
						delete g_ListObject[i];
					}
				}
				else
				{
					MessageBeep(MB_ICONEXCLAMATION);
					MessageBox(hWnd, L"File DLL.dll không đúng định dạng!", L"Thông báo", MB_OK);
				}
			}
			else
			{
				MessageBeep(MB_ICONEXCLAMATION);
				MessageBox(hWnd, L"Không thể tìm thấy file DLL.dll!", L"Thông báo", MB_OK);
			}
			}
			//thoát
			PostQuitMessage(0);
			break;
		case IDCANCEL:
			break;
		default:
			break;
		}
		//DestroyWindow(hWnd);
		break;
	}
	default:
		return DefWindowProc(hWnd, message, wParam, lParam);
	}
	return 0;
}

// Message handler for about box.
INT_PTR CALLBACK About(HWND hDlg, UINT message, WPARAM wParam, LPARAM lParam)
{
	UNREFERENCED_PARAMETER(lParam);
	switch (message)
	{
	case WM_INITDIALOG:
		return (INT_PTR)TRUE;

	case WM_COMMAND:
		if (LOWORD(wParam) == IDOK || LOWORD(wParam) == IDCANCEL)
		{
			EndDialog(hDlg, LOWORD(wParam));
			return (INT_PTR)TRUE;
		}
		break;
	}
	return (INT_PTR)FALSE;
}


bool SaveOject(WCHAR fileName[])
{
	fstream f(fileName, ios_base::out);
	if (!f.is_open())
		return false;

	for (int i = 0; i < g_ListObject.size(); i++)
	{
		g_ListObject[i]->SaveToFile(f);
	}

	f << -1;
	f.close();
	return true;
}

bool LoadOject(WCHAR fileName[])
{

	//xóa tất cả phần tử hiện tại
	while (!g_ListObject.empty())
	{
		delete g_ListObject[g_ListObject.size() - 1];
		g_ListObject.pop_back();
	}

	fstream f(fileName, ios_base::in);
	if (!f.is_open())
		return false;

	int temp;
	CShape *shapeTemp;
	while (!f.eof())
	{
		f >> temp;
		if (temp == -1)
			break;
		switch (temp)
		{
		case square:
			shapeTemp = new CSquare();
			shapeTemp->LoadFromFile(f);
			break;
		case rectangle:
			shapeTemp = new CRectangle();
			shapeTemp->LoadFromFile(f);
			break;
		case ellipse:
			shapeTemp = new CEllipse();
			shapeTemp->LoadFromFile(f);
			break;
		case circle:
			shapeTemp = new CCircle();
			shapeTemp->LoadFromFile(f);
			break;
		case line:
			shapeTemp = new Cline();
			shapeTemp->LoadFromFile(f);
			break;
		default:
			break;
		}
		g_ListObject.push_back(shapeTemp);
	}
	f.close();
	return true;
}

void HandleModePaint(TypeShape type)
{
	switch (type)
	{
	case rectangle:
		CheckMenuItem(hMenu, ID_PAINT_Ellipse, MF_UNCHECKED);
		CheckMenuItem(hMenu, ID_PAINT_LINE, MF_UNCHECKED);

		CheckMenuItem(hMenu, ID_PAINT_RECTANGLE, MF_CHECKED);


		//xóa hết những gì trong popup
		DeleteMenu(hPopupMenu, 0, MF_BYPOSITION);
		DeleteMenu(hPopupMenu, 0, MF_BYPOSITION);
		DeleteMenu(hPopupMenu, 0, MF_BYPOSITION);

		//thêm lại item vào popup
		InsertMenu(hPopupMenu, 0, MF_UNCHECKED, ID_PAINT_LINE, L"Line");
		InsertMenu(hPopupMenu, 1, MF_UNCHECKED, ID_PAINT_Ellipse, L"Ellipse");
		InsertMenu(hPopupMenu, 2, MF_CHECKED, ID_PAINT_RECTANGLE, L"Rectangle");

		g_ShapeChoise = rectangle;
		break;
	case ellipse:
		CheckMenuItem(hMenu, ID_PAINT_Ellipse, MF_CHECKED);
		CheckMenuItem(hMenu, ID_PAINT_LINE, MF_UNCHECKED);

		CheckMenuItem(hMenu, ID_PAINT_RECTANGLE, MF_UNCHECKED);

		//xóa hết những gì trong popup
		DeleteMenu(hPopupMenu, 0, MF_BYPOSITION);
		DeleteMenu(hPopupMenu, 0, MF_BYPOSITION);
		DeleteMenu(hPopupMenu, 0, MF_BYPOSITION);

		//thêm lại item vào popup
		InsertMenu(hPopupMenu, 0, MF_UNCHECKED, ID_PAINT_LINE, L"Line");
		InsertMenu(hPopupMenu, 1, MF_CHECKED, ID_PAINT_Ellipse, L"Ellipse");
		InsertMenu(hPopupMenu, 2, MF_UNCHECKED, ID_PAINT_RECTANGLE, L"Rectangle");

		g_ShapeChoise = ellipse;
		break;
	case line:
		CheckMenuItem(hMenu, ID_PAINT_Ellipse, MF_UNCHECKED);
		CheckMenuItem(hMenu, ID_PAINT_LINE, MF_CHECKED);

		CheckMenuItem(hMenu, ID_PAINT_RECTANGLE, MF_UNCHECKED);

		//xóa hết những gì trong popup
		DeleteMenu(hPopupMenu, 0, MF_BYPOSITION);
		DeleteMenu(hPopupMenu, 0, MF_BYPOSITION);
		DeleteMenu(hPopupMenu, 0, MF_BYPOSITION);

		//thêm lại item vào popup
		InsertMenu(hPopupMenu, 0, MF_CHECKED, ID_PAINT_LINE, L"Line");
		InsertMenu(hPopupMenu, 1, MF_UNCHECKED, ID_PAINT_Ellipse, L"Ellipse");
		InsertMenu(hPopupMenu, 2, MF_UNCHECKED, ID_PAINT_RECTANGLE, L"Rectangle");

		g_ShapeChoise = line;
		break;
	default:
		break;
	}
}
