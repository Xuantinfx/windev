﻿#include "stdafx.h"
#include "CShape.h"


CShape::CShape()
{
	position1.x = position1.y = position2.x = position2.y = 0;
}

CShape::CShape(CPoint2D _point1, CPoint2D _point2,int _RGB[3],int _size)
{
	position1.x = _point1.x;
	position1.y = _point1.y;
	position2.x = _point2.x;
	position2.y = _point2.y;
	RGB[0] = _RGB[0];
	RGB[1] = _RGB[1];
	RGB[2] = _RGB[2];
	size = _size;
}

CShape::CShape(int _x1, int _y1,int _x2,int _y2, int _RGB[3],int _size)
{
	position1.x = _x1;
	position1.y = _y1;
	position2.x = _x2;
	position2.y = _y2;
	RGB[0] = _RGB[0];
	RGB[1] = _RGB[1];
	RGB[2] = _RGB[2];
	size = _size;
}

void CShape::SetPosition(CPoint2D _position1, CPoint2D _position2)
{
	position1.x = _position1.x;
	position1.y = _position1.y;
	position2.x = _position2.x;
	position2.y = _position2.y;
	RGB[0] = 0;
	RGB[1] = 0;
	RGB[2] = 0;
	size = 1;
}


CShape::~CShape()
{
}

void CShape::SaveToFile(fstream & f)
{
	//lưu các điểm và màu sắc, kích thước
	f << position1.x << " " << position1.y << " " << position2.x << " " << position2.y << " "<<RGB[0]<<" "<<RGB[1]<<" " << RGB[2] << " "<<size<<" ";
}

void CShape::LoadFromFile(fstream & f)
{
	f >> position1.x;
	f >> position1.y;
	f >> position2.x;
	f >> position2.y;
	f >> RGB[0];
	f >> RGB[1];
	f >> RGB[2];
	f >> size;
}
