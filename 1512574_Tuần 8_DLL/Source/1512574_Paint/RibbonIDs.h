// *****************************************************************************
// * This is an automatically generated header file for UI Element definition  *
// * resource symbols and values. Please do not modify manually.               *
// *****************************************************************************

#pragma once

#define ID_BTN_LINE 2 
#define ID_BTN_LINE_LabelTitle_RESID 60001
#define ID_BTN_LINE_LargeImages_RESID 201
#define ID_BTN_RECT 3 
#define ID_BTN_RECT_LabelTitle_RESID 60002
#define ID_BTN_RECT_SmallImages_RESID 202
#define ID_BTN_ELLIPSE 4 
#define ID_BTN_ELLIPSE_LabelTitle_RESID 60003
#define ID_BTN_ELLIPSE_SmallImages_RESID 203
#define groupShape 5 
#define groupShape_LabelTitle_RESID 60004
#define ID_BTN_OPEN 6 
#define ID_BTN_OPEN_LabelTitle_RESID 60005
#define ID_BTN_OPEN_SmallImages_RESID 204
#define ID_BTN_SAVE 7 
#define ID_BTN_SAVE_LabelTitle_RESID 60006
#define ID_BTN_SAVE_SmallImages_RESID 205
#define ID_BTN_SAVEAS 8 
#define ID_BTN_SAVEAS_LabelTitle_RESID 60007
#define ID_BTN_SAVEAS_SmallImages_RESID 206
#define ID_BTN_EXIT 9 
#define ID_BTN_EXIT_LabelTitle_RESID 60008
#define ID_BTN_EXIT_SmallImages_RESID 207
#define ID_BTN_NEW 10 
#define ID_BTN_NEW_LabelTitle_RESID 60009
#define ID_BTN_NEW_SmallImages_RESID 208
#define ID_BTN_UNDO 11 
#define ID_BTN_UNDO_LabelTitle_RESID 60010
#define ID_BTN_UNDO_SmallImages_RESID 209
#define ID_BTN_ABOUT 12 
#define ID_BTN_ABOUT_LabelTitle_RESID 60011
#define ID_BTN_ABOUT_SmallImages_RESID 210
#define ID_BTN_SIZE 13 
#define ID_BTN_SIZE_LabelTitle_RESID 60012
#define ID_BTN_SIZE_TooltipTitle_RESID 60013
#define ID_BTN_SIZE_LargeImages_RESID 60014
#define ID_BTN_SIZE_1 14 
#define ID_BTN_SIZE_1_LabelTitle_RESID 60015
#define ID_BTN_SIZE_1_TooltipTitle_RESID 60016
#define ID_BTN_SIZE_1_LargeImages_RESID 60017
#define ID_BTN_SIZE_2 15 
#define ID_BTN_SIZE_2_LabelTitle_RESID 60018
#define ID_BTN_SIZE_2_TooltipTitle_RESID 60019
#define ID_BTN_SIZE_2_LargeImages_RESID 60020
#define ID_BTN_SIZE_3 16 
#define ID_BTN_SIZE_3_LabelTitle_RESID 60021
#define ID_BTN_SIZE_3_TooltipTitle_RESID 60022
#define ID_BTN_SIZE_3_LargeImages_RESID 60023
#define GroupColor 17 
#define GroupColor_LabelTitle_RESID 60024
#define GroupSize 18 
#define GroupSize_LabelTitle_RESID 60025
#define ID_BTN_COLOR 19 
#define ID_BTN_COLOR_LabelTitle_RESID 60026
#define ID_BTN_COLOR_SmallImages_RESID 211
#define IDC_CMD_POPUPMAP 20  /* the Context Map for cmdPopup */ 
#define tabShape 21 
#define tabShape_LabelTitle_RESID 60027
#define tabSetting 22 
#define tabSetting_LabelTitle_RESID 60028
#define fileMenu_LabelTitle_RESID 60029
