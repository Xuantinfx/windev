﻿// THIS CODE AND INFORMATION IS PROVIDED "AS IS" WITHOUT WARRANTY OF
// ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO
// THE IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A
// PARTICULAR PURPOSE.
//
// Copyright (c) Microsoft Corporation. All rights reserved.
#include "stdafx.h"
#include <UIRibbon.h>

#include "CommandHandler.h"
#include "RibbonIDs.h"
#include "RibbonFramework.h"
#include "stdafx.h"
#include "resource.h"

extern int		g_RGB[3];
extern int		g_size;

// Static method to create an instance of the object.
HRESULT CCommandHandler::CreateInstance(IUICommandHandler **ppCommandHandler)
{
	if (!ppCommandHandler)
	{
		return E_POINTER;
	}

	*ppCommandHandler = NULL;

	HRESULT hr = S_OK;

	CCommandHandler* pCommandHandler = new CCommandHandler();

	if (pCommandHandler != NULL)
	{
		*ppCommandHandler = static_cast<IUICommandHandler *>(pCommandHandler);
	}
	else
	{
		hr = E_OUTOFMEMORY;
	}

	return hr;
}

// IUnknown method implementations.
STDMETHODIMP_(ULONG) CCommandHandler::AddRef()
{
	return InterlockedIncrement(&m_cRef);
}

STDMETHODIMP_(ULONG) CCommandHandler::Release()
{
	LONG cRef = InterlockedDecrement(&m_cRef);
	if (cRef == 0)
	{
		delete this;
	}

	return cRef;
}

STDMETHODIMP CCommandHandler::QueryInterface(REFIID iid, void** ppv)
{
	if (iid == __uuidof(IUnknown))
	{
		*ppv = static_cast<IUnknown*>(this);
	}
	else if (iid == __uuidof(IUICommandHandler))
	{
		*ppv = static_cast<IUICommandHandler*>(this);
	}
	else
	{
		*ppv = NULL;
		return E_NOINTERFACE;
	}

	AddRef();
	return S_OK;
}

//
//  FUNCTION: UpdateProperty()
//
//  PURPOSE: Called by the Ribbon framework when a command property (PKEY) needs to be updated.
//
//  COMMENTS:
//
//    This function is used to provide new command property values, such as labels, icons, or
//    tooltip information, when requested by the Ribbon framework.  
//    
//    In this SimpleRibbon sample, the method is not implemented.  
//
STDMETHODIMP CCommandHandler::UpdateProperty(
	UINT nCmdID,
	REFPROPERTYKEY key,
	const PROPVARIANT* ppropvarCurrentValue,
	PROPVARIANT* ppropvarNewValue)
{
	UNREFERENCED_PARAMETER(nCmdID);
	UNREFERENCED_PARAMETER(key);
	UNREFERENCED_PARAMETER(ppropvarCurrentValue);
	UNREFERENCED_PARAMETER(ppropvarNewValue);

	return E_NOTIMPL;
}

//
//  FUNCTION: Execute()
//
//  PURPOSE: Called by the Ribbon framework when a command is executed by the user.  For example, when
//           a button is pressed.
//
STDMETHODIMP CCommandHandler::Execute(
	UINT nCmdID,
	UI_EXECUTIONVERB verb,
	const PROPERTYKEY* key,
	const PROPVARIANT* ppropvarValue,
	IUISimplePropertySet* pCommandExecutionProperties)
{
	HRESULT hr = S_OK;
	switch (nCmdID)
	{
	case ID_BTN_OPEN:
	{
		SendMessage(GetForegroundWindow(), WM_COMMAND, ID_FILE_LOAD, 0);
		break;
	}
	case ID_BTN_NEW:
	{
		SendMessage(GetForegroundWindow(), WM_COMMAND, ID_FILE_NEW, 0);
	}
	break;
	case ID_BTN_SAVE:
	{
		SendMessage(GetForegroundWindow(), WM_COMMAND, ID_FILE_SAVE, 0);
	}
	break;
	case ID_BTN_SAVEAS:
		SendMessage(GetForegroundWindow(), WM_COMMAND, ID_FILE_SAVEASNEWPAINT, 0);
		break;
	case ID_BTN_UNDO:
		SendMessage(GetForegroundWindow(), WM_COMMAND, ID_EDIT_UNDO, 0);
		break;
	case ID_BTN_ABOUT:
	{
		SendMessage(GetForegroundWindow(), WM_COMMAND, IDM_ABOUT, 0);
		break;
	}
	case ID_BTN_EXIT:
		SendMessage(GetForegroundWindow(), WM_CLOSE, NULL, NULL);
		break;
	case ID_BTN_LINE:
	{
		PROPVARIANT value;

		//lấy thông tin cho value
		g_pFramework->GetUICommandProperty(ID_BTN_LINE, UI_PKEY_BooleanValue, &value);
		value.boolVal = 0;

		//tắt đi rect và ellip
		g_pFramework->SetUICommandProperty(ID_BTN_RECT, UI_PKEY_BooleanValue, value);
		g_pFramework->SetUICommandProperty(ID_BTN_ELLIPSE, UI_PKEY_BooleanValue, value);

		//bật line
		value.boolVal = -1;
		g_pFramework->SetUICommandProperty(ID_BTN_LINE, UI_PKEY_BooleanValue, value);

		SendMessage(GetForegroundWindow(), WM_COMMAND, ID_PAINT_LINE, 0);
	}
	break;
	case ID_BTN_RECT:
	{
		PROPVARIANT value;

		//lấy thông tin cho value
		g_pFramework->GetUICommandProperty(ID_BTN_LINE, UI_PKEY_BooleanValue, &value);
		value.boolVal = 0;

		//tắt đi line và ellip
		g_pFramework->SetUICommandProperty(ID_BTN_LINE, UI_PKEY_BooleanValue, value);
		g_pFramework->SetUICommandProperty(ID_BTN_ELLIPSE, UI_PKEY_BooleanValue, value);

		//bật rect
		value.boolVal = -1;
		g_pFramework->SetUICommandProperty(ID_BTN_RECT, UI_PKEY_BooleanValue, value);

		SendMessage(GetForegroundWindow(), WM_COMMAND, ID_PAINT_RECTANGLE, 0);
	}
	break;
	case ID_BTN_ELLIPSE:
	{
		PROPVARIANT value;

		//lấy thông tin cho value
		g_pFramework->GetUICommandProperty(ID_BTN_LINE, UI_PKEY_BooleanValue, &value);
		value.boolVal = 0;

		//tắt đi rect và line
		g_pFramework->SetUICommandProperty(ID_BTN_RECT, UI_PKEY_BooleanValue, value);
		g_pFramework->SetUICommandProperty(ID_BTN_LINE, UI_PKEY_BooleanValue, value);

		//bật ellip
		value.boolVal = -1;
		g_pFramework->SetUICommandProperty(ID_BTN_ELLIPSE, UI_PKEY_BooleanValue, value);

		SendMessage(GetForegroundWindow(), WM_COMMAND, ID_PAINT_Ellipse, 0);
	}
	break;
	case ID_BTN_COLOR:
	{
		CHOOSECOLOR cc;
		static COLORREF colors[16];
		static DWORD rgbCurrent;

		ZeroMemory(&cc, sizeof(cc));
		cc.lStructSize = sizeof(cc);
		cc.hwndOwner = GetForegroundWindow();
		cc.lpCustColors = (LPDWORD)colors;
		cc.rgbResult = rgbCurrent;
		cc.Flags = CC_FULLOPEN | CC_RGBINIT;

		if (ChooseColor(&cc) == TRUE) {
			rgbCurrent = cc.rgbResult;
			g_RGB[0] = (int)GetRValue(rgbCurrent);
			g_RGB[1] = (int)GetGValue(rgbCurrent);
			g_RGB[2] = (int)GetBValue(rgbCurrent);

			InvalidateRect(GetForegroundWindow(), NULL, FALSE);
		}
		break;
	}
	case ID_BTN_SIZE_1:
		g_size = 1;

		PROPVARIANT value;

		//lấy thông tin cho value
		g_pFramework->GetUICommandProperty(ID_BTN_SIZE_1, UI_PKEY_BooleanValue, &value);
		value.boolVal = 0;

		//tắt đi size2 và size3
		g_pFramework->SetUICommandProperty(ID_BTN_SIZE_2, UI_PKEY_BooleanValue, value);
		g_pFramework->SetUICommandProperty(ID_BTN_SIZE_3, UI_PKEY_BooleanValue, value);

		//bật size1
		value.boolVal = -1;
		g_pFramework->SetUICommandProperty(ID_BTN_SIZE_1, UI_PKEY_BooleanValue, value);
		break;
	case ID_BTN_SIZE_2:
		g_size = 3;
		//lấy thông tin cho value
		g_pFramework->GetUICommandProperty(ID_BTN_SIZE_1, UI_PKEY_BooleanValue, &value);
		value.boolVal = 0;

		//tắt đi size1 và size3
		g_pFramework->SetUICommandProperty(ID_BTN_SIZE_1, UI_PKEY_BooleanValue, value);
		g_pFramework->SetUICommandProperty(ID_BTN_SIZE_3, UI_PKEY_BooleanValue, value);

		//bật size2
		value.boolVal = -1;
		g_pFramework->SetUICommandProperty(ID_BTN_SIZE_2, UI_PKEY_BooleanValue, value);
		break;
	case ID_BTN_SIZE_3:
		g_size = 5;
		//lấy thông tin cho value
		g_pFramework->GetUICommandProperty(ID_BTN_SIZE_1, UI_PKEY_BooleanValue, &value);
		value.boolVal = 0;

		//tắt đi size2 và size1
		g_pFramework->SetUICommandProperty(ID_BTN_SIZE_2, UI_PKEY_BooleanValue, value);
		g_pFramework->SetUICommandProperty(ID_BTN_SIZE_1, UI_PKEY_BooleanValue, value);

		//bật size3
		value.boolVal = -1;
		g_pFramework->SetUICommandProperty(ID_BTN_SIZE_3, UI_PKEY_BooleanValue, value);
		break;
	default:
		break;
	}
	/*UNREFERENCED_PARAMETER(pCommandExecutionProperties);
	UNREFERENCED_PARAMETER(ppropvarValue);
	UNREFERENCED_PARAMETER(key);
	UNREFERENCED_PARAMETER(verb);
	UNREFERENCED_PARAMETER(nCmdID);*/

	return hr;

}

