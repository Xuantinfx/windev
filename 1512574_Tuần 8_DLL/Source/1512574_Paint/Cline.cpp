#include "stdafx.h"
#include "Cline.h"


Cline::Cline()
{
}

Cline::Cline(CPoint2D _d1, CPoint2D _d2)
{
	position1.x = _d1.x;
	position1.y = _d1.y;
	position2.x = _d2.x;
	position2.y = _d2.y;
}

Cline::Cline(int _x1, int _y1, int _x2, int _y2)
{
	position1.x = _x1;
	position1.y = _y1;
	position2.x = _x2;
	position2.y = _y2;
}

void Cline::Draw(Gdiplus::Graphics* _graphic, Gdiplus::Pen* _pen)
{
	_graphic->DrawLine(_pen, position1.x, position1.y, position2.x, position2.y);
	//MoveToEx(_hdc, position1.x, position1.y, NULL);
	//LineTo(_hdc, position2.x, position2.y);
}

TypeShape Cline::GetType()
{
	return line;
}

void Cline::SaveToFile(fstream & f)
{
	f << line << " ";
	CShape::SaveToFile(f);
}


Cline::~Cline()
{
}
