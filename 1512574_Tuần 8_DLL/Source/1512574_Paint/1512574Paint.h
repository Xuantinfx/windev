﻿#pragma once

#include "resource.h"


/******************* Khu vực khai báo hàm ***************************/
// Forward declarations of functions included in this code module:
ATOM                MyRegisterClass(HINSTANCE hInstance);
BOOL                InitInstance(HINSTANCE, int);
LRESULT CALLBACK    WndProc(HWND, UINT, WPARAM, LPARAM);
INT_PTR CALLBACK    About(HWND, UINT, WPARAM, LPARAM);


bool SaveOject(WCHAR fileName[]);
bool LoadOject(WCHAR fileName[]);
void HandleModePaint(TypeShape type);