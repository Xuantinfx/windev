#include "stdafx.h"
#include "CEllipse.h"


void CEllipse::Draw(Gdiplus::Graphics* _graphic, Gdiplus::Pen* _pen)
{
	_graphic->DrawEllipse(_pen, position1.x, position1.y, position2.x - position1.x, position2.y - position1.y);
	//Ellipse(_hdc, position1.x, position1.y, position2.x, position2.y);
}

TypeShape CEllipse::GetType()
{
	return ellipse;
}

void CEllipse::SaveToFile(fstream & f)
{
	f << ellipse << " ";
	CShape::SaveToFile(f);
}

CEllipse::CEllipse(CPoint2D _point1, CPoint2D _point2)
{
	position1.x = _point1.x;
	position1.y = _point1.y;
	position2.x = _point2.x;
	position2.y = _point2.y;
}

CEllipse::CEllipse(int _x1, int _y1, int _x2, int _y2)
{
	position1.x = _x1;
	position1.y = _y1;
	position2.x = _x2;
	position2.y = _y2;
}

CEllipse::CEllipse()
{
}


CEllipse::~CEllipse()
{
}
