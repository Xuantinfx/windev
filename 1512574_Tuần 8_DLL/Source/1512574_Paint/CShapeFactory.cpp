#include "stdafx.h"
#include "CShapeFactory.h"


CShape * CShapeFactory::GetShape(TypeShape _type, int _xStart, int _yStart, int _xEnd, int _yEnd,int _RGB[3],int _size)
{
	CPoint2D position1(_xStart, _yStart), position2(_xEnd, _yEnd);
	CShape *temp;
	switch (_type)
	{
	case square:
	{
		int canh = abs(_xStart - _xEnd);
		if (canh < abs(_yStart - _xEnd));
		{
			canh = abs(_yStart - _xEnd);
		}
		temp =  new CSquare(position1, position2);
		break;
	}
	case rectangle:
	{
		temp = new CRectangle(position1, position2);
		break;
	}
	case line:
	{
		temp = new Cline(position1, position2);
		break;
	}
	case ellipse:
	{
		temp = new CEllipse(position1, position2);
		break;
	}
	case circle:
	{
		temp = new CCircle(position1, position2);
		break;
	}
	default: return nullptr;
	}
	temp->RGB[0] = _RGB[0];
	temp->RGB[1] = _RGB[1];
	temp->RGB[2] = _RGB[2];
	temp->size = _size;
	return temp;
}

CShape * CShapeFactory::GetShape(TypeShape _type, CPoint2D position1, CPoint2D position2,int _RGB[3],int _size)
{
	CShape *temp;
	switch (_type)
	{
	case square:
	{
		int canh = abs(position1.x - position2.x);
		if (canh < abs(position1.y - position2.y));
		{
			canh = abs(position1.y - position2.y);
		}
		temp = new CSquare(position1, position2);
		break;
	}
	case rectangle:
	{
		temp = new CRectangle(position1, position2);
		break;
	}
	case line:
	{
		temp = new Cline(position1, position2);
		break;
	}
	case ellipse:
	{
		temp = new CEllipse(position1, position2);
		break;
	}
	case circle:
	{
		temp = new CCircle(position1, position2);
		break;
	}
	default: return nullptr;
	}
	temp->RGB[0] = _RGB[0];
	temp->RGB[1] = _RGB[1];
	temp->RGB[2] = _RGB[2];
	temp->size = _size;
	return temp;
}

CShapeFactory::CShapeFactory()
{
}


CShapeFactory::~CShapeFactory()
{
}
