// stdafx.h : include file for standard system include files,
// or project specific include files that are used frequently, but
// are changed infrequently
//

#pragma once

#include "targetver.h"

#define WIN32_LEAN_AND_MEAN             // Exclude rarely-used stuff from Windows headers
// Windows Header Files:
#include <windows.h>

// C RunTime Header Files
#include <stdlib.h>
#include <malloc.h>
#include <memory.h>
#include <tchar.h>
#include <windowsx.h>
#include <vector>
#include <fstream>
#include <commdlg.h>
#include <Olectl.h>

using namespace std;

#include "CShapeFactory.h"
#include "CShapePrototype.h"

//Ribbon
#include <objbase.h>
#pragma comment(lib,"Ole32.lib")
#include "RibbonFramework.h"
#include "RibbonIDs.h"

//GDI+
#include <ObjIdl.h>
#include <gdiplus.h>
#pragma comment (lib,"gdiplus.lib")
//using namespace Gdiplus;

// TODO: reference additional headers your program requires here
